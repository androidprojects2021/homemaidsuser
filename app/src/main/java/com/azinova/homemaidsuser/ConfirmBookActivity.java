package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.fragments.ChooseAdressDialog;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.azinova.homemaidsuser.utils.Shared.bookingdetails;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class ConfirmBookActivity extends AppCompatActivity {

    private RelativeLayout backbutton;
    private RelativeLayout cusLay, detLay, sumLay;
    private LinearLayout cusDetailLay, detDetailLay, sumDetailLay;
    private boolean iscusclicked = false;
    private boolean isdetclicked = true;
    private boolean issumclicked = true;
    private ImageView cusArrow, detArrow, sumArrow;
    private RelativeLayout proceedButton;
    private TextView name, mobile, area, email, address;
    private TextView date, booktype, sertype, time, maids;
    private Prefs pref;
    private CustomerModels customerModels;
    private String total_price, service_total, price_splits, cleaning_material_total_price, clean_split_value, offer_price, vat, vat_amount, dis_type_val, dis_price,notes;
    private int total_cleanign_hours;
    private TextView ser_total, clean_total, total_spilt, total_price_text;
    private TextView ser_split, clean_split, sum_total, offer_price_text, discount_label;
    private ProgressDialog pDialog;
    private RelativeLayout help_icon;
    private LinearLayout dis_lay, clean_lay;
    private TextView vat_text;
    private TextView vat_price_text;
    private boolean ismultiaddress = false;
    private String muladdressid = "";
    private LinearLayout editLay;
    private TextView change_add;
    private TextView dis_type;
    private LinearLayout notesLayout;
    private TextView txtNotes;

//    private Prefs pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_book);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            notes =  extras.getString("notes", "");

            total_price = extras.getString("total_price", "error");
            service_total = extras.getString("service_total", "");
            price_splits = extras.getString("price_splits", "");
            cleaning_material_total_price = extras.getString("cleaning_material_total_price", "");
            offer_price = extras.getString("offer_price_perc", "");
            dis_price = extras.getString("dis_amount", "");
            clean_split_value = extras.getString("cleaning_price_splits");
            vat = extras.getString("vat");
            vat_amount = extras.getString("vat_amount");
            dis_type_val = extras.getString("dis_type");
            total_cleanign_hours = extras.getInt("cleaning_hours");
        }


        pref = Prefs.with(this);
        initView();
        EventBus.getDefault().post(new UiupdateEvent());
        clickListeners();

        LogUtil.e(bookingdetails.toString());


        if (customerModels.getAreaList().size() > 1) {
            ChooseAdressDialog addressDialog = new ChooseAdressDialog();
            addressDialog.show(ConfirmBookActivity.this.getFragmentManager(), "1");
            change_add.setVisibility(View.VISIBLE);
            ismultiaddress = true;
        } else {
            ismultiaddress = false;
            change_add.setVisibility(View.GONE);
        }

        if (total_cleanign_hours >= 4) {
            clean_split.setVisibility(View.VISIBLE);
        } else clean_split.setVisibility(View.INVISIBLE);

    }

    private void clickListeners() {
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        cusLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (iscusclicked) {
                    cusDetailLay.setVisibility(View.GONE);
                    cusArrow.setRotation(90);
                    iscusclicked = false;
                } else {
                    cusDetailLay.setVisibility(View.VISIBLE);
                    cusArrow.setRotation(0);
                    iscusclicked = true;
                }

            }
        });

        detLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isdetclicked) {
                    detDetailLay.setVisibility(View.GONE);
                    detArrow.setRotation(90);
                    isdetclicked = false;
                } else {
                    detDetailLay.setVisibility(View.VISIBLE);
                    detArrow.setRotation(0);
                    isdetclicked = true;
                }
            }
        });

        sumLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (issumclicked) {
                    sumDetailLay.setVisibility(View.GONE);
                    sumArrow.setRotation(90);
                    issumclicked = false;
                } else {
                    sumDetailLay.setVisibility(View.VISIBLE);
                    sumArrow.setRotation(0);
                    issumclicked = true;
                }
            }
        });

        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customerModels = pref.getUserDetails();
                CustomerDetails model = customerModels.getCustomerDetails();

                if (model.getEmail() == null || model.getPhone() == null || model.getEmail().isEmpty() || model.getPhone().isEmpty()) {
                    Snackbar.make(v, "Please fill required fields", Snackbar.LENGTH_SHORT).show();
                    if (!iscusclicked) {
                        cusDetailLay.setVisibility(View.VISIBLE);
                        cusArrow.setRotation(0);
                        iscusclicked = true;
                    }
                    return;
                }

                if (customerModels.getAreaList().size() > 0) {
                    if (customerModels.getAreaList().get(0).getCustomerAddress() == null || customerModels.getAreaList().get(0).getAreaName() == null) {
                        Snackbar.make(v, "Please fill required fields", Snackbar.LENGTH_SHORT).show();
                        if (!iscusclicked) {
                            cusDetailLay.setVisibility(View.VISIBLE);
                            cusArrow.setRotation(0);
                            iscusclicked = true;
                        }
                        return;
                    }
                } else {
                    Snackbar.make(v, "Please fill required fields", Snackbar.LENGTH_SHORT).show();
                    if (!iscusclicked) {
                        cusDetailLay.setVisibility(View.VISIBLE);
                        cusArrow.setRotation(0);
                        iscusclicked = true;
                    }
                    return;
                }

                BookingEngine();
            }
        });

        editLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pro = new Intent(ConfirmBookActivity.this, EditProfileActivity.class);
                pro.putExtra("user_data", customerModels);
                pro.putExtra("isfromconfirm", true); //to be done in profile
                startActivity(pro);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        help_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(ConfirmBookActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

        change_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismultiaddress) {
                    ChooseAdressDialog addressDialog = new ChooseAdressDialog();
                    addressDialog.show(ConfirmBookActivity.this.getFragmentManager(), "1");
                }
            }
        });
    }

    private void initView() {
        backbutton = findViewById(R.id.backbutton);
        cusLay = findViewById(R.id.cus_lay);
        cusDetailLay = findViewById(R.id.cus_lay_ans);
        editLay = findViewById(R.id.edit_lay);
        change_add = findViewById(R.id.change_add);
        detLay = findViewById(R.id.det_lay);
        detDetailLay = findViewById(R.id.det_ans_lay);
        sumLay = findViewById(R.id.sum_lay);
        sumDetailLay = findViewById(R.id.sum_ans_lay);
        cusArrow = findViewById(R.id.cus_arrow);
        detArrow = findViewById(R.id.det_arrow);
        sumArrow = findViewById(R.id.sum_arrow);
        proceedButton = findViewById(R.id.proceedButton);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        email = findViewById(R.id.email);
        area = findViewById(R.id.area);
        address = findViewById(R.id.address);
        date = findViewById(R.id.date);
        booktype = findViewById(R.id.booktype);
        sertype = findViewById(R.id.sertype);
        time = findViewById(R.id.time);
        maids = findViewById(R.id.maids);

        notesLayout = findViewById(R.id.notesLayout);

        ser_total = findViewById(R.id.ser_total);
        ser_split = findViewById(R.id.ser_split);
        clean_split = findViewById(R.id.split_clean);
        clean_total = findViewById(R.id.clean_total);
        sum_total = findViewById(R.id.total_sum);
        offer_price_text = findViewById(R.id.offer_price_text);
        total_spilt = findViewById(R.id.total_spilt);
        total_price_text = findViewById(R.id.total_price_text);

        help_icon = findViewById(R.id.help_lay);
        dis_lay = findViewById(R.id.dis_lay);
        clean_lay = findViewById(R.id.clean_lay);

        vat_text = findViewById(R.id.vat_text);
        vat_price_text = findViewById(R.id.vat_price_text);
        dis_type = findViewById(R.id.dis_type);
        discount_label = findViewById(R.id.discount_label);
        txtNotes = findViewById(R.id.txtNotes);


        if(notes.isEmpty()){
            notesLayout.setVisibility(View.GONE);
        }else{
            notesLayout.setVisibility(View.VISIBLE);
            txtNotes.setText(notes);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(finishEvent event) {
        finish();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(UiupdateEvent event) {

        customerModels = pref.getUserDetails();
        CustomerDetails model = customerModels.getCustomerDetails();
        name.setText(model.getName());
        email.setText(model.getEmail());
        mobile.setText(model.getPhone());
        if (customerModels.getAreaList().size() > 0) {
            address.setText(customerModels.getAreaList().get(0).getCustomerAddress());
            area.setText(customerModels.getAreaList().get(0).getAreaName());
        }

        JSONObject details = bookingdetails;
        try {
            date.setText(details.getString("date"));

            if (details.getString("book_type").equalsIgnoreCase("OD")) {
                booktype.setText("One Day");
            } else if (details.getString("book_type").equalsIgnoreCase("WE")) {

                String weekly = "Every Week\n( ";
                JSONArray arry = details.getJSONArray("service_week");
                for (int i = 0; i < arry.length(); i++) {
                    String m = arry.get(i).toString();
                    if (m.equalsIgnoreCase("0")) {
                        weekly = weekly + "Sun, ";
                    } else if (m.equalsIgnoreCase("1")) {
                        weekly = weekly + "Mon, ";
                    } else if (m.equalsIgnoreCase("2")) {
                        weekly = weekly + "Tue, ";
                    } else if (m.equalsIgnoreCase("3")) {
                        weekly = weekly + "Wed, ";
                    } else if (m.equalsIgnoreCase("4")) {
                        weekly = weekly + "Thu, ";
                    } else if (m.equalsIgnoreCase("6")) {
                        weekly = weekly + "Sat, ";
                    }
                }
                weekly = weekly.substring(0, weekly.length() - 2);
                booktype.setText(weekly + " )");
            }


            if (details.getString("service_type").equalsIgnoreCase("1")) {
                sertype.setText("Residential Cleaning");
            } else if (details.getString("service_type").equalsIgnoreCase("2")) {
                sertype.setText("Commercial Cleaning");
            } else if (details.getString("service_type").equalsIgnoreCase("6")) {
                sertype.setText("Parties and After Parties");
            }

            time.setText(details.getString("from_time") + " - " + details.getString("to_time"));
            maids.setText(details.getString("maid_no"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ser_total.setText("AED " + service_total);
        ser_split.setText(price_splits);
        if (cleaning_material_total_price.equalsIgnoreCase("0.00")) {
            clean_lay.setVisibility(View.GONE);
        } else {
            clean_lay.setVisibility(View.VISIBLE);
            clean_total.setText("AED " + cleaning_material_total_price);
            clean_split.setText(clean_split_value);
        }
        sum_total.setText("AED " + total_price);
        if (!offer_price.isEmpty()) {
            dis_lay.setVisibility(View.VISIBLE);
            offer_price_text.setText(dis_price + " AED");
            discount_label.setText("Discount " + offer_price + "%");
            dis_type.setText(dis_type_val);
        } else {
            dis_lay.setVisibility(View.GONE);
        }
        total_spilt.setText(price_splits);
        total_price_text.setText(total_price);
        vat_text.setText(vat);
        vat_price_text.setText("AED " + vat_amount);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

    private void BookingEngine() {

        pDialog = new ProgressDialog(ConfirmBookActivity.this);
        pDialog.setMessage("Send Booking...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject bookingdata = new JSONObject();
        try {
            bookingdata = bookingdetails;
            bookingdata.put("customer_id", pref.getCustomerId());

            Log.e("Default",ismultiaddress+"");

            if (ismultiaddress) {

                if(muladdressid.equals("")){
                    bookingdata.put("customer_address_id",customerModels.getAreaList().get(0).getCustomerAddressId());
                }else { bookingdata.put("customer_address_id", muladdressid);}

            } else {
                bookingdata.put("customer_address_id",customerModels.getAreaList().get(0).getCustomerAddressId()); //// TODO: 09/01/18
            }
            bookingdata.put("rate", total_price);

//            prefs.getUserDetails().getAreaList().get(0).getCustomerAddressId()
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("INPUT",bookingdata.toString());

        JsonObjectRequest bookingjsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.bookingShift(), bookingdata,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtil.e(response.toString());
                        pDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("error") ||
                                    response.getString("status").equalsIgnoreCase("failure")) {
                                Snackbar.make(proceedButton, "Booking Failed", Snackbar.LENGTH_LONG).show();
                                return;
                            }

                            if (response.getString("status").equalsIgnoreCase("success")) {
                                LogUtil.e(response.toString());

                                Intent proc = new Intent(ConfirmBookActivity.this, BookedDetailActivity.class);
                                proc.putExtra("total_price", response.getString("total_price"));
                                proc.putExtra("service_total", response.getString("service_total"));
                                proc.putExtra("price_splits", response.getString("price_splits"));
                                proc.putExtra("maid_splits", response.getString("maid_splits"));
                                proc.putExtra("booking_id", response.getString("booking_id"));

                                proc.putExtra("reference_id", response.getString("reference_id"));

                                proc.putExtra("service_date_new", response.getString("service_date_new"));
                                proc.putExtra("day_service_id", "0");


                                proc.putExtra("vat", response.getString("vat"));
                                proc.putExtra("vat_amount", response.getString("vat_amount"));
                                proc.putExtra("date", date.getText().toString());
                                proc.putExtra("time", time.getText().toString());
                                proc.putExtra("address", address.getText().toString());
                                proc.putExtra("area", area.getText().toString());
                                startActivity(proc);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            Snackbar.make(proceedButton, "Booking Failed", Snackbar.LENGTH_LONG).show();


                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(proceedButton, message, Snackbar.LENGTH_LONG).show();
            }
        });

        bookingjsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(bookingjsonObjReq);

    }

    public static class finishEvent {

    }

    public static class UiupdateEvent {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AddressEvent event) {
        LogUtil.e(event.cus_id + event.cus_add + event.cus_zon); //todo need address control  here
        muladdressid = event.cus_id;
        address.setText(event.cus_add);
        area.setText(event.cus_zon);
    }

    public static class AddressEvent {
        public String cus_id, cus_add, cus_zon;

        public AddressEvent(String cus_id, String address, String zone) {
            this.cus_id = cus_id;
            this.cus_add = address;
            this.cus_zon = zone;
        }
    }


}
