package com.azinova.homemaidsuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.adapters.GalleryViewAdapter;
import com.azinova.homemaidsuser.models.Gallery;
import com.azinova.homemaidsuser.utils.GlideUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryViewActivity extends AppCompatActivity {
    private static final String TAG = "Gallery Activity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.help_lay)
    RelativeLayout help;
    Bundle bundle;
    private List<Gallery> galleries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_view);
        ButterKnife.bind(this);
        setupRecyler();
        bundle = getIntent().getExtras();
        if (bundle != null) {
            Gallery galleryModel = new Gson().fromJson(bundle.getString("json"), Gallery.class);
            recyclerView.setAdapter(new GalleryViewAdapter(galleryModel.getCatImgs(), this));
            recyclerView.scheduleLayoutAnimation();
            title.setText(galleryModel.getCatName());
            GlideUtils.loadImage(galleryModel.getCatImgUrl(), imageView, this);
        }


        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(GalleryViewActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });
    }

    private void setupRecyler() {
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setLayoutAnimation(controller);
    }

    public void backClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
