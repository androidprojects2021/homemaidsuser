package com.azinova.homemaidsuser.payment;


public interface Communicator  {
    public void respond(String data);
    public void actionSelected(String data);
}
