package com.azinova.homemaidsuser.payment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.azinova.homemaidsuser.BookedDetailActivity;
import com.azinova.homemaidsuser.HelpActivity;
import com.azinova.homemaidsuser.MainActivity;
import com.azinova.homemaidsuser.MakePaymentActivity;
import com.azinova.homemaidsuser.PaymentSuccessActivity;
import com.azinova.homemaidsuser.PaymentUtility.AvenuesParams;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.utils.Prefs;
import com.google.gson.JsonObject;
import com.google.zxing.common.StringUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

public class PaymentWebViewActivity extends AppCompatActivity {

    WebView payWebView;
    ProgressDialog loading;
    String amount,booking_id,From,description,reference_id,service_date_new,day_service_id;
    String customer_id ="";
    String email = "";
    String name = "";
    String address = "";
    private RelativeLayout backButton;
//    private RelativeLayout closeButton;
    private RelativeLayout help_lay;
    String trackingId = "";
    String orderId = "";
    String modeOfPayment = "";

    JsonObject jsonInput = new JsonObject();
    JsonObject jsonInputMakePayment = new JsonObject();

//
//    private String url= "https://www.homemaids.ae/homemaid-ngpay/ApiMobileView/online_payment?ws=homemaids1511201717359d1ac3d2516d0b7f2";//demo payment url
//
//    private String paymentsuccess= "https://www.homemaids.ae/homemaid-ngpay/apiMobileView/payment_success?ref=7340879b-bea5-474e-af3f-96415c5ab349&order=3008990=&mode=";//demo payment_success
//
//    private String url_make_payment= "https://www.homemaids.ae/homemaid-ngpay/ApiMobileView/make_customer_payment?ws=homemaids1511201717359d1ac3d2516d0b7f2";//demo make payment url
//



    private String url= "https://www.homemaids.ae/ApiMobileView/online_payment?ws=homemaids1511201717359d1ac3d2516d0b7f2";//live payment url

    private String paymentsuccess= "https://www.homemaids.ae/ApiMobileView/payment_success?ref=&order=&mode=";//live payment_success

    private String url_make_payment= "https://www.homemaids.ae/ApiMobileView/make_customer_payment?ws=homemaids1511201717359d1ac3d2516d0b7f2";//live make payment url

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view);
        init();

        onClick();

        loading = new ProgressDialog(this);

        payWebView.getSettings().setSupportZoom(true);

        payWebView.getSettings().setBuiltInZoomControls(true);

        payWebView.getSettings().setJavaScriptEnabled(true);


//        payWebView.getSettings().setLoadWithOverviewMode(true);
//        payWebView.getSettings().setUseWideViewPort(true);

        Prefs  prefs = Prefs.with(this);
        customer_id=""+prefs.with(getApplicationContext()).getCustomerId();
        email=""+prefs.with(getApplicationContext()).getEmail();
        name=""+prefs.with(getApplicationContext()).getName();
        address=""+prefs.with(getApplicationContext()).getAddress();

        jsonInput.addProperty("amount",amount);
        jsonInput.addProperty("customer_id",customer_id);
        jsonInput.addProperty("reference_id",reference_id);
        jsonInput.addProperty("customer_email",email);
        jsonInput.addProperty("customer_mobile","null");
        jsonInput.addProperty("customer_name",name);
        jsonInput.addProperty("address",address);
        jsonInput.addProperty("booking_id",booking_id);


        if(day_service_id.equals("")){day_service_id = "0";}
        jsonInput.addProperty("day_service_id",day_service_id);
        jsonInput.addProperty("service_date_new",service_date_new);


        Log.e("JSON INP",jsonInput.toString());


        if(From.equals("make_payment")){

            jsonInputMakePayment.addProperty("customer_id",prefs.getCustomerId());
            jsonInputMakePayment.addProperty("price",amount);
            jsonInputMakePayment.addProperty("description",description);

            callWebViewMakePayment(url_make_payment,jsonInputMakePayment.toString().getBytes());

            urlChangeHandle();




        }else if(From.equals("book_detail")){

            callWebViewPay(url,jsonInput.toString().getBytes());
            urlChangeHandle();
        }
        else if(From.equals("payment_fail")){

            callWebViewPay(url,jsonInput.toString().getBytes());
            urlChangeHandle();
        }
        else if(From.equals("booking_history")){

            callWebViewPay(url,jsonInput.toString().getBytes());
            urlChangeHandle();
        }


        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");

        payWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                if (progress < 100) {
                    progressDialog.show();
                }
                if (progress == 100) {
                    progressDialog.cancel();
                }
            }
        });

    }

    private void finishBackActivities() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void onClick() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishBackActivities();
            }
        });

//        closeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finishBackActivities();
//            }
//        });

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(PaymentWebViewActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });
    }


    private void callWebViewMakePayment(String url_make_payment, byte[] bytes) {

        payWebView.postUrl(url_make_payment,bytes);

    }

    private void urlChangeHandle() {

        payWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(payWebView, url);

//
                int refIndex = url.indexOf("ref=");
                if (refIndex >= 0) {
                    trackingId = url.substring(refIndex + 4, url.indexOf("&order"));
                }

                int ordrIndex = url.indexOf("order=");
                if (ordrIndex >= 0) {
                    orderId = url.substring(ordrIndex + 6, url.indexOf("&mode"));
                }

                int modeIndex =  url.indexOf("&mode=");
                if (modeIndex >= 0) {
                    modeOfPayment = url.substring(modeIndex+6);
                }


                if (url.contains("payment_error")) {
                    onPageFail();
                }

                if (url.contains("payment_success")) {

                    onPageSuccess();
                }
            }
        });

    }

    private void onPageFail() {
        Intent intent = new Intent(getApplicationContext(), PaymentSuccessActivity.class);
        intent.putExtra("transStatus", "fail");
        intent.putExtra("price", amount);
        intent.putExtra("order_id",orderId);
        intent.putExtra("tracking_id",trackingId);
        intent.putExtra("from",From);
        intent.putExtra("modeOfPayment",modeOfPayment);
        intent.putExtra("service_date_new",service_date_new);
        intent.putExtra("day_service_id",day_service_id);
        startActivity(intent);
    }

    private void onPageSuccess() {
        Intent intent = new Intent(getApplicationContext(), PaymentSuccessActivity.class);
        intent.putExtra("transStatus", "success");
        intent.putExtra("price", amount);
        intent.putExtra("order_id",orderId);
        intent.putExtra("tracking_id",trackingId);
        intent.putExtra("from",From);
        intent.putExtra("modeOfPayment",modeOfPayment);
        intent.putExtra("service_date_new",service_date_new);
        intent.putExtra("day_service_id",day_service_id);
        startActivity(intent);
    }

    private void callWebViewPay(String url, byte[] bytes) {

        payWebView.postUrl(url,bytes);
    }

    private void init() {
        payWebView = findViewById(R.id.payWebView);
        backButton = findViewById(R.id.backbutton);
//        closeButton = findViewById(R.id.close_button);
        help_lay = findViewById(R.id.help_lay);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            amount = extras.getString("price");
            booking_id = extras.getString("booking_id");
            From = extras.getString("from");
            service_date_new = extras.getString("service_date_new");
            day_service_id = extras.getString("day_service_id");
            description = extras.getString("description");
            reference_id = extras.getString("reference_id");

            Log.e("Log3",service_date_new);
            Log.e("Log4",day_service_id);

        }
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(PaymentWebViewActivity.this)
                .setMessage("Are you sure you want to cancel this payment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        EventBus.getDefault().post(new PaymentWebViewActivity.finishEvent());
                        EventBus.getDefault().post(new BookedDetailActivity.finishEvent());
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentWebViewActivity.finishEvent event) {
        finish();
    }

    public static class finishEvent{

    }




}