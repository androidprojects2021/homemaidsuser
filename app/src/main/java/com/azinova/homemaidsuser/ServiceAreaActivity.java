package com.azinova.homemaidsuser;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.azinova.homemaidsuser.adapters.ServiceAreaAdapter;
import com.azinova.homemaidsuser.models.Area;
import com.azinova.homemaidsuser.utils.Shared;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceAreaActivity extends AppCompatActivity implements OnMapReadyCallback, ServiceAreaAdapter.AdapterCallback {

    public static int pos = -1;
    private final String TAG = "ServiceAreaActivity";
    @BindView(R.id.hiserrorlay)
    LinearLayout errorlayout;
    @BindView(R.id.service_area_layout)
    LinearLayout service_area_layout;
    @BindView(R.id.help_lay)
    RelativeLayout help_lay;
    private AppBarLayout appBarLayout;
    private GoogleMap googleMap;
    private RecyclerView areaListRecyc;
    private List<Area> areas;
    private ServiceAreaAdapter adapter;
    private LatLngBounds.Builder builder;
    private BitmapDescriptor markerbitmap;

    private Boolean is_latlng_available=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_area);
        ButterKnife.bind(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        pos = -1;
        appBarLayout = findViewById(R.id.appbar);
        areaListRecyc = findViewById(R.id.servicearealist);

        service_area_layout.setVisibility(View.INVISIBLE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        areas = new ArrayList<>();

        initAdapter();

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(ServiceAreaActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

        builder = new LatLngBounds.Builder(); //for bound

    }

    public void initAdapter() {
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(ServiceAreaActivity.this, R.anim.layout_animation_fall_down); //recyclerview animation
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        areaListRecyc.setLayoutManager(mLayoutManager);
        areaListRecyc.setLayoutAnimation(controller);

    }

    public void setMarkerData() {
        errorlayout.setVisibility(View.GONE);

            areas.addAll(Shared.listModel.getAreas());
            adapter = new ServiceAreaAdapter(this, areas);
            areaListRecyc.setAdapter(adapter);

            service_area_layout.setVisibility(View.VISIBLE);
        Log.e(TAG, "setMarkerData: " + Shared.listModel.getAreas().get(1).getLatitude());

            new Thread(new Runnable() {
                public void run() {
                    // a time consuming task
                    for (int i = 0; i < areas.size(); i++) {
                        if (areas.get(i).getLatitude().isEmpty() || areas.get(i).getLongitude().isEmpty()) {
                            is_latlng_available=false;
                            continue;

                        } else {
                            is_latlng_available=true;



                            final LatLng latlng = new LatLng(Double.parseDouble(areas.get(i).getLatitude()), Double.parseDouble(areas.get(i).getLongitude()));
                            final String areanam = areas.get(i).getAreaName();
                            markerbitmap = BitmapDescriptorFactory.fromBitmap
                                    (getBitmapFromVectorDrawable(ServiceAreaActivity.this, R.drawable.ic_home_maid_location_indicator_icon_01_02_2018));

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    addMarker(latlng,areanam,markerbitmap);
                                    if (is_latlng_available) {
                                        LatLngBounds bounds = builder.build();
                                        int padding = 0; // offset from edges of the map in pixels
                                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
                                        googleMap.moveCamera(cu);
                                        googleMap.animateCamera(zoom);
                                    }
                                }
                            });

                        }

                    }

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (is_latlng_available) {
//                                    LatLngBounds bounds = builder.build();
//                                    int padding = 0; // offset from edges of the map in pixels
//                                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//                                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);
//                                    googleMap.moveCamera(cu);
//                                    googleMap.animateCamera(zoom);
//                                }
//                            }});





                }
            }).start();

    }


    public void retryClick(View view) {
        setMarkerData();
    }

    public void backClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Snackbar.make(appBarLayout, "Location permission denied", Snackbar.LENGTH_SHORT).show();
                }
                return;

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;
        googleMap.getUiSettings().setAllGesturesEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        LatLng latlngs = new LatLng(25.087015,55.225513);
        CameraPosition position = new CameraPosition.Builder().target(latlngs)
                .zoom(5)
                .build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
        googleMap.animateCamera(update);
        googleMap.moveCamera(update);
        setMarkerData();

    }

    private void addMarker(LatLng latLng, String title, BitmapDescriptor markerbitmap) {
        MarkerOptions options = new MarkerOptions();
        options.title(title);
        options.position(latLng);
        options.icon(markerbitmap);
        googleMap.addMarker(options);
        builder.include(latLng); //for setting bound
    }

    @Override
    public void onMethodCallback(LatLng latLng, String title) {
        MarkerOptions options = new MarkerOptions();
        options.title(title);
        options.position(latLng);

        options.icon(markerbitmap);
        googleMap.addMarker(options).showInfoWindow();
        CameraPosition position = new CameraPosition.Builder().target(latLng)
                .zoom(15)
                .build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
        googleMap.animateCamera(update);
    }

    private Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}
