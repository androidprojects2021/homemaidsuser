package com.azinova.homemaidsuser.utils;

import com.azinova.homemaidsuser.models.AreaListModel;
import com.azinova.homemaidsuser.models.OfferModel;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by azinova-mac6 on 04/12/17.
 */

public class Shared {

    public static JSONObject bookingdetails = new JSONObject();

    public static AreaListModel listModel = null;

    public static OfferModel offermodel = null;

    public static String sharedareaid = null;

    public static String offertitle = null;

    public static String offerdetails = null;

    public static JSONArray times = null;

    public static String mobile_number = null;

    public static String pay_bookingid = null;

    public static String pay_total = null;

}
