package com.azinova.homemaidsuser.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.azinova.homemaidsuser.models.CustomerModels;
import com.google.gson.Gson;

/**
 * Created by Leo elstin on 15/03/17.
 */

public class Prefs {

    private static final String PRE_LOAD = "preLoad";
    private static final String PRE_RESET = "preReset";
    private static final String PRE_REGID = "preRegID";
    private static final String PREFS_NAME = "homemaidspref";
    private static final String PRE_NAME = "prefname";
    private static final String PRE_EMAIL = "prefemail";
    private static final String PRE_FACEBOOK = "preffacebook";
    private static final String PRE_ADDRESS = "prefaddress";
    private static final String PRE_ImageURlL = "prefimage";
    private static final String PRE_ID = "prefid";
    private static final String PRE_VERI = "prefveri";
    private static final String PRE_TOKEN = "preToken";
    private static final String PRE_EMIRATES = "preEmiratesIsDubai";
    private static final String PRE_EMIR_SELECT = "preEmiratesSelected";
    private static final String PRE_BASE_URL = "preBaseUrl";
    private static Prefs instance;
    private final SharedPreferences sharedPreferences;
    private String PRE_UserDetails = "preUserDetails";

    public Prefs(Context context) {

        sharedPreferences = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static void clearSession(Context context) {
        final SharedPreferences.Editor homemaidsEditor = context.getApplicationContext().getSharedPreferences(PREFS_NAME,
                Context.MODE_PRIVATE).edit();
        homemaidsEditor.clear();
        homemaidsEditor.commit();
    }

    public static Prefs with(Context context) {

        if (instance == null) {
            instance = new Prefs(context);
        }
        return instance;
    }

    public void setReset(boolean signedin) {

        sharedPreferences
                .edit()
                .putBoolean(PRE_RESET, signedin)
                .apply();
    }

    public void setSignedin(boolean signedin) {

        sharedPreferences
                .edit()
                .putBoolean(PRE_LOAD, signedin)
                .apply();
    }

    public void setImageUrl(String url) {
        sharedPreferences
                .edit()
                .putString(PRE_ImageURlL, url)
                .apply();
    }

    public String getName() {
        return sharedPreferences.getString(PRE_NAME, "");
    }

    public void setName(String name) {

        sharedPreferences
                .edit()
                .putString(PRE_NAME, name)
                .apply();
    }

    public String getPreRegid() {
        return sharedPreferences.getString(PRE_REGID, "");
    }

    public void setPreRegid(String name) {

        sharedPreferences
                .edit()
                .putString(PRE_REGID, name)
                .apply();
    }

    public String getEmail() {
        return sharedPreferences.getString(PRE_EMAIL, "");
    }

    public void setEmail(String email) {

        sharedPreferences
                .edit()
                .putString(PRE_EMAIL, email)
                .apply();
    }

    public boolean getisfacebook() {
        return sharedPreferences.getBoolean(PRE_FACEBOOK, false);
    }

    public void setisfacebook(boolean isfacebook) {

        sharedPreferences
                .edit()
                .putBoolean(PRE_FACEBOOK, isfacebook)
                .apply();
    }

    public String getAddress() {
        return sharedPreferences.getString(PRE_ADDRESS, "");
    }

    public void setAddress(String address) {
        sharedPreferences
                .edit()
                .putString(PRE_ADDRESS, address)
                .apply();
    }

    public String getmageURlL() {
        return sharedPreferences.getString(PRE_ImageURlL, null);
    }

    public boolean getSigned() {
        return sharedPreferences.getBoolean(PRE_LOAD, false);
    }

    public boolean getReset() {
        return sharedPreferences.getBoolean(PRE_RESET, false);
    }

    public String getCustomerId() {
        return sharedPreferences.getString(PRE_ID, null);
    }

    public void setCustomerId(String id) {
        sharedPreferences
                .edit()
                .putString(PRE_ID, id)
                .apply();
    }

    public void setisverfied(boolean isveri) {
        sharedPreferences
                .edit()
                .putBoolean(PRE_VERI, isveri)
                .apply();
    }

    public boolean getisverfied() {
        return sharedPreferences.getBoolean(PRE_VERI, true);
    }

    public String getFCMTOKEN() {
        return sharedPreferences.getString(PRE_TOKEN, null);
    }

    public void setFCMTOKEN(String name) {

        sharedPreferences
                .edit()
                .putString(PRE_TOKEN, name)
                .apply();
    }

    public CustomerModels getUserDetails() {
        Gson gson = new Gson();
        CustomerModels cus = gson.fromJson(sharedPreferences.getString(PRE_UserDetails, null), CustomerModels.class);
        return cus;
    }

    public void setUserDetails(String name) {

        sharedPreferences
                .edit()
                .putString(PRE_UserDetails, name)
                .apply();
    }

    public void setEmiratesIs_Dubai(Boolean dubai) {
        sharedPreferences
                .edit()
                .putBoolean(PRE_EMIRATES, dubai)
                .apply();
    }

    public Boolean getEmiratesIs_Dubai() {
        return sharedPreferences.getBoolean(PRE_EMIRATES, false);
    }

    public void setIsEmiratesSelected(Boolean sel) {
        sharedPreferences
                .edit()
                .putBoolean(PRE_EMIR_SELECT, sel)
                .apply();
    }

    public Boolean getIsEmiratesSelected() {
        return sharedPreferences.getBoolean(PRE_EMIR_SELECT, false);
    }

}
