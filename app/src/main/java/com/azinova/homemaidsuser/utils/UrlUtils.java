package com.azinova.homemaidsuser.utils;

import android.util.Log;

import com.azinova.homemaidsuser.BuildConfig;

/**
 * ABGthDev
 */

public class UrlUtils {

    public static final int DEFAULT_TIMEOUT = 15000; //todo API call timeout duration
    public static final String TAG = "UrlUtils";

    private static String AUTH = BuildConfig.API_AUTH_KEY;

//    public static String DUBALURL = "https://www.homemaids.ae/api/";

    public static String DUBALURL = "http://limshq.fortiddns.com:8090/new/customerappapi/";
    public static String ABUDHABIURL = "http://limshq.fortiddns.com:8090/new/customerappapi_abudhabi/";

//    public static String ABUDHABIURL = "https://www.homemaids.ae/api_abudhabi/";

//    public static String DUBALURLDEMO = "https://www.homemaids.ae/homemaids_new_210717/api/";
//    public static String ABUDHABIURLDEMO = "https://www.homemaids.ae/homemaids_new_210717/api_abudhabi/";

    public static String DUBALURLDEMO = "http://limshq.fortiddns.com:8090/admin/customerappapi/"; //Demo
    public static String ABUDHABIURLDEMO = "http://limshq.fortiddns.com:8090/admin/customerappapi_abudhabi/"; //Demo




    /*public static String ABUDHABIURLDEMO = "http://limshq.fortiddns.com:8090/admin/api_abudhabi/";*/




    public static String BASEURL;

    public static void setBaseUrl(Boolean is_dubai) {
        if (is_dubai) {
            Log.e(TAG, "setBaseUrl: dubai");
            BASEURL = DUBALURL;  // Live


        } else {
            Log.e(TAG, "setBaseUrl: abu dubai");
            BASEURL = ABUDHABIURL;  // Live

        }
    }

    public static String removeNull(String URL1) {
        String URL = URL1.replace(" ", "%20").trim();
        LogUtil.d("************" + URL);
        return URL;
    }

    public static String customerLogin() {
        String URL = BASEURL + "customerLogin?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String customerRegister() {
        String URL = BASEURL + "cust_registration?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String userDetails() {
        String URL = BASEURL + "userDetails?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String editProfile() {
        String URL = BASEURL + "editUserDetails?ws=" + AUTH;
        return removeNull(URL);
    }

/*    public static String getBookingHistory() {
        String URL = BASEURL + "bookingHistory_new?ws=" + AUTH;
        return removeNull(URL);
    } */

    public static String getBookingHistory() {
        String URL = BASEURL + "bookingHistory_newest?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getGalleryDetails() {
        String URL = BASEURL + "gallery?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getServiceList() {
        String URL = BASEURL + "servicesList?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getAreaList() {
        String URL = BASEURL + "getAreas?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getOffer() {
        String URL = BASEURL + "offers?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String ForgotPass(String email) {
        String URL = BASEURL + "forgotpassword?ws=" + AUTH + "&email=" + email;
        return removeNull(URL);
    }

    public static String getTime(String date) {
        String URL = BASEURL + "getBookingTime?date=" + date;
        return removeNull(URL);
    }

    public static String changePassword() {
        String URL = BASEURL + "change_password?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String priceCalculator() {
        String URL = BASEURL + "calculate_booking?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String bookingShift() {
        String URL = BASEURL + "add_booking?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String requestSms() {
        String URL = BASEURL + "send_otp?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String verifyotp() {
        String URL = BASEURL + "mobile_verify?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String rating() {
        String URL = BASEURL + "add_rating?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String contactus() {
        String URL = BASEURL + "contact_us?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String helplist() {
        String URL = BASEURL + "helpList?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String notifications() {
        String URL = BASEURL + "user_notifications?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String help() {
        String URL = BASEURL + "customer_feedback?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getOrderId() {
        String URL = BASEURL + "add_payment?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getPaymentDetail() {
        String URL = BASEURL + "track_payment_details?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String homenotifications() {
        String URL = BASEURL + "get_user_notifications_unread?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String notificationscount() {
        String URL = BASEURL + "get_user_notifications_count?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getoutstandingAmount() {
        String URL = BASEURL + "get_customer_outstanding_amount?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String payoutstandingAmount() {
        String URL = BASEURL + "make_customer_payment?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getplaystoreversion() {
        String URL = BASEURL + "check_playstore_version?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String userDataToServer() {
        String URL = BASEURL + "temp_customer_details?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String priceDetailApi() {
        String URL = BASEURL + "price_list?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getRatingList() {
        String URL = BASEURL + "rating_list?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String getComplaintsList() {
        String URL = BASEURL + "complaint_list?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String ratingSubmit() {
        String URL = BASEURL + "rating_add?ws=" + AUTH;
        return removeNull(URL);
    }

    public static String updateTransaction() {
        String URL = BASEURL + "update_payment_transaction?ws=" + AUTH;
        return removeNull(URL);
    }
}




