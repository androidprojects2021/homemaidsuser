package com.azinova.homemaidsuser.utils;

import com.azinova.homemaidsuser.BuildConfig;

/**
 * Created by azinova-mac6 on 16/01/18.
 */

public class PaymentUtils {

    /**
     * LIVE PAYMENT CREDENTIALS
     **/
    public static String AccessCode = BuildConfig.ACCESS_CODE; //live url
    public static String MerchantID = BuildConfig.MERCHANT_ID; //live url

    public static String RedirectUrl = "https://www.homemaids.ae/mobile_pay/ccavResponseHandler.php"; //live url
    public static String RSA_URL = "https://www.homemaids.ae/mobile_pay/GetRSA.php"; //live url

//     DEMO CREDENTIALS

//    public static String AccessCode = "AVRS02EJ65AY00SRYA";
//    public static String MerchantID = "43551";
//
//    public static String RedirectUrl = "https://secure.ccavenue.ae/transaction/jsp/ResponseHandler_43551.jsp";
//    public static String RSA_URL = "https://secure.ccavenue.ae/transaction/jsp/GetRSA.jsp";

}
