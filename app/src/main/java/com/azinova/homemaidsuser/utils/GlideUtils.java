package com.azinova.homemaidsuser.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Leo elstin on 11/15/17.
 */

public class GlideUtils {

    public static void loadImage(String url, ImageView view, Context context) {
        Glide.with(context)
                .load(url)
                .apply(RequestOptions.centerCropTransform())
                .into(view);
    }
}
