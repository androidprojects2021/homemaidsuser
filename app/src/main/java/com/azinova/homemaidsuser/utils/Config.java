package com.azinova.homemaidsuser.utils;

public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global"; //// TODO: 04/01/18 LIVE TOPIC PUSH
//    public static final String TOPIC_GLOBAL = "globaltest"; //// TODO: 19/12/17  Demo Topic Push

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

}
