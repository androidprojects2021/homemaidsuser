package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.NotificationAdapter;
import com.azinova.homemaidsuser.models.NotifDatum;
import com.azinova.homemaidsuser.models.NotificationModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.NotificationUtils;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class NotificationActivity extends AppCompatActivity {

    public final String TAG = "message";
    @BindView(R.id.notification_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.help_lay)
    RelativeLayout help;
    private TextView hiserrorText;
    private LinearLayout hiserrorLay;
    private LottieAnimationView animationView;
    private List<NotifDatum> notifData = new ArrayList<>();
    private NotificationAdapter adapter;
    private ProgressDialog pDialog;
    private boolean isfromsplash;
    private Prefs prefs;
    private SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        prefs = Prefs.with(this);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isfromsplash = extras.getBoolean("isfromsplash", false);
        }

        ButterKnife.bind(this);
        initView();
        fetchNotfications();
        initAdapter();

        NotificationUtils.clearNotifications(getApplicationContext());

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(NotificationActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        fetchNotfications();
                    }
                }
        );



    }

    private void initAdapter() {
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        if (adapter == null) {
            adapter = new NotificationAdapter(notifData, this);
            recyclerView.setAdapter(adapter);

        }
        recyclerView.setLayoutAnimation(controller);

    }

    public void backClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (isfromsplash) {
            Intent i = new Intent(NotificationActivity.this, MainActivity.class);
            startActivity(i);
        }
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }

    private void initView() {

        animationView = findViewById(R.id.animation_view);
        hiserrorText = findViewById(R.id.hiserrortext);
        hiserrorLay = findViewById(R.id.hiserrorlay);
        swiperefresh  =findViewById(R.id.swiperefresh);
        animationView.setAnimation("error_animation.json");
        animationView.loop(true);

    }

    private void fetchNotfications() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();


        recyclerView.setVisibility(View.VISIBLE);
        hiserrorLay.setVisibility(View.GONE);

        if (animationView.isAnimating())
            animationView.cancelAnimation();
        JSONObject jsonObject = new JSONObject();
        try {
            if(prefs.getSigned())
            jsonObject.put("customerId", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest notfRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.notifications()
                , jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                swiperefresh.setRefreshing(false);
                Log.e(TAG, "onResponse: " + response.toString());
                try {

                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failure")) {
                        recyclerView.setVisibility(View.GONE);
                        hiserrorLay.setVisibility(View.VISIBLE);
                        hiserrorText.setText(response.getString("message"));
                        animationView.playAnimation();
                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new Gson();
                        NotificationModel model = gson.fromJson(response.toString(), NotificationModel.class);
                        notifData.clear();
                        notifData.addAll(model.getNotifData());
                        adapter.notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();

                    } else {
                        recyclerView.setVisibility(View.GONE);
                        animationView.playAnimation();
                        hiserrorLay.setVisibility(View.VISIBLE);
                        hiserrorText.setText(response.getString("message"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                swiperefresh.setRefreshing(false);
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                recyclerView.setVisibility(View.GONE);
                animationView.playAnimation();
                hiserrorLay.setVisibility(View.VISIBLE);
                hiserrorText.setText(message);

            }
        });
        notfRequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(notfRequest);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Refreshevent event) {
        fetchNotfications();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

//    public void retryClicker(View view) {
//        fetchNotfications();
//    }

    public static class Refreshevent {

    }
}
