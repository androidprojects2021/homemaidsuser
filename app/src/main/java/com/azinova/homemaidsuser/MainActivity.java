package com.azinova.homemaidsuser;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.AreaCustomArrayAdapter;
import com.azinova.homemaidsuser.adapters.CustomArrayAdapter;
import com.azinova.homemaidsuser.fragments.PopNotificationFragment;
import com.azinova.homemaidsuser.models.Area;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.Config;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.Shared;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.messaging.FirebaseMessaging;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.azinova.homemaidsuser.utils.Shared.bookingdetails;
import static com.azinova.homemaidsuser.utils.Shared.listModel;
import static com.azinova.homemaidsuser.utils.Shared.offerdetails;
import static com.azinova.homemaidsuser.utils.Shared.offertitle;
import static com.azinova.homemaidsuser.utils.Shared.sharedareaid;
import static com.azinova.homemaidsuser.utils.Shared.times;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    //com.azinova.homemaid.Homepage previous fb classs name

    private final String TAG = "MainActivity";
    Dialog dialog;
    private TextView dateText, login_button, register_button;
    private int mYear, mMonth, mDay;
    private AutoCompleteTextView spinner, from_spinner, to_spinner;
    private Calendar cal = null;
    private SimpleDateFormat format1 = null;
    private RelativeLayout bookButton;
    private Toolbar toolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private LinearLayout bookinghistorydraw;
    private LinearLayout serviceareadraw;
    private LinearLayout servicesdraw;
    private LinearLayout gallerydraw;
    private LinearLayout contactdraw;
    private LinearLayout aboutusdraw;
    private RelativeLayout logoutlaydraw;
    private RelativeLayout hamIcon;
    private RelativeLayout viewProfile;
    private RelativeLayout dateLay;
    private TextView offerText;
    private CustomArrayAdapter todataAdapter;
    private Prefs prefs;
    private LinearLayout bottomLay;
    private TextView profilename;
    private TextView viewprofiletext;
    private ImageView profileImage;
    private List<String> totime;
    private List<String> parentime;
    private List<String> fromtime;
    private String fromtimer = null;
    private String totimer = null;
    private RelativeLayout areaLayer, mobileLayer;
    private List<Area> areas;
    private TextView offerDetails;
    private LinearLayout offerLay;
    private ProgressBar progress;
    private LinearLayout notificationdraw;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private int dayOfWeek;
    private EditText mobile;
    //    private String area_id = null;
    private RelativeLayout help_lay;
    private LinearLayout hi_linear;
    private RelativeLayout hi_content;
    private TextView hi_text;
    private LinearLayout chat;
    private List<Area> search_area;
    //    private RelativeLayout call_now;
    private ListView listView;
    private RelativeLayout call_now;
    private ImageView fb, twitter, linkedin, gplus;
    private FloatingActionButton chathead;
    private PopNotificationFragment firstFragment;
    private RelativeLayout chatheader;
    private LinearLayout makepaylay;
    private RelativeLayout badgelay;
    private TextView badgecount;
    private int version;

    private LinearLayout linearChangeEmirates;
    private TextView emirates_name;

    public MainActivity() {
        search_area = new ArrayList<>();
        totime = new ArrayList<>();
        parentime = new ArrayList<>();
        fromtime = new ArrayList<>();
        areas = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        prefs = Prefs.with(this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        //// TODO: 15/12/17 pls check the address and area id inn the booking api now its dummy 

        initView();
        clickListeners();
        fromtoSpinnerEngine();
        initCalendar();
        setUpToolbar();
        navigationItemsInit();
        navigationDrawerEngine();
        navigationClickListteners();

        EventBus.getDefault().post(new LoginUiUpdate(true)); //update the ui according to login conditions

//        String refreshedToken = FirebaseInstanceId.getInstance().getToken(); //// TODO: 12/12/17 need to remove this code after testing push
//        LogUtil.e(TAG, "Refreshed token: " + refreshedToken);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

//                    String refreshedToken = FirebaseInstanceId.getInstance().getToken(); //// TODO: 12/12/17 need to remove this code after testing push
//                    LogUtil.e(TAG, "Refreshed token: " + refreshedToken);

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    Toast.makeText(context, "new notification", Toast.LENGTH_SHORT).show();

                }
            }
        };

        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

        Shared.mobile_number = null;
        sharedareaid = null;

        EventBus.getDefault().post(new MessageBadgeEvent());

        versionCheck();

        if (!prefs.getReset()) {
            Prefs.clearSession(MainActivity.this);

            if (AccessToken.getCurrentAccessToken() != null)
                LoginManager.getInstance().logOut();

            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage(getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("EXIT", true);
            startActivity(i);
            ActivityCompat.finishAffinity(MainActivity.this);

            prefs.setReset(true);
        }

    }

    private void delayCall(String name) {
        hi_text.setText("Hi, " + name);
        final Transition slide_left = new Slide(Gravity.LEFT);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                com.transitionseverywhere.TransitionManager.beginDelayedTransition(hi_linear, slide_left);
                hi_content.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        com.transitionseverywhere.TransitionManager.beginDelayedTransition(hi_linear, slide_left);
                        hi_content.setVisibility(View.INVISIBLE);

                    }
                }, 3000);

            }
        }, 800);

    }

    private void navigationClickListteners() {

        hamIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        bookinghistorydraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent boohis = new Intent(MainActivity.this, BookingHistoryActivity.class);
                startActivity(boohis);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        serviceareadraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serarea = new Intent(MainActivity.this, ServiceAreaActivity.class);
                startActivity(serarea);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        servicesdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serlist = new Intent(MainActivity.this, ServiceListActvity.class);
                startActivity(serlist);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        gallerydraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gal = new Intent(MainActivity.this, GalleryActivity.class);
                startActivity(gal);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getSigned()) {
                    Intent pro = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(pro);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } else {
                    Intent reg = new Intent(MainActivity.this, RegLogActivity.class);
                    reg.putExtra("isfromlogin", true);
                    startActivity(reg);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }

                DrawercloserEngine();
            }
        });

        contactdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent con = new Intent(MainActivity.this, ContactUsActivity.class);
                startActivity(con);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        aboutusdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent about = new Intent(MainActivity.this, AboutUsActivity.class);
                startActivity(about);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        logoutlaydraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setMessage("Are you sure you want to logout?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Prefs.clearSession(MainActivity.this);

                                if (AccessToken.getCurrentAccessToken() != null)
                                    LoginManager.getInstance().logOut();

                                Intent i = getBaseContext().getPackageManager()
                                        .getLaunchIntentForPackage(getBaseContext().getPackageName());
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                i.putExtra("EXIT", true);
                                startActivity(i);
                                ActivityCompat.finishAffinity(MainActivity.this);
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        notificationdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent noti = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(noti);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });


        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chat = new Intent(MainActivity.this, ChatActivity.class);
                startActivity(chat);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                DrawercloserEngine();
            }
        });

        makepaylay.setOnClickListener(v -> {
            Intent makepay = new Intent(MainActivity.this, MakePaymentActivity.class);
            startActivity(makepay);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            DrawercloserEngine();
        });

        linearChangeEmirates.setOnClickListener(v -> alertDialogEmirates());


    }

    private void alertDialogEmirates() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                .setCancelable(true)
                .setTitle("Change Emirates")
                .setMessage("Are you want to change Emirates ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Prefs.clearSession(MainActivity.this);

                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();

                        Intent i = getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i.putExtra("EXIT", true);
                        startActivity(i);
                        ActivityCompat.finishAffinity(MainActivity.this);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Navigation drawer items init (for sample now)
     **/
    private void navigationItemsInit() {
        hamIcon = findViewById(R.id.ham_icon);
        bookinghistorydraw = findViewById(R.id.bookinghistory);
        serviceareadraw = findViewById(R.id.service_area);
        viewProfile = findViewById(R.id.viewProfile);
        contactdraw = findViewById(R.id.contact_us);
        profilename = findViewById(R.id.profileName);
        logoutlaydraw = findViewById(R.id.logoutlay);
        servicesdraw = findViewById(R.id.services);
        gallerydraw = findViewById(R.id.gallery);
        viewprofiletext = findViewById(R.id.viewprofiletext);
        profileImage = findViewById(R.id.profilePic);
        aboutusdraw = findViewById(R.id.about_us);
        notificationdraw = findViewById(R.id.notification);
        makepaylay = findViewById(R.id.makepaylay);
        chat = findViewById(R.id.chat);
        linearChangeEmirates = findViewById(R.id.linear_change_emirates);
        emirates_name = findViewById(R.id.tv_emirates);

        linearChangeEmirates.setVisibility(View.INVISIBLE);

        if (prefs.getEmiratesIs_Dubai()) {
            emirates_name.setText("Dubai");
        } else {
            emirates_name.setText("Abu Dhabi");
        }


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }


    private void navigationDrawerEngine() {
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(false);

    }

    private void initCalendar() {
        //get date
        cal = Calendar.getInstance();
        Log.d("TAG", "initCalendar: "+cal.get(Calendar.DAY_OF_WEEK)+", "+Calendar.SATURDAY);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) { //THURSDAY
            cal.add(Calendar.DATE, 2);
        } else {
            cal.add(Calendar.DATE, 1);
        }
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);
        mDay = cal.get(Calendar.DAY_OF_MONTH);

        format1 = new SimpleDateFormat("dd/MM/yyyy");
        dateText.setText(format1.format(cal.getTime()));
        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
    }

    private void fromtoSpinnerEngine() {

        try {
            parentime.clear();
            fromtime.clear();
            totime.clear();
            for (int i = 0; i < times.length(); i++)
                parentime.add(times.getString(i));

            fromtime.addAll(parentime);

            if (fromtime.size() < 2) {
                LogUtil.e("inside if");
                return;
            }

            LogUtil.e("outside if");
            fromtime.remove(parentime.size() - 1);

            totime.addAll(parentime);
            totime.remove(0);

            fromtimer = fromtime.get(0); //default time 08.00am
//            if (totime.size() == 0) {
//                totimer = totime.get(0);
//            } else {
//                totimer = totime.get(1); //default time 12.00pm
//            }

            if (parentime.size() > 5) {
                totimer = totime.get(1); //default time 12.00pm
            } else {
                totimer = totime.get(0);
            }

            // Creating adapter for spinner
            CustomArrayAdapter fromdataAdapter = new CustomArrayAdapter(this,
                    R.layout.custom_spinner_item, fromtime);

            todataAdapter = new CustomArrayAdapter(this,
                    R.layout.custom_spinner_item, totime);

            fromdataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
            todataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
            // attaching data adapter to spinner
            from_spinner.setThreshold(100);
            to_spinner.setThreshold(100);
            from_spinner.setAdapter(fromdataAdapter);
            to_spinner.setAdapter(todataAdapter);


            from_spinner.setText(fromtime.get(0));//08.00 default time
            if (parentime.size() > 5) {
                to_spinner.setText(totime.get(1)); //12.00 default time
            } else {
                to_spinner.setText(totime.get(0));
            }

            RelativeLayout ii = findViewById(R.id.main_container);
            DisplayMetrics metrics = ii.getResources().getDisplayMetrics();
            float dp = 150f;
            float fpixels = metrics.density * dp;
            final int pixels = (int) (fpixels + 0.5f);

            if (totime.size() < 4) {
                to_spinner.setDropDownHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            } else {
                to_spinner.setDropDownHeight(pixels);//250
            }

            if (fromtime.size() < 4) {
                from_spinner.setDropDownHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
            } else {
                from_spinner.setDropDownHeight(pixels);
            }

            //// TODO: 24/11/17 man!... need to work here (under testing phase)
            from_spinner.setOnItemClickListener((parent, view, position, id) -> {

                totime.clear();
                for (int i = position + 1; i < parentime.size(); i++) {
                    totime.add(parentime.get(i));
                }

                todataAdapter = new CustomArrayAdapter(MainActivity.this,
                        R.layout.custom_spinner_item, totime);
                todataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
                to_spinner.setAdapter(todataAdapter);
                to_spinner.setText(totime.get(0));
                if (totime.size() < 4) {
                    to_spinner.setDropDownHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                } else {
                    to_spinner.setDropDownHeight(pixels);
                }

                fromtimer = fromtime.get(position);
                totimer = totime.get(0);
            });

            to_spinner.setOnItemClickListener((parent, view, position, id) -> totimer = totime.get(position));

        } catch (Exception e) {
            e.printStackTrace();

            LogUtil.e("error occured" + e.toString());
        }

    }


    @SuppressLint("ClickableViewAccessibility")
    private void clickListeners() {


        dateLay.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, MainActivity.this, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();
        });

        spinner.setOnClickListener(v -> areaDialog());

        from_spinner.setOnClickListener(v -> {
            if (parentime.size() > 1) {
                if (from_spinner.isPopupShowing()) {
                    from_spinner.dismissDropDown();
                } else {
                    from_spinner.showDropDown();
                }
            }

        });

        to_spinner.setOnClickListener(v -> {
            if (parentime.size() > 1) {
                if (to_spinner.isPopupShowing()) {
                    to_spinner.dismissDropDown();
                } else {
                    to_spinner.showDropDown();
                }
            }
        });

        register_button.setOnClickListener(v -> {
            Intent reg = new Intent(MainActivity.this, RegLogActivity.class);
            reg.putExtra("isfromlogin", false);
            startActivity(reg);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        });

        login_button.setOnClickListener(v -> {
            Intent log = new Intent(MainActivity.this, RegLogActivity.class);
            log.putExtra("isfromlogin", true);
            startActivity(log);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        });

        bookButton.setOnClickListener(v -> {

            if (times == null || times.length() == 0) {
                return;
            }

            try {
                bookingdetails = new JSONObject();
                bookingdetails.put("date", dateText.getText().toString());
                bookingdetails.put("from_time", fromtimer);
                bookingdetails.put("to_time", totimer);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (!prefs.getSigned()) {
                Shared.mobile_number = mobile.getText().toString();
                if (sharedareaid == null) {
                    Snackbar.make(bookButton, "Please select your area", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                if (Shared.mobile_number.isEmpty()) {
                    Snackbar.make(bottomLay, "Please enter your mobile number", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                if (Shared.mobile_number.length() < 6) {
                    Snackbar.make(bottomLay, "Please enter valid mobile number", Snackbar.LENGTH_SHORT).show();
                    return;
                }

            }

            Intent book = new Intent(MainActivity.this, BookNowActivity.class);
            book.putExtra("dayofweek", dayOfWeek);
            if (!prefs.getSigned()) {
                dataToServer();

            }

            startActivity(book);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        });

        help_lay.setOnClickListener(v -> {
            Intent help = new Intent(MainActivity.this, HelpActivity.class);
            startActivity(help);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        });

        call_now.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            if (prefs.getEmiratesIs_Dubai()) {
                intent.setData(Uri.parse("tel:8006243"));

            } else {
                intent.setData(Uri.parse("tel:024452352"));
            }
//                intent.setData(Uri.parse("tel:8006243"));
            startActivity(intent);
        });

        fb.setOnClickListener(view -> {
            Intent hm_intent = new Intent(Intent.ACTION_VIEW);
            hm_intent.setData(Uri.parse("https://www.facebook.com/homemaidsdubai"));
            startActivity(hm_intent);
        });
        twitter.setOnClickListener(view -> {
            Intent hm_intent = new Intent(Intent.ACTION_VIEW);
            hm_intent.setData(Uri.parse("https://twitter.com/homemaidsdubai"));
            startActivity(hm_intent);
        });
        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hm_intent = new Intent(Intent.ACTION_VIEW);
                hm_intent.setData(Uri.parse("https://www.linkedin.com/company/homemaids-llc"));
                startActivity(hm_intent);
            }
        });
        gplus.setOnClickListener(view -> {
            Intent hm_intent = new Intent(Intent.ACTION_VIEW);
            hm_intent.setData(Uri.parse("https://plus.google.com/+HomemaidsAe-dubai"));
            startActivity(hm_intent);
        });

        chathead.setOnClickListener(v -> {
            if (findViewById(R.id.fragment_container) != null) {

                if (firstFragment == null) {  // not added
                    firstFragment = new PopNotificationFragment();
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragment_container, firstFragment).commit();
                    chathead.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_close_black));
                    badgelay.setVisibility(View.GONE);
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .remove(firstFragment).commit();
                    firstFragment = null;
                    chathead.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.noti_icon));
                }

            }

        });


        mobileLayer.requestFocus();
        mobile.setFocusable(false);

        mobile.setOnTouchListener((v, event) -> {

            v.setFocusable(true);
            v.setFocusableInTouchMode(true);
            return false;
        });

        mobile.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                if (mobile.getText().toString().isEmpty())
                    mobile.setText("+971");
            } else {

            }
        });

    }

    private void initView() {
        dateText = findViewById(R.id.date_text);
        spinner = findViewById(R.id.spinner);
        from_spinner = findViewById(R.id.from_spinner);
        to_spinner = findViewById(R.id.to_spinner);
        register_button = findViewById(R.id.register_button);
        login_button = findViewById(R.id.login_button);
        bookButton = findViewById(R.id.book_button);
        toolbar = findViewById(R.id.toolbar);
        offerText = findViewById(R.id.offertext);
        offerDetails = findViewById(R.id.offertitletext);
        dateLay = findViewById(R.id.date_lay);
        bottomLay = findViewById(R.id.bottomlay);
        areaLayer = findViewById(R.id.area_layer);
        mobileLayer = findViewById(R.id.mobile_layer);
        offerLay = findViewById(R.id.offerLay);
        progress = findViewById(R.id.progress);
        mobile = findViewById(R.id.mobile);
        help_lay = findViewById(R.id.help_lay);
        hi_linear = findViewById(R.id.hi_linear);
        hi_content = findViewById(R.id.hi_content_relative);
        hi_text = findViewById(R.id.hi_text);
        call_now = findViewById(R.id.call_now);
        fb = findViewById(R.id.hm_fb);
        twitter = findViewById(R.id.hm_twitter);
        linkedin = findViewById(R.id.hm_linkedin);
        gplus = findViewById(R.id.hm_gplus);
        chathead = findViewById(R.id.chathead);
        chatheader = findViewById(R.id.chatheader);
        badgelay = findViewById(R.id.badge_lay);
        badgecount = findViewById(R.id.badge_count);

    }

    @Override
    public void onBackPressed() {
        if (spinner.isPopupShowing()) {
            spinner.dismissDropDown();
            return;
        }
        if (from_spinner.isPopupShowing()) {
            from_spinner.dismissDropDown();
            return;
        }
        if (to_spinner.isPopupShowing()) {
            to_spinner.dismissDropDown();
            return;
        }

        if (firstFragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(firstFragment).commit();
            chathead.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.noti_icon));
            firstFragment = null;
            return;
        }

        super.onBackPressed();
    }

    private void setUpToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(LoginUiUpdate event) {

        CustomerModels user = prefs.getUserDetails();

        if (prefs.getSigned()) {
            //linearChangeEmirates.setVisibility(View.VISIBLE);
            //emirates_name.setVisibility(View.VISIBLE);

            bottomLay.setVisibility(View.INVISIBLE);
            bookinghistorydraw.setVisibility(View.VISIBLE);
            profilename.setText(user.getCustomerDetails().getName());
            logoutlaydraw.setVisibility(View.VISIBLE);
            viewprofiletext.setVisibility(View.VISIBLE);
            mobileLayer.setVisibility(View.GONE);
            areaLayer.setVisibility(View.GONE);
//            chatheader.setVisibility(View.VISIBLE);
            makepaylay.setVisibility(View.VISIBLE);

            Glide.with(this)
                    .applyDefaultRequestOptions(RequestOptions.centerCropTransform())
                    .applyDefaultRequestOptions(RequestOptions.circleCropTransform())
                    .applyDefaultRequestOptions(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_user_profile))
                    .load(prefs.getUserDetails().getCustomerDetails().getPhotoUrl()).into(profileImage);
            if (event.showName) {
                delayCall(user.getCustomerDetails().getName());
            }
        } else {
            bottomLay.setVisibility(View.VISIBLE);
            bookinghistorydraw.setVisibility(View.GONE);
            logoutlaydraw.setVisibility(View.GONE);
            profilename.setText("Login/Register");
            viewprofiletext.setVisibility(View.GONE);
//            chatheader.setVisibility(View.GONE);
            makepaylay.setVisibility(View.GONE);


            //linearChangeEmirates.setVisibility(View.GONE);
            //emirates_name.setVisibility(View.GONE);
        }
//https://www.journaldev.com/23096/android-dialogfragment
       // http://www.devexchanges.info/2015/10/showing-dialog-with-animation-in-android.htm
        if (offerdetails == null || offerdetails.isEmpty()) { //for showing offer
            offerLay.setVisibility(View.GONE);
        } else {
            offerLay.setVisibility(View.VISIBLE);
            offerDetails.setText(Html.fromHtml(offertitle));
            offerText.setText(Html.fromHtml(offerdetails));
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        if (view.isShown()) {
            cal.set(year, month, dayOfMonth);
            mYear = year;
            mMonth = month;
            mDay = dayOfMonth;

           /* For Home Maids -> Sunday is the week day off and they are working on Friday starting from tomorrow onwards (07/01/2022)
              changed on 07/01/2022
              By Reema */

            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {  // Calendar.FRIDAY
                Snackbar.make(bottomLay, "Can't choose Sundays", Snackbar.LENGTH_SHORT).show();
               // Snackbar.make(bottomLay, "Can't choose fridays", Snackbar.LENGTH_SHORT).show();
            } else {
                Log.d("DATE", "onDateSet: "+format1.format(cal.getTime()));
                dateText.setText(format1.format(cal.getTime()));
                dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
                getTiming(format1.format(cal.getTime()));

            }
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AreaClickEvent event) {
        spinner.setText(event.text);
        dialog.dismiss();
        sharedareaid = event.areaid;
        LogUtil.e(event.text + "  " + event.areaid);
    }

    private void DrawercloserEngine() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO call closeSlider method here.
                mDrawerLayout.closeDrawers();
            }
        }, 1000);
    }

    public void getTiming(String date) {

        progress.setVisibility(View.VISIBLE);

        JsonObjectRequest timingrequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.getTime(date), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress.setVisibility(View.GONE);
                try {
                    Log.d("TAG", "onResponse: "+response);
                    if (response.getString("status").equalsIgnoreCase("success")) {

                        if (response.getJSONArray("times").length() > 1) {
                            times = null;
                            times = response.getJSONArray("times");
                            fromtoSpinnerEngine();
                        }
                //--- today 28 jan 2022
                        // for next playstore update previous date not selectable in calender
                       // and error section timeing adapter reset  - noted by Reema
                        else if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            /*times = null;*/
                            initCalendar();
                            Snackbar.make(bookButton, "error", Snackbar.LENGTH_SHORT).show();

                        }
                        else {
                            initCalendar();
                            Snackbar.make(bookButton, "No Schedules available in this date", Snackbar.LENGTH_SHORT).show();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    initCalendar();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                initCalendar();
            }
        });
        timingrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(timingrequest);

    }
   // https://stackoverflow.com/questions/7562786/android-first-run-popup-dialog   -  isFirstRun
   // https://stackoverflow.com/questions/21399804/android-popup-certain-dialog-at-first-startiup-only

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void areaDialog() {
        View view = View.inflate(this, R.layout.fragment_search_area, null);
        dialog = new Dialog(this, R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        SearchView searchView = view.findViewById(R.id.search_view);
        listView = view.findViewById(R.id.search_area_list);
        TextView close = view.findViewById(R.id.close_text);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                search(newText);
                return false;
            }
        });

        areas = listModel.getAreas();

        // Creating adapter for spinner

        AreaCustomArrayAdapter dataAdapter = new AreaCustomArrayAdapter(this,
                R.layout.custom_spinner_item, areas);

        listView.setAdapter(dataAdapter);
        dialog.show();

    }

    private void search(String text) {
        List<Area> area_search = listModel.getAreas();
        search_area.clear();

        for (Area area : area_search) {
            if (area.getAreaName().toString().toLowerCase().contains(text)) {
                search_area.add(area);

            }
        }
        if (search_area.size() != 0) {
            AreaCustomArrayAdapter dataAdapter = new AreaCustomArrayAdapter(this,
                    R.layout.custom_spinner_item, search_area);

            listView.setAdapter(dataAdapter);
        }

    }

    public static class LoginUiUpdate {

        public boolean showName = false;

        public LoginUiUpdate(boolean showname) {
            this.showName = showname;
        }
    }

    public static class AreaClickEvent {
        public String text;
        public String areaid;

        public AreaClickEvent(String text, String id) {
            this.text = text;
            this.areaid = id;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BookingHistoryOpenEvent event) {
        Intent boohis = new Intent(MainActivity.this, BookingHistoryActivity.class);
        startActivity(boohis);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public static class BookingHistoryOpenEvent {


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageBadgeEvent event) {
        //// TODO: 24/01/18 message badge api calls goes here
        getMessageCount();
    }

    public static class MessageBadgeEvent {


    }

    public void getMessageCount() { //for badge count
        badgelay.setVisibility(View.GONE);
        JSONObject jsonObject = new JSONObject();
        try {
            if (prefs.getSigned())
                Log.d("Customer_id", "getCustomer_id: "+ prefs.getCustomerId());
                jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest badgerequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.notificationscount()
                , jsonObject, response -> {
            progress.setVisibility(View.GONE);
            LogUtil.e("count", "onResponse: " + response.toString());
            try {

                if (response.getString("status").equalsIgnoreCase("error") ||
                        response.getString("status").equalsIgnoreCase("failure")) {
                    badgelay.setVisibility(View.GONE);
                    return;
                }

                if (response.getString("status").equalsIgnoreCase("success")) {
                    int count = response.getInt("notifications_count");
                    if (count > 0) {
                        badgelay.setVisibility(View.VISIBLE);
                        badgecount.setText(String.valueOf(count));
                    } else {
                        badgelay.setVisibility(View.GONE);
                    }

                } else {
                    badgelay.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                badgelay.setVisibility(View.GONE);
            }

        }, error -> badgelay.setVisibility(View.GONE));
        badgerequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(badgerequest);
    }

    public void versionCheck() { //for badge count

        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            Log.e("Version ", version + "");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        LogUtil.e(String.valueOf(version));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("my_version", String.valueOf(version));
            Log.d("TEST", "versionCheck: "+String.valueOf(version));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest versionrequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getplaystoreversion()
                , jsonObject, response -> {

            try {

                Log.e("Res ","Checking:    " +response.toString());

                if (response.getString("status").equalsIgnoreCase("error") ||
                        response.getString("status").equalsIgnoreCase("failure")) {
                    return;
                }

                if (response.getString("status").equalsIgnoreCase("success")) {

                    if (response.getString("message").equalsIgnoreCase("true")) {

                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("New version available")
                                .setCancelable(false)
                                .setMessage("Please, update app to new version")
                                .setPositiveButton("UPDATE", (dialog, which) -> {
                                    final String appPackageName = getApplication().getPackageName();
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException e) { // if there is no Google Play on device
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                })
                                .setNegativeButton("NO, THANKS", null)
                                .show();
                    }

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }, error -> {
        });
        versionrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(versionrequest);
    }


    private void dataToServer() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("service_date", dateText.getText().toString());
            jsonObject.put("from_time", fromtimer);
            jsonObject.put("to_time", totimer);
            jsonObject.put("area", spinner.getText().toString());
            jsonObject.put("mobile_no", mobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest datarequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.userDataToServer(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                LogUtil.e("plagy", "onResponse: " + response.toString());

            }
        }, error -> {

        });
        datarequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(datarequest);
    }


}

