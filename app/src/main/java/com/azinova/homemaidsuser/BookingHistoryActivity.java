package com.azinova.homemaidsuser;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.fragments.BookingFrag;
import com.azinova.homemaidsuser.fragments.RatingFrag;
import com.azinova.homemaidsuser.models.BookingHistory;
import com.azinova.homemaidsuser.models.BookingHistoryListModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class BookingHistoryActivity extends AppCompatActivity {

    private final String TAG = "BookingHistoryActivity";
    RatingFrag ratingFrag;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private List<BookingHistoryListModel> history;
    private ProgressDialog pDialog;
    private ImageView nonetImg;
    private TextView hiserrorText;
    private LinearLayout hiserrorLay;
    private Button hisRetry;
    private LottieAnimationView animationView;
    private RelativeLayout backButton;
    private RelativeLayout help_lay;
    private Prefs pref;

    public BookingHistoryActivity() {
        history = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!pDialog.isShowing()){
            fetchDataEngine(UrlUtils.getBookingHistory(), true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_booking_history);
        initView();

        pref = Prefs.with(this);
        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
        pDialog = new ProgressDialog(this);

        fetchDataEngine(UrlUtils.getBookingHistory(), false);

        hisRetry.setOnClickListener(v -> fetchDataEngine(UrlUtils.getBookingHistory(), false));

        backButton.setOnClickListener(v -> onBackPressed());

        help_lay.setOnClickListener(v -> {
            Intent help = new Intent(BookingHistoryActivity.this, HelpActivity.class);
            startActivity(help);
        });
    }
    private void fetchDataEngine(String url, final boolean isfomrate) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", pref.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();
        hiserrorLay.setVisibility(View.GONE);

        if (animationView.isAnimating())
            animationView.cancelAnimation();

        @SuppressLint("SetTextI18n")
        JsonObjectRequest historyjsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, jsonObject,
                response -> {
                    pDialog.dismiss();
                    try {
                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {

                            hiserrorLay.setVisibility(View.VISIBLE);
                            hiserrorText.setText(response.getString("message"));
                            animationView.playAnimation();
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {
                            BookingHistory historyresponse = new Gson().fromJson(response.toString(), BookingHistory.class);

                            if (
                            isfomrate) {
                                EventBus.getDefault().post(new BookingFrag.AdapterRefresh(historyresponse));
                            } else {
                                setupViewPager(viewPager, historyresponse);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        LogUtil.e(TAG, e.toString());

                        hiserrorLay.setVisibility(View.VISIBLE);
                        hiserrorText.setText("Something went wrong");
                        animationView.playAnimation();
                    }
                }, error -> {
                    LogUtil.e(TAG, "Error: " + error.getMessage());
                    pDialog.dismiss();

                    String message = null;
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found.\nPlease try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ParseError) {
                        message = "Something went wrong!\nPlease try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut!\nPlease check your internet connection.";
                    }


                    hiserrorLay.setVisibility(View.VISIBLE);
                    hiserrorText.setText(message);
                    animationView.playAnimation();
                });

        historyjsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(historyjsonObjReq);
    }

    private void initView() {
        viewPager = findViewById(R.id.viewPager);
        nonetImg = findViewById(R.id.nonetimg);
        animationView = findViewById(R.id.animation_view);
        hiserrorText = findViewById(R.id.hiserrortext);
        hiserrorLay = findViewById(R.id.hiserrorlay);
        hisRetry = findViewById(R.id.hisretry);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        backButton = findViewById(R.id.backbutton);
        help_lay = findViewById(R.id.help_lay);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setupViewPager(ViewPager viewPager, BookingHistory bookingHistoryModel) {
        //Log.e(TAG, "setupViewPager: "+bookingHistoryModel.getPreHistory().get(1).getRating() );

        String result = new Gson().toJson(bookingHistoryModel);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new BookingFrag().newInstance(result, "current"), "ACTIVE");
        adapter.addFrag(new BookingFrag().newInstance(result, "past"), "PAST");
        LogUtil.e(result);

        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(RefreshEvent event) {
        fetchDataEngine(UrlUtils.getBookingHistory(), true);
    }

    public static class RefreshEvent {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
