package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.GalleryAdapter;
import com.azinova.homemaidsuser.models.GalleryModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class GalleryActivity extends AppCompatActivity {

    private static final String TAG = "GalleryActivity";
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.backbutton)
    RelativeLayout backbutton;
    @BindView(R.id.help_lay)
    RelativeLayout help;
    private TextView hiserrorText;
    private LinearLayout hiserrorLay;
    private Button hisRetry;
    private LottieAnimationView animationView;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);
        viewInit();
        setupRecyler();
        fetchDataEngine();

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        hisRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchDataEngine();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(GalleryActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

    }

    private void fetchDataEngine() {


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        hiserrorLay.setVisibility(View.GONE);

        if (animationView.isAnimating())
            animationView.cancelAnimation();

        JsonObjectRequest galleryjsonObjReq = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.getGalleryDetails(), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        pDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("error") ||
                                    response.getString("status").equalsIgnoreCase("failure")) {
                                recyclerView.setVisibility(View.GONE);
                                hiserrorLay.setVisibility(View.VISIBLE);
                                hiserrorText.setText(response.getString("message"));
                                animationView.playAnimation();
                                return;
                            }

                            if (response.getString("status").equalsIgnoreCase("success")) {
                                LogUtil.e(TAG, response.toString());
                                Gson gson = new Gson();
                                recyclerView.setVisibility(View.VISIBLE);
                                GalleryModel galleryModel = gson.fromJson(response.toString(), GalleryModel.class);
                                recyclerView.setAdapter(new GalleryAdapter(galleryModel.getGallery(), GalleryActivity.this));
                                recyclerView.scheduleLayoutAnimation(); //for animations
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            LogUtil.e(TAG, e.toString());
                            recyclerView.setVisibility(View.GONE);
                            hiserrorLay.setVisibility(View.VISIBLE);
                            hiserrorText.setText("Something went wrong");
                            animationView.playAnimation();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                LogUtil.e(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }
                recyclerView.setVisibility(View.GONE);
                hiserrorLay.setVisibility(View.VISIBLE);
                hiserrorText.setText(message);
                animationView.playAnimation();
            }
        });

        galleryjsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(galleryjsonObjReq);

    }

    private void setupRecyler() {
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutAnimation(controller);
    }

    private void viewInit() {
        animationView = findViewById(R.id.animation_view);
        hiserrorText = findViewById(R.id.hiserrortext);
        hiserrorLay = findViewById(R.id.hiserrorlay);
        hisRetry = findViewById(R.id.hisretry);

        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

}
