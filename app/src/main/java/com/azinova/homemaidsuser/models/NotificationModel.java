package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {

    @SerializedName("notifications")
    @Expose
    private List<NotifDatum> notifData = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<NotifDatum> getNotifData() {
        return notifData;
    }

    public void setNotifData(List<NotifDatum> notifData) {
        this.notifData = notifData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
