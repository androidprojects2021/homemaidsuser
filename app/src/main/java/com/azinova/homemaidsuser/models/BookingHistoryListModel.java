package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by azinova-mac6 on 07/11/17.
 */

public class BookingHistoryListModel {

    @SerializedName("reference_id")
    public String reference_id;

    @SerializedName("address")
    public String address;

    @SerializedName("date")
    public String date;

}
