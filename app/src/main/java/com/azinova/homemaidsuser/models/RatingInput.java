package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RatingInput {

@SerializedName("booking_id")
@Expose
private Integer bookingId;
@SerializedName("day_service_id")
@Expose
private Integer dayServiceId;
@SerializedName("rating")
@Expose
private String rating;
@SerializedName("rating_review")
@Expose
private String ratingReview;
@SerializedName("other_rating")
@Expose
private List<OtherRating> otherRating = null;

public Integer getBookingId() {
return bookingId;
}

public void setBookingId(Integer bookingId) {
this.bookingId = bookingId;
}

public Integer getDayServiceId() {
return dayServiceId;
}

public void setDayServiceId(Integer dayServiceId) {
this.dayServiceId = dayServiceId;
}

public String getRating() {
return rating;
}

public void setRating(String rating) {
this.rating = rating;
}

public String getRatingReview() {
return ratingReview;
}

public void setRatingReview(String ratingReview) {
this.ratingReview = ratingReview;
}

public List<OtherRating> getOtherRating() {
return otherRating;
}

public void setOtherRating(List<OtherRating> otherRating) {
this.otherRating = otherRating;
}

}