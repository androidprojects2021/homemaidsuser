package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingHistory {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("cur_history")
    @Expose
    private List<CurHistory> curHistory = null;
    @SerializedName("pre_history")
    @Expose
    private List<PreHistory> preHistory = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public List<CurHistory> getCurHistory() {
        return curHistory;
    }

    public void setCurHistory(List<CurHistory> curHistory) {
        this.curHistory = curHistory;
    }

    public List<PreHistory> getPreHistory() {
        return preHistory;
    }

    public void setPreHistory(List<PreHistory> preHistory) {
        this.preHistory = preHistory;
    }

}
