package com.azinova.homemaidsuser.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AreaList implements Parcelable {

    public static final Parcelable.Creator<AreaList> CREATOR = new Creator<AreaList>() {
        @Override
        public AreaList createFromParcel(Parcel parcel) {
            return new AreaList(parcel);
        }

        @Override
        public AreaList[] newArray(int i) {
            return new AreaList[i];
        }
    };
    @SerializedName("customer_address_id")
    @Expose
    private String customerAddressId;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("customer_address")
    @Expose
    private String customerAddress;
    @SerializedName("zone_name")
    @Expose
    private String zoneName;

    protected AreaList(Parcel parcel) {
        customerAddressId = parcel.readString();
        areaId = parcel.readString();
        areaName = parcel.readString();
        customerAddress = parcel.readString();
        zoneName = parcel.readString();
    }

    public String getCustomerAddressId() {
        return customerAddressId;
    }

    public void setCustomerAddressId(String customerAddressId) {
        this.customerAddressId = customerAddressId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(customerAddressId);
        parcel.writeString(areaId);
        parcel.writeString(areaName);
        parcel.writeString(customerAddress);
        parcel.writeString(zoneName);

    }
}
