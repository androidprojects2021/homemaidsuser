package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Abins Shaji on 27/11/17.
 */

public class CurHistory {

    @SerializedName("maid_name")
    @Expose
    private String maidName;

    @SerializedName("booking_type")
    @Expose
    private String bookingType;

    @SerializedName("service_date")
    @Expose
    private String serviceDate;



    @SerializedName("service_start_date")
    @Expose
    private String serviceStartDate;

    @SerializedName("time_from")
    @Expose
    private String timeFrom;

    @SerializedName("time_to")
    @Expose
    private String timeTo;

    @SerializedName("booking_id")
    @Expose
    private String bookingId;

    @SerializedName("reference_id")
    @Expose
    private String reference_id;

    @SerializedName("total_fee")
    @Expose
    private String totalFee;

    @SerializedName("shift")
    @Expose
    private String shift;

    @SerializedName("no_of_maids")
    @Expose
    private String noOfMaids;

    @SerializedName("booking_status")
    @Expose
    private String booking_status;

    @SerializedName("pay_status")
    @Expose
    private String pay_status;

    @SerializedName("cleaning_material")
    private String cleaning_material;


    @SerializedName("service_date_new")
    @Expose
    private String serviceDateNew;

    @SerializedName("day_service_id")
    @Expose
    private String dayServiceId;

    public String getDayServiceId() {
        return dayServiceId;
    }

    public void setDayServiceId(String dayServiceId) {
        this.dayServiceId = dayServiceId;
    }

    public String getServiceDateNew() {
        return serviceDateNew;
    }

    public void setServiceDateNew(String serviceDateNew) {
        this.serviceDateNew = serviceDateNew;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getCleaning_material() {
        return cleaning_material;
    }

    public void setCleaning_material(String cleaning_material) {
        this.cleaning_material = cleaning_material;
    }

    public String getPay_status() {
        return pay_status;
    }

    public void setPay_status(String pay_status) {
        this.pay_status = pay_status;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getMaidName() {
        return maidName;
    }

    public void setMaidName(String maidName) {
        this.maidName = maidName;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getServiceStartDate() {
        return serviceStartDate;
    }

    public void setServiceStartDate(String serviceStartDate) {
        this.serviceStartDate = serviceStartDate;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getNoOfMaids() {
        return noOfMaids;
    }

    public void setNoOfMaids(String noOfMaids) {
        this.noOfMaids = noOfMaids;
    }

}