package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gallery {

    @SerializedName("cat_img_url")
    @Expose
    private String catImgUrl;
    @SerializedName("cat_imgs")
    @Expose
    private List<CatImg> catImgs = null;
    @SerializedName("cat_name")
    @Expose
    private String catName;

    public String getCatImgUrl() {
        return catImgUrl;
    }

    public void setCatImgUrl(String catImgUrl) {
        this.catImgUrl = catImgUrl;
    }

    public List<CatImg> getCatImgs() {
        return catImgs;
    }

    public void setCatImgs(List<CatImg> catImgs) {
        this.catImgs = catImgs;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

}