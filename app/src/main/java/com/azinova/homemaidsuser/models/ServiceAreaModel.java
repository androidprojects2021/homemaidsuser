package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServiceAreaModel {

    @SerializedName("ser_area_data")
    @Expose
    private List<SerAreaDatum> serAreaData = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<SerAreaDatum> getSerAreaData() {
        return serAreaData;
    }

    public void setSerAreaData(List<SerAreaDatum> serAreaData) {
        this.serAreaData = serAreaData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
