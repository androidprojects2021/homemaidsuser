package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceListModel {

    @SerializedName("list")
    @Expose
    private java.util.List<ServiceList> list = null;
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @SerializedName("status")
    @Expose
    private String status;

    public java.util.List<ServiceList> getList() {
        return list;
    }

    public void setList(java.util.List<ServiceList> list) {
        this.list = list;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
