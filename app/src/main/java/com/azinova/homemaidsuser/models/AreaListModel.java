package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AreaListModel {

    @SerializedName("area_list")
    @Expose
    private List<Area> areas = null;
    @SerializedName("responce_code")
    @Expose
    private Integer responceCode;
    @SerializedName("status")
    @Expose
    private String status;

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public Integer getResponceCode() {
        return responceCode;
    }

    public void setResponceCode(Integer responceCode) {
        this.responceCode = responceCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
