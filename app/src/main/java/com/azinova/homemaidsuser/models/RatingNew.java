package com.azinova.homemaidsuser.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingNew {

@SerializedName("status")
@Expose
private String status;
@SerializedName("ratings")
@Expose
private List<Rating> ratings = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public List<Rating> getRatings() {
return ratings;
}

public void setRatings(List<Rating> ratings) {
this.ratings = ratings;
}

}