package com.azinova.homemaidsuser.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComplaintsNew {

@SerializedName("status")
@Expose
private String status;
@SerializedName("complaints")
@Expose
private List<Complaint> complaints = null;

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public List<Complaint> getComplaints() {
return complaints;
}

public void setComplaints(List<Complaint> complaints) {
this.complaints = complaints;
}

}