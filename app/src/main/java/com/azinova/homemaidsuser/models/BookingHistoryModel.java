package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by azinova-mac6 on 07/11/17.
 */

public class BookingHistoryModel {

    @SerializedName("status")
    public String status;

    @SerializedName("current_bookings")
    public List<BookingHistoryListModel> current_bookings;

    @SerializedName("past_bookings")
    public List<BookingHistoryListModel> past_bookings;
}
