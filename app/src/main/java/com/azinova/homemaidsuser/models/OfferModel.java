package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OfferModel {

    @SerializedName("customer_type")
    @Expose
    private List<CustomerTypes> types = null;

    public List<CustomerTypes> getTypes() {
        return types;
    }

    public void setTypes(List<CustomerTypes> types) {
        this.types = types;
    }
}
