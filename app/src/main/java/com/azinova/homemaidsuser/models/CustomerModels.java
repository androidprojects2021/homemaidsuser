package com.azinova.homemaidsuser.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CustomerModels implements Parcelable {

    public static final Parcelable.Creator<CustomerModels> CREATOR = new Creator<CustomerModels>() {
        @Override
        public CustomerModels createFromParcel(Parcel parcel) {
            return new CustomerModels(parcel);
        }

        @Override
        public CustomerModels[] newArray(int i) {
            return new CustomerModels[i];
        }
    };
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("customer_details")
    @Expose
    private CustomerDetails customerDetails;
    @SerializedName("area_list")
    @Expose
    private List<AreaList> areaList = new ArrayList<>();

    private CustomerModels(Parcel in) {
        this.customerDetails = in.readParcelable(CustomerDetails.class.getClassLoader());
        in.readTypedList(areaList, AreaList.CREATOR);


    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public List<AreaList> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<AreaList> areaList) {
        this.areaList = areaList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(customerDetails, i);
        parcel.writeTypedList(areaList);

    }
}
