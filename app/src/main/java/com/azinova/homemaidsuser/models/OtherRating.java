package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OtherRating {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("rating")
@Expose
private float rating;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public float getRating() {
return rating;
}

public void setRating(float rating) {
this.rating = rating;
}

}