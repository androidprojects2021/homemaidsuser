package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Abins Shaji on 28/06/18.
 */
public class PriceDetailsModel {

    @SerializedName("status")
    String status;
    @SerializedName("rate_data")
    RateData rateData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RateData getRateData() {
        return rateData;
    }

    public void setRateData(RateData rateData)  {
        this.rateData = rateData;
    }

    public class RateData{
        @SerializedName("service_rate")
        Double service_rate;
        @SerializedName("material_rate")
        Double material_rate;
        @SerializedName("discount_perc")
        Double discount_perc;
        @SerializedName("vat_perc")
        Double vat_perc;
        @SerializedName("no_hours")
        int no_hour;

        public int getNo_hour() {
            return no_hour;
        }

        public void setNo_hour(int no_hour) {
            this.no_hour = no_hour;
        }

        public Double getService_rate() {
            return service_rate;
        }

        public void setService_rate(Double service_rate) {
            this.service_rate = service_rate;
        }

        public Double getMaterial_rate() {
            return material_rate;
        }

        public void setMaterial_rate(Double material_rate) {
            this.material_rate = material_rate;
        }

        public Double getDiscount_perc() {
            return discount_perc;
        }

        public void setDiscount_perc(Double discount_perc) {
            this.discount_perc = discount_perc;
        }

        public Double getVat_perc() {
            return vat_perc;
        }

        public void setVat_perc(Double vat_perc) {
            this.vat_perc = vat_perc;
        }
    }
}

