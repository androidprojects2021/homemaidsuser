package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HelpModel {

    @SerializedName("help_data")
    @Expose
    private List<HelpDatum> helpData = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<HelpDatum> getHelpData() {
        return helpData;
    }

    public void setHelpData(List<HelpDatum> helpData) {
        this.helpData = helpData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
