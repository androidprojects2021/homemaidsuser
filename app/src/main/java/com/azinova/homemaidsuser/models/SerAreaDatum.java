package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SerAreaDatum {

    @SerializedName("ser_lat")
    @Expose
    private Double serLat;
    @SerializedName("ser_long")
    @Expose
    private Double serLong;
    @SerializedName("ser_name")
    @Expose
    private String serName;

    public Double getSerLat() {
        return serLat;
    }

    public void setSerLat(Double serLat) {
        this.serLat = serLat;
    }

    public Double getSerLong() {
        return serLong;
    }

    public void setSerLong(Double serLong) {
        this.serLong = serLong;
    }

    public String getSerName() {
        return serName;
    }

    public void setSerName(String serName) {
        this.serName = serName;
    }

}
