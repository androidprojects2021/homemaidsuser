package com.azinova.homemaidsuser.models;

public class PaymentSuccess {

    private String merchant_param6;
    private String merchant_param5;
    private String merchant_param4;
    private String merchant_param3;
    private String billing_name;
    private String merchant_param2;
    private String status_message;
    private String merchant_param1;
    private String response_type;
    private String billing_city;
    private String amount;
    private String order_status;
    private String billing_country;
    private String billing_address;
    private String discount_value;
    private String billing_zip;
    private String delivery_country;
    private String billing_tel;
    private String failure_message;
    private String order_id;
    private String bank_ref_no;
    private String delivery_address;
    private String status_code;
    private String billing_state;
    private String payment_mode;
    private String vault;
    private String delivery_state;
    private String card_holder_name;
    private String offer_type;
    private String delivery_name;
    private String offer_code;
    private String bank_receipt_no;
    private String tracking_id;
    private String delivery_city;
    private String delivery_zip;
    private String delivery_tel;
    private String currency;
    private String eci_value;
    private String card_name;
    private String billing_email;
    private String mer_amount;

    public String getMerchant_param6() {
        return merchant_param6;
    }

    public void setMerchant_param6(String merchant_param6) {
        this.merchant_param6 = merchant_param6;
    }

    public String getMerchant_param5() {
        return merchant_param5;
    }

    public void setMerchant_param5(String merchant_param5) {
        this.merchant_param5 = merchant_param5;
    }

    public String getMerchant_param4() {
        return merchant_param4;
    }

    public void setMerchant_param4(String merchant_param4) {
        this.merchant_param4 = merchant_param4;
    }

    public String getMerchant_param3() {
        return merchant_param3;
    }

    public void setMerchant_param3(String merchant_param3) {
        this.merchant_param3 = merchant_param3;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public void setBilling_name(String billing_name) {
        this.billing_name = billing_name;
    }

    public String getMerchant_param2() {
        return merchant_param2;
    }

    public void setMerchant_param2(String merchant_param2) {
        this.merchant_param2 = merchant_param2;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public String getMerchant_param1() {
        return merchant_param1;
    }

    public void setMerchant_param1(String merchant_param1) {
        this.merchant_param1 = merchant_param1;
    }

    public String getResponse_type() {
        return response_type;
    }

    public void setResponse_type(String response_type) {
        this.response_type = response_type;
    }

    public String getBilling_city() {
        return billing_city;
    }

    public void setBilling_city(String billing_city) {
        this.billing_city = billing_city;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getBilling_country() {
        return billing_country;
    }

    public void setBilling_country(String billing_country) {
        this.billing_country = billing_country;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public void setBilling_address(String billing_address) {
        this.billing_address = billing_address;
    }

    public String getDiscount_value() {
        return discount_value;
    }

    public void setDiscount_value(String discount_value) {
        this.discount_value = discount_value;
    }

    public String getBilling_zip() {
        return billing_zip;
    }

    public void setBilling_zip(String billing_zip) {
        this.billing_zip = billing_zip;
    }

    public String getDelivery_country() {
        return delivery_country;
    }

    public void setDelivery_country(String delivery_country) {
        this.delivery_country = delivery_country;
    }

    public String getBilling_tel() {
        return billing_tel;
    }

    public void setBilling_tel(String billing_tel) {
        this.billing_tel = billing_tel;
    }

    public String getFailure_message() {
        return failure_message;
    }

    public void setFailure_message(String failure_message) {
        this.failure_message = failure_message;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getBank_ref_no() {
        return bank_ref_no;
    }

    public void setBank_ref_no(String bank_ref_no) {
        this.bank_ref_no = bank_ref_no;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getBilling_state() {
        return billing_state;
    }

    public void setBilling_state(String billing_state) {
        this.billing_state = billing_state;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getVault() {
        return vault;
    }

    public void setVault(String vault) {
        this.vault = vault;
    }

    public String getDelivery_state() {
        return delivery_state;
    }

    public void setDelivery_state(String delivery_state) {
        this.delivery_state = delivery_state;
    }

    public String getCard_holder_name() {
        return card_holder_name;
    }

    public void setCard_holder_name(String card_holder_name) {
        this.card_holder_name = card_holder_name;
    }

    public String getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public String getDelivery_name() {
        return delivery_name;
    }

    public void setDelivery_name(String delivery_name) {
        this.delivery_name = delivery_name;
    }

    public String getOffer_code() {
        return offer_code;
    }

    public void setOffer_code(String offer_code) {
        this.offer_code = offer_code;
    }

    public String getBank_receipt_no() {
        return bank_receipt_no;
    }

    public void setBank_receipt_no(String bank_receipt_no) {
        this.bank_receipt_no = bank_receipt_no;
    }

    public String getTracking_id() {
        return tracking_id;
    }

    public void setTracking_id(String tracking_id) {
        this.tracking_id = tracking_id;
    }

    public String getDelivery_city() {
        return delivery_city;
    }

    public void setDelivery_city(String delivery_city) {
        this.delivery_city = delivery_city;
    }

    public String getDelivery_zip() {
        return delivery_zip;
    }

    public void setDelivery_zip(String delivery_zip) {
        this.delivery_zip = delivery_zip;
    }

    public String getDelivery_tel() {
        return delivery_tel;
    }

    public void setDelivery_tel(String delivery_tel) {
        this.delivery_tel = delivery_tel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEci_value() {
        return eci_value;
    }

    public void setEci_value(String eci_value) {
        this.eci_value = eci_value;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getBilling_email() {
        return billing_email;
    }

    public void setBilling_email(String billing_email) {
        this.billing_email = billing_email;
    }

    public String getMer_amount() {
        return mer_amount;
    }

    public void setMer_amount(String mer_amount) {
        this.mer_amount = mer_amount;
    }

}