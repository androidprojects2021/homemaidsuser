package com.azinova.homemaidsuser.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreHistory {

    @SerializedName("maid_name")
    @Expose
    private String maidName;
    @SerializedName("booking_type")
    @Expose
    private String bookingType;
    @SerializedName("shift")
    @Expose
    private String shift;
    @SerializedName("service_date")
    @Expose
    private String serviceDate;
    @SerializedName("total_fee")
    @Expose
    private String totalFee;
    @SerializedName("service_status")
    @Expose
    private String service_status;
    @SerializedName("day_service_id")
    @Expose
    private String day_service_id;
    @SerializedName("booking_id")
    @Expose
    private String booking_id;
    @SerializedName("no_of_maids")
    @Expose
    private String noOfMaids;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("cleaning_material")
    private String cleaning_material;


    @SerializedName("complaint_status")
    @Expose
    private String complaintStatus;
    @SerializedName("rate_status")
    @Expose
    private String rateStatus;


    @SerializedName("service_date_new")
    @Expose
    private String serviceDateNew;


    public String getServiceDateNew() {
        return serviceDateNew;
    }

    public void setServiceDateNew(String serviceDateNew) {
        this.serviceDateNew = serviceDateNew;
    }


    public String getComplaintStatus() {
        return complaintStatus;
    }

    public void setComplaintStatus(String complaintStatus) {
        this.complaintStatus = complaintStatus;
    }

    public String getRateStatus() {
        return rateStatus;
    }

    public void setRateStatus(String rateStatus) {
        this.rateStatus = rateStatus;
    }


    public String getCleaning_material() {
        return cleaning_material;
    }

    public void setCleaning_material(String cleaning_material) {
        this.cleaning_material = cleaning_material;
    }

    public String getNoOfMaids() {
        return noOfMaids;
    }

    public void setNoOfMaids(String noOfMaids) {
        this.noOfMaids = noOfMaids;
    }

    public String getDay_service_id() {
        return day_service_id;
    }

    public void setDay_service_id(String day_service_id) {
        this.day_service_id = day_service_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getService_status() {
        return service_status;
    }

    public void setService_status(String service_status) {
        this.service_status = service_status;
    }

    public String getMaidName() {
        return maidName;
    }

    public void setMaidName(String maidName) {
        this.maidName = maidName;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(String totalFee) {
        this.totalFee = totalFee;
    }

}
