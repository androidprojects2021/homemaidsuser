package com.azinova.homemaidsuser;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.RatingAdapterNew;
import com.azinova.homemaidsuser.models.OtherRating;
import com.azinova.homemaidsuser.models.Rating;
import com.azinova.homemaidsuser.models.RatingNew;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;


public class RatingActivity extends AppCompatActivity {

    ArrayList<Rating> ratingList = new ArrayList<>();
    JSONObject jsonObject = new JSONObject();
    RatingBar ratingBar;
    RecyclerView  recyclerView;
    ProgressDialog   pDialog;
    ScrollView scrollView;
    String book_id,dayServiceId;
    LinearLayout linearLayout;
    EditText editTextComplaints;
    String edtData="";
    RatingAdapterNew adapterNew;
    TextView headingTxt;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_new);
        ratingBar = findViewById(R.id.ratingBar_main);
        recyclerView=findViewById(R.id.recycler_review);
        scrollView=findViewById(R.id.scrollView);
        linearLayout=findViewById(R.id.submit_review);
        headingTxt=findViewById(R.id.heading_txt);

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {

            for (Rating rating1 : ratingList) {
                rating1.setRating(rating);
            }
            adapterNew.notifyDataSetChanged();

        });

        Bundle bundle=getIntent().getExtras();

        if (bundle!=null){
            book_id=bundle.getString("booking_id");
            dayServiceId=bundle.getString("day_service_id");

            headingTxt.setText("Please rate/review this booking \n RefNo : "+book_id);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        pDialog = new ProgressDialog(this);
        scrollView.setVisibility(View.INVISIBLE);
        pDialog.show();

        JsonObjectRequest getRatingQuestions = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.getRatingList(), jsonObject,
                response -> {

                    RatingNew ratingNew=new Gson().fromJson(response.toString(),RatingNew.class);
                    ratingList.addAll(ratingNew.getRatings());

                    adapterNew=new RatingAdapterNew(ratingList);
                    recyclerView.setAdapter(adapterNew);

                    scrollView.setVisibility(View.VISIBLE );
                    pDialog.dismiss();
                }, error ->
                LogUtil.e("error   ",error.toString())
        );

        getRatingQuestions.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(getRatingQuestions);

        linearLayout.setOnClickListener(v->{

            pDialog.show();
            if (editTextComplaints!=null){
                edtData=editTextComplaints.getText().toString();

            }else {
                edtData="No Review";
            }
            try {

                JSONObject jsonObjectNew = new JSONObject();
                List<JSONObject>  list = new ArrayList<>();


                for (Rating rating : ratingList) {
                    OtherRating other=new OtherRating();

                    JSONObject object = new JSONObject();
                    object.put("id", Integer.parseInt(rating.getId()));
                    object.put("rating", rating.getRating());
                    other.setId(Integer.parseInt(rating.getId()));
                    other.setRating(rating.getRating());
                    list.add(object);
                }

                jsonObjectNew.put("booking_id",book_id );
                jsonObjectNew.put("day_service_id", dayServiceId);
                jsonObjectNew.put("rating",ratingBar.getRating() );
                jsonObjectNew.put("rating_review",edtData);
                jsonObjectNew.put("other_rating", list);

                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST,UrlUtils.ratingSubmit(),jsonObjectNew,
                        response -> {

                    pDialog.dismiss();
                            try {

                                Snackbar.make(v, response.getString("status"), Snackbar.LENGTH_SHORT).show();
                                if (response.getString("status").equalsIgnoreCase("success")){
                                    backClick(v);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        },error -> LogUtil.e("error   ",error.toString()));
                AppController.getInstance().addToRequestQueue(jsonObjectRequest);
                VolleyLog.DEBUG = true;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        });
    }


    public void backClick(View view) {
        onBackPressed();

    }
}