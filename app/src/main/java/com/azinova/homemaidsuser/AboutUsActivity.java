package com.azinova.homemaidsuser;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends AppCompatActivity {
    @BindView(R.id.data_basic)
    TextView content_main;
    @BindView(R.id.read_more)
    TextView read_more;
    @BindView(R.id.arrow)
    ImageView arrow;
    @BindView(R.id.help_lay)
    RelativeLayout help_lay;
    private boolean f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        f = false;

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hel = new Intent(AboutUsActivity.this, HelpActivity.class);
                startActivity(hel);
            }
        });

    }

    public void backClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void readMoreClick(View view) {
        if (f) {
            content_main.setText(getResources().getString(R.string.about_us_content_one));
            f = false;
            read_more.setText("Read more");
            arrow.setRotation(0);
            return;
        } else {
            content_main.setText(getResources().getString(R.string.about_us_content_full));
            f = true;
            read_more.setText("Read less");
            arrow.setRotation(-90);
        }


    }
}
