package com.azinova.homemaidsuser.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.ComplaintsNew;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.RatingActivity;
import com.azinova.homemaidsuser.fragments.RatingFrag;
import com.azinova.homemaidsuser.models.PreHistory;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 27/11/17.
 */

public class BookingHistoryAdapterPrev extends RecyclerView.Adapter<BookingHistoryAdapterPrev.HistoryHolderData> {

    public static final String TAG = "message";
    List<PreHistory> preHistoryList = new ArrayList<>();
    Context context;
    Bundle bundle = new Bundle();


    public BookingHistoryAdapterPrev(List<PreHistory> preHistoryList, Context context) {

        this.preHistoryList = preHistoryList;

        Gson gson=new Gson();
        String string=gson.toJson(preHistoryList);
        Log.e("pre History",string);

        this.context = context;
    }

    @Override
    public HistoryHolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_booking_history, parent, false);
        return new HistoryHolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HistoryHolderData holder, int position) {
        final PreHistory bookingHistory = preHistoryList.get(position);
        holder.ratingBtn.setEnabled(false);

        if (!bookingHistory.getRateStatus().equals("1")){
            holder.ratingBtn.setEnabled(true);

            Log.e("pre History",bookingHistory.getRateStatus());

            holder.ratingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent boohis = new Intent(context, RatingActivity.class);

                    Bundle bundle=new Bundle();
                    bundle.putString("day_service_id", bookingHistory.getDay_service_id());
                    bundle.putString("booking_id", bookingHistory.  getBooking_id());
                    boohis.putExtras(bundle);
                    context.startActivity(boohis);
                }
            });
        }

        holder.booking_amount.setText("AED " + bookingHistory.getTotalFee());
        holder.booking_date.setText(bookingHistory.getServiceDate());
        //changed to number of maids
        holder.booking_maid_name.setText(bookingHistory.getNoOfMaids());

        if (bookingHistory.getCleaning_material()!=null){
            holder.material_status.setText(bookingHistory.getCleaning_material().contentEquals("Y")?"YES":"NO");
        }

        holder.booking_time.setText(bookingHistory.getShift());
        holder.booking_button.setVisibility(View.INVISIBLE);

        holder.complaintsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent boohis = new Intent(context, ComplaintsNew.class);
                context.startActivity(boohis);
            }
        });

        if (position != 0) {
            holder.line.setVisibility(View.GONE);
        } else {
            holder.line.setVisibility(View.VISIBLE);
        }
        switch (bookingHistory.getService_status()) {
            case "1": {
                holder.booking_status.setText("Pending");
                break;


            }
            case "2": {
                holder.booking_status.setText("Completed");
                holder.booking_status.setTextColor(ContextCompat.getColor(context, R.color.green));
                break;

            }
            case "3": {
                holder.booking_status.setText("Canceled");
                holder.booking_status.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                break;
            }
            default:
                holder.booking_status.setText("Pending");
                break;

        }
        if (bookingHistory.getRating().equalsIgnoreCase("0")) {
            holder.rating_relative.setVisibility(View.GONE);
            holder.rate_button.setVisibility(View.VISIBLE);
            holder.rate_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("day_service_id", bookingHistory.getDay_service_id());
                    bundle.putString("booking_id", bookingHistory.getBooking_id());
                    FragmentActivity fragmentActivity = (FragmentActivity) (context);
                    RatingFrag ratingFrag = new RatingFrag();
                    ratingFrag.setArguments(bundle);
                    ratingFrag.show(fragmentActivity.getFragmentManager(), "");

                }
            });

        } else {
            holder.rate_button.setVisibility(View.GONE);
            holder.rating_relative.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(Float.parseFloat(bookingHistory.getRating()));
        }


    }

    @Override
    public int getItemCount() {
        return preHistoryList.size();
    }

    public class HistoryHolderData extends RecyclerView.ViewHolder {
        @BindView(R.id.bottom_line)
        View line;
        @BindView(R.id.booking_date)
        TextView booking_date;
        @BindView(R.id.booking_time)
        TextView booking_time;
        @BindView(R.id.maid_name)
        TextView booking_maid_name;
        @BindView(R.id.booking_day)
        TextView booking_day;
        @BindView(R.id.booking_amount)
        TextView booking_amount;
        @BindView(R.id.booking_status)
        TextView booking_status;
        @BindView(R.id.booking_button)
        Button booking_button;
        @BindView(R.id.default_rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.rate_button)
        Button rate_button;
        @BindView(R.id.rating_relative)
        RelativeLayout rating_relative;
        @BindView(R.id.complaints_new)
        Button complaintsBtn;
        @BindView(R.id.rate_new)
        Button ratingBtn;
        @BindView(R.id.material_status)
        TextView material_status;

        public HistoryHolderData(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
