package com.azinova.homemaidsuser.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.Rating;
import com.azinova.homemaidsuser.models.RatingNew;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class RatingAdapterNew extends RecyclerView.Adapter<RatingAdapterNew.MyViewHolder> {

    ArrayList<Rating> ratingList;

    public RatingAdapterNew(ArrayList<Rating> ratingList) {
        this.ratingList=ratingList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_rating_new,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (ratingList!=null){
           holder.bar.setRating(ratingList.get(position).getRating());
        }

        holder.ratingQuestions.setText(ratingList.get(position).getDescription());
        holder.bar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> ratingList.get(position).setRating(rating));

    }

    @Override
    public int getItemCount() {
        return ratingList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView ratingQuestions;
        RatingBar bar;
        public MyViewHolder(View itemView) {
            super(itemView);

            ratingQuestions=itemView.findViewById(R.id.ratingQuestions);
            bar = itemView.findViewById(R.id.rating);
        }
    }
}
