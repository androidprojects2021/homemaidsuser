package com.azinova.homemaidsuser.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.BookingHistoryListModel;

import java.util.List;

/**
 * Created by azinova-mac6 on 07/11/17.
 */

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.MyViewHolder> {

    private List<BookingHistoryListModel> boohistory;
    private TextView idText;

    public PaymentHistoryAdapter(List<BookingHistoryListModel> history) {
        this.boohistory = history;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_booking_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PaymentHistoryAdapter.MyViewHolder holder, int position) {
        BookingHistoryListModel data = boohistory.get(position);
        idText.setText(data.reference_id + "\n" + data.address + "\n" + data.date); //// TODO: 08/11/17 for testing purpose 

    }

    @Override
    public int getItemCount() {
        return boohistory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public MyViewHolder(View itemView) {
            super(itemView);
            idText = itemView.findViewById(R.id.idtext);
        }
    }
}
