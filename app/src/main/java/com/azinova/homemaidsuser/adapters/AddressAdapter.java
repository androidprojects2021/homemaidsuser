package com.azinova.homemaidsuser.adapters;

import android.app.Dialog;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.ConfirmBookActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.AreaList;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leo elstin on 11/2/17.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {

    private List<AreaList> modelList;
    private   int k=-1;
    Boolean b=true;
    private Dialog dialog;

    public AddressAdapter(List<AreaList> modelList, Dialog dialog) {


        this.modelList = modelList;
        this.dialog = dialog;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.select_image)
        ImageView select_image;
        @BindView(R.id.relative_adapter_add_address)
        RelativeLayout relative_adapter_add_address;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_add_address, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        final AreaList response=modelList.get(position);
        holder.address.setText(Html.fromHtml(response.getCustomerAddress()));
        if(b && position==0)
        {
            holder.select_image.setImageResource(R.drawable.on_tick);
        }else {
            holder.select_image.setImageResource(R.drawable.off_tick);

        }


        if(k==position)
        {
            holder.select_image.setImageResource(R.drawable.on_tick);
        }

        holder.relative_adapter_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b=false;
                k=position;
                notifyDataSetChanged();
                EventBus.getDefault().post(new ConfirmBookActivity.AddressEvent(response.getCustomerAddressId(),response.getCustomerAddress(),response.getAreaName()));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //dialog.dismiss();

                    }
                }, 500);

            }
        });


    }


    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
