package com.azinova.homemaidsuser.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.BookedDetailActivity;
import com.azinova.homemaidsuser.PaymentActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.CurHistory;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.payment.PaymentWebViewActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.PaymentUtils;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.Shared;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import mumbai.dev.sdkdubai.BillingAddress;
import mumbai.dev.sdkdubai.MerchantDetails;
import mumbai.dev.sdkdubai.ShippingAddress;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

/**
 * Created by azinova-mac6 on 07/11/17.
 */

public class BookingHistoryAdapter extends RecyclerView.Adapter<BookingHistoryAdapter.MyViewHolder> {

    private List<CurHistory> boohistory;
    private Context context;
    private TextView idText;
    private Prefs prefs;
    private ProgressDialog pDialog;
    private CustomerModels customerModels;
    private CustomerDetails model;

    public BookingHistoryAdapter(List<CurHistory> history, Context context) {
        this.boohistory = history;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_booking_history_current, parent, false);
        prefs = Prefs.with(context);
        customerModels = prefs.getUserDetails();
        model = customerModels.getCustomerDetails();


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final CurHistory bookingHistory = boohistory.get(position);

        holder.booking_amount.setText("AED " + bookingHistory.getTotalFee());
        holder.booking_date.setText(bookingHistory.getServiceDate());

        //changed to number of maids

        holder.booking_maid_name.setText(bookingHistory.getNoOfMaids());
        holder.booking_time.setText(bookingHistory.getShift());

        holder.material_status.setText(bookingHistory.getCleaning_material().contentEquals("Y") ? "YES" : "NO");

        if (bookingHistory.getBookingType().equalsIgnoreCase("OD")) {
            holder.booking_day.setText("One Day");
        } else {
            holder.booking_day.setText("Weekly");
        }

        holder.booking_status.setText(bookingHistory.getBooking_status());

        if (bookingHistory.getPay_status().equalsIgnoreCase("1")) {
            holder.booking_button.setVisibility(View.GONE);
        } else {
            holder.booking_button.setVisibility(View.VISIBLE);
        }

        if (position != 0) {
            holder.line.setVisibility(View.GONE);
        } else {
            holder.line.setVisibility(View.VISIBLE);
        }


        holder.booking_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage("Are you sure you want to proceed this payment?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Shared.pay_bookingid = bookingHistory.getBookingId();
//                                Shared.pay_total = bookingHistory.getTotalFee();
//                                PaymentEngine(bookingHistory.getBookingId(), bookingHistory.getTotalFee());// bookingHistory.getTotalFee()

                                Intent webview_payment = new Intent(context, PaymentWebViewActivity.class);
                                webview_payment.putExtra("booking_id", bookingHistory.getBookingId());
                                webview_payment.putExtra("reference_id", bookingHistory.getReference_id());
                                webview_payment.putExtra("price", bookingHistory.getTotalFee());
                                webview_payment.putExtra("service_date_new", bookingHistory.getServiceDateNew());
                                webview_payment.putExtra("day_service_id",bookingHistory.getDayServiceId() );
                                webview_payment.putExtra("from","booking_history");
                                context.startActivity(webview_payment);

                            }

                        })
                        .setNegativeButton("No", null)
                        .show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return boohistory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.bottom_line)
        View line;
        @BindView(R.id.booking_date)
        TextView booking_date;
        @BindView(R.id.booking_time)
        TextView booking_time;
        @BindView(R.id.maid_name)
        TextView booking_maid_name;
        @BindView(R.id.booking_day)
        TextView booking_day;
        @BindView(R.id.booking_amount)
        TextView booking_amount;
        @BindView(R.id.booking_status)
        TextView booking_status;
        @BindView(R.id.booking_button)
        Button booking_button;
        @BindView(R.id.default_rating_bar)
        RatingBar ratingBar;
        @BindView(R.id.rate_button)
        Button rate_button;
        @BindView(R.id.material_status)
        TextView material_status;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            idText = itemView.findViewById(R.id.idtext);
        }
    }

    private void PaymentEngine(String bookingid, final String totalprice) {

        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject paydetails = new JSONObject();
        try {
            paydetails.put("customer_id", prefs.getCustomerId());
            paydetails.put("booking_id", bookingid);
            paydetails.put("price", totalprice);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        LogUtil.e(paydetails.toString());

        JsonObjectRequest adpaymentObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getOrderId(), paydetails,
                response -> {
                    LogUtil.e(response.toString());
                    pDialog.dismiss();
                    try {
                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {
/*
 ************************************************************ old CC Avenue *************************************************************************
 *
                            LogUtil.e(response.toString());

                            String vAccessCode = ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim();
                            String vMerchantId = ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim();
                            String vCurrency = ServiceUtility.chkNull("AED").toString().trim();
                            String vAmount = ServiceUtility.chkNull(totalprice).toString().trim(); //total_price
                            if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
                                Intent intent = new Intent(context, WebViewActivity.class);
                                intent.putExtra(AvenuesParams.ACCESS_CODE, ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim());
                                intent.putExtra(AvenuesParams.MERCHANT_ID, ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim());
                                intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(response.getString("order_id")).toString().trim());
                                intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull("AED").toString().trim());
                                intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(totalprice).toString().trim()); //total_price
                                intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(PaymentUtils.RSA_URL).toString().trim());

                                intent.putExtra(AvenuesParams.BILLING_NAME, model.getName());
                                if (customerModels.getAreaList().size() > 0) {
                                    intent.putExtra(AvenuesParams.BILLING_ADDRESS, customerModels.getAreaList().get(0).getCustomerAddress());
                                    intent.putExtra(AvenuesParams.BILLING_CITY, customerModels.getAreaList().get(0).getAreaName());
                                }
                                intent.putExtra(AvenuesParams.BILLING_COUNTRY, "United Arab Emirates");
                                intent.putExtra(AvenuesParams.BILLING_TEL, model.getPhone());
                                intent.putExtra(AvenuesParams.BILLING_EMAIL, model.getEmail());

                                Activity activity = (Activity) context;
                                activity.startActivity(intent);
                                activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            } else {
                                Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
                            }

************************************************************ old CC Avenue *************************************************************************
*/

                            MerchantDetails m = new MerchantDetails();

                            m.setCurrency("AED");
                            m.setAmount(totalprice.trim());
                            m.setAccess_code(PaymentUtils.AccessCode);
                            m.setMerchant_id(PaymentUtils.MerchantID);
                            m.setRedirect_url(PaymentUtils.RedirectUrl);
                            m.setCancel_url(PaymentUtils.RedirectUrl);
                            m.setRsa_url(PaymentUtils.RSA_URL);
                            m.setOrder_id(response.getString("order_id"));
                            m.setCustomer_id(customerModels.getCustomerDetails().getCustomerId());
                            m.setPromo_code("");
                            m.setAdd1("add1");
                            m.setAdd2("add2");
                            m.setAdd3("add3");
                            m.setAdd4("add4");
                            m.setAdd5("add5");

                            BillingAddress billingAddress = new BillingAddress();

                            billingAddress.setName(customerModels.getCustomerDetails().getName());
                            billingAddress.setAddress(customerModels.getAreaList().get(0).getCustomerAddress());
                            billingAddress.setCountry("Dubai");
                            billingAddress.setState("Dubai");
                            billingAddress.setCity("Dubai");
                            billingAddress.setTelephone(model.getPhone());
                            billingAddress.setEmail(model.getEmail());

                            ShippingAddress s = new ShippingAddress();
                            s.setName(customerModels.getCustomerDetails().getName());
                            s.setAddress(customerModels.getAreaList().get(0).getCustomerAddress());
                            s.setCountry("Dubai");
                            s.setState("Dubai");
                            s.setCity("Dubai");
                            s.setTelephone(model.getPhone());

                            Intent i = new Intent(context, PaymentActivity.class);

                            i.putExtra("merchant", m);
                            i.putExtra("billing", billingAddress);
                            i.putExtra("shipping", s);

                            Activity activity = (Activity) context;
                            activity.startActivity(i);

                        } else {
                            Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }, error -> {
            pDialog.dismiss();
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ServerError) {
                message = "The server could not be found.\nPlease try again after some time!!";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ParseError) {
                message = "Something went wrong!\nPlease try again after some time!!";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut!\nPlease check your internet connection.";
            }

            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        });

        adpaymentObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(adpaymentObjReq);

    }


}
