package com.azinova.homemaidsuser.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.NotifDatum;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 07/12/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotfDAta> {

    List<NotifDatum> notifData = new ArrayList<>();
    Context context;

    public NotificationAdapter(List<NotifDatum> notifData, Context context) {
        this.notifData = notifData;
        this.context = context;
    }

    @Override
    public NotfDAta onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notification, parent, false);
        return new NotfDAta(view);
    }

    @Override
    public void onBindViewHolder(NotfDAta holder, int position) {
        NotifDatum datum = notifData.get(position);
        holder.content.setText(datum.getContent());
        holder.title.setText(datum.getTitle());
        try {

            if (datum.getImage().isEmpty() || datum.getImage().equalsIgnoreCase("null")) {
                holder.image.setVisibility(View.GONE);
            } else {
                Glide.with(context)
                        .load(datum.getImage())
                        .into(holder.image);
            }
        } catch (Exception e) {
            holder.image.setVisibility(View.GONE);

        }


    }

    @Override
    public int getItemCount() {
        return notifData.size();
    }

    public class NotfDAta extends RecyclerView.ViewHolder {
        @BindView(R.id.notf_image)
        ImageView image;
        @BindView(R.id.notf_content)
        TextView content;
        @BindView(R.id.notf_title)
        TextView title;

        public NotfDAta(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
