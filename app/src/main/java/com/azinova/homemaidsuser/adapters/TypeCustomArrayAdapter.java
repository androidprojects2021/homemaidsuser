package com.azinova.homemaidsuser.adapters;

/**
 * Created by azinova-mac6 on 06/12/17.
 */

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.azinova.homemaidsuser.MainActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.fragments.RegisterFragment;
import com.azinova.homemaidsuser.models.Area;
import com.azinova.homemaidsuser.models.CustomerTypes;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class TypeCustomArrayAdapter extends ArrayAdapter<CustomerTypes> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<CustomerTypes> items;
    private final int mResource;

    public TypeCustomArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                                  @NonNull List<CustomerTypes> dataset) {
        super(context, resource, 0, dataset);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = dataset;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mResource, parent, false);
        final CustomerTypes item = items.get(position);

        TextView areatext = convertView.findViewById(R.id.spinner_text);

        areatext.setText(item.getType());

        areatext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new RegisterFragment.TypeClickEvent(item.getType(), item.getTid(), item.getDescription()));
            }
        });

        return convertView;
    }
}
