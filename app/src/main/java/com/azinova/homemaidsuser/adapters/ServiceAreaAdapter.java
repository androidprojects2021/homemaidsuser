package com.azinova.homemaidsuser.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.ServiceAreaActivity;
import com.azinova.homemaidsuser.models.Area;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 22/11/17.
 */

public class ServiceAreaAdapter extends RecyclerView.Adapter<ServiceAreaAdapter.HolderData> {

    private Context context;
    private List<Area> data = new ArrayList<>();
    private int pos = 0;
    private AdapterCallback adapterCallback;

    public ServiceAreaAdapter(Context context, List<Area> data) {
        this.adapterCallback = ((AdapterCallback) context);
        this.context = context;
        this.data = data;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service_area, parent, false);
        return new HolderData(view);
    }

    @Override
    public void onBindViewHolder(final HolderData holder, final int position) {
        final Area areaDatum = data.get(position);
        holder.area_name.setText(areaDatum.getAreaName());
        holder.line.setVisibility(View.INVISIBLE);
        holder.area_name.setTypeface(Typeface.DEFAULT);

        if (ServiceAreaActivity.pos == position) {
            holder.line.setVisibility(View.VISIBLE);
            holder.area_name.setTypeface(holder.area_name.getTypeface(), Typeface.BOLD);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (areaDatum.getLatitude().isEmpty() || areaDatum.getLongitude().isEmpty()) {

                } else {
                    adapterCallback.onMethodCallback(new LatLng(Double.parseDouble(areaDatum.getLatitude()), Double.parseDouble(areaDatum.getLongitude())), areaDatum.getAreaName());
                    ServiceAreaActivity.pos = position;
                    notifyDataSetChanged();
                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static interface AdapterCallback {
        void onMethodCallback(LatLng latLng, String title);
    }

    public class HolderData extends RecyclerView.ViewHolder {
        @BindView(R.id.ser_area_layout)
        ConstraintLayout layout;
        @BindView(R.id.ser_area_name)
        TextView area_name;
        @BindView(R.id.ser_area_line)
        View line;

        public HolderData(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
