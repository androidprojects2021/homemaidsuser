package com.azinova.homemaidsuser.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.fragments.ImageViewerDialog;
import com.azinova.homemaidsuser.models.CatImg;
import com.azinova.homemaidsuser.utils.GlideUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leo elstin on 11/15/17.
 */

public class GalleryViewAdapter extends RecyclerView.Adapter<GalleryViewAdapter.ViewHolder> {

    public static List<CatImg> galleries = new ArrayList<>();
    public static int position_;
    private Context context;


    public GalleryViewAdapter(List<CatImg> galleries, Context context) {
        this.galleries = galleries;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_gallery_view, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final CatImg gallery = galleries.get(position);
        GlideUtils.loadImage(gallery.getImgUrl(), holder.imageView, context);
        holder.title.setVisibility(View.GONE);

        if (gallery.getImgUrl() != null) {
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Activity activity = (Activity) context;
                    ImageViewerDialog dialog = new ImageViewerDialog();
                    dialog.show(activity.getFragmentManager(), "");
                    position_ = position;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return galleries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.title)
        TextView title;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
