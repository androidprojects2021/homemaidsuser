package com.azinova.homemaidsuser.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.fragments.ServiceDetailDialog;
import com.azinova.homemaidsuser.models.ServiceList;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abins Shaji on 15/11/17.
 */

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ServiceData> {

    List<ServiceList> serviceLists = new ArrayList<>();
    Context context;

    public ServiceListAdapter(Context context, List<ServiceList> serviceLists) {
        this.context = context;
        this.serviceLists = serviceLists;
    }

    @Override
    public ServiceData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_service_list, parent, false);
        return new ServiceData(view);
    }

    @Override
    public void onBindViewHolder(final ServiceData holder, final int position) {

        final ServiceList list = serviceLists.get(position);
        holder.title.setText(list.getTitle());
        Glide.with(context)
                .applyDefaultRequestOptions(RequestOptions.placeholderOf(R.drawable.mainlogo))
                .load(list.getImage())
                .into(holder.imageView);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                bundle.putString("title", list.getTitle());
                bundle.putString("content", list.getContent());
                bundle.putString("image", list.getImage());
                ServiceDetailDialog detailDialog = new ServiceDetailDialog();
                detailDialog.setArguments(bundle);
                detailDialog.show(((Activity) context).getFragmentManager(), "1");
            }
        });

    }

    @Override
    public int getItemCount() {
        return serviceLists.size();
    }

    public class ServiceData extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView title, content;
        CardView card;

        public ServiceData(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.service_image);
            title = itemView.findViewById(R.id.service_title);
            card = itemView.findViewById(R.id.card_service_list);


        }
    }
}
