package com.azinova.homemaidsuser.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.HelpActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.fragments.HelpFrag;
import com.azinova.homemaidsuser.models.HelpDatum;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 14/12/17.
 */

public class Help_adapter extends RecyclerView.Adapter<Help_adapter.HelpData> {
    Context context;
    List<HelpDatum> modelList = new ArrayList<>();

    public Help_adapter(HelpActivity helpActivity, List<HelpDatum> modelList) {
        context = helpActivity;
        this.modelList = modelList;

    }

    @Override
    public HelpData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_help, parent, false);
        return new HelpData(view);
    }

    @Override
    public void onBindViewHolder(HelpData holder, int position) {
//        HelpDatum helpDatum=modelList.get(position);
//        holder.title.setText(helpDatum.getTitle());
        holder.setData(modelList.get(position), position);


    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class HelpData extends RecyclerView.ViewHolder {
        FragmentActivity fragmentActivity = (FragmentActivity) (context);
        Bundle bundle = new Bundle();
        HelpFrag helpFrag = new HelpFrag();
        @BindView(R.id.view_line)
        View line;
        @BindView(R.id.help_title)
        TextView title;
        @BindView(R.id.help_adapter_linear)
        LinearLayout layout;
        public HelpData(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(final HelpDatum data, int p) {
            title.setText(data.getTitle());
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bundle.putString("content", data.getContent());
                    bundle.putString("title", data.getTitle());
                    helpFrag.setArguments(bundle);
                    helpFrag.show(fragmentActivity.getFragmentManager(), "");

                }
            });
//            if(p==modelList.size()-1)
//                line.setVisibility(View.INVISIBLE);

        }
    }
}
