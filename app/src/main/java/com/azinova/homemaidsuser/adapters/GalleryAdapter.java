package com.azinova.homemaidsuser.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.GalleryViewActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.Gallery;
import com.azinova.homemaidsuser.utils.GlideUtils;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Leo elstin on 11/15/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private List<Gallery> galleries = new ArrayList<>();
    private Context context;

    public GalleryAdapter(List<Gallery> galleries, Context context) {
        this.galleries = galleries;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_gallery, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final Gallery gallery = galleries.get(position);
        GlideUtils.loadImage(gallery.getCatImgUrl(), holder.imageView, context);
        holder.title.setText(gallery.getCatName());

        if (gallery.getCatImgs() != null) {
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String result = new Gson().toJson(gallery);
                    LogUtil.e(result);
                    Intent intent = new Intent(context, GalleryViewActivity.class);
                    intent.putExtra("json", result);
                    Activity activity = (Activity) context;

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(activity, holder.cardView, "splashing");
                    activity.startActivity(intent, options.toBundle());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return galleries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.cardView)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
