package com.azinova.homemaidsuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.json.JSONObject;


import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class ComplaintsNew extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints_new);

        JSONObject jsonObject=new JSONObject();

        Log.e("ComplaintsNew  ","onCreate  ");

        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET,
                UrlUtils.getComplaintsList(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("log test  success  ","log test");

                Log.e("success  ",response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("log test  error","log test");

                Log.e("error  ",error.toString());

            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public void backClick(View view) {
        onBackPressed();
    }
}
