package com.azinova.homemaidsuser;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.payment.PaymentWebViewActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.PaymentUtils;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.Shared;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import mumbai.dev.sdkdubai.BillingAddress;
import mumbai.dev.sdkdubai.CustomModel;
import mumbai.dev.sdkdubai.MerchantDetails;
import mumbai.dev.sdkdubai.ShippingAddress;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;


public class BookedDetailActivity extends AppCompatActivity implements CustomModel.OnCustomStateListener {

    private RelativeLayout backButton;
    private RelativeLayout payButton;
    private String total_price;
    private String service_total, price_splits, maid_splits, date, time, addresstext, areatext;
    private String booking_id,reference_id,day_service_id,service_date_new;
    private TextView total_price_text;
    private TextView service_price;
    private TextView maid_split, price_split;
    private TextView bookingId;
    private TextView date_text, time_text;
    private RelativeLayout help_lay;
    private TextView address;
    private TextView area;
    private TextView vat_texter, vat_price_texter;
    private String vat_text;
    private String vat_price;
    private ProgressDialog pDialog;
    private Prefs pref;
    private CustomerModels customerModels;
    private CustomerDetails model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_detail);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        CustomModel.getInstance().setListener(this);  // Payment Gateway state listener

        pref = Prefs.with(BookedDetailActivity.this);
        customerModels = pref.getUserDetails();
        model = customerModels.getCustomerDetails();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            total_price = extras.getString("total_price", "error");
            service_total = extras.getString("service_total", "");
            price_splits = extras.getString("price_splits", "");
            maid_splits = extras.getString("maid_splits", "");
            booking_id = extras.getString("booking_id", "");

            reference_id = extras.getString("reference_id", "");
            service_date_new = extras.getString("service_date_new", "");
            day_service_id = extras.getString("day_service_id", "");

            date = extras.getString("date", "");
            time = extras.getString("time", "");
            addresstext = extras.getString("address", "");
            areatext = extras.getString("area", "");
            vat_text = extras.getString("vat", "");
            vat_price = extras.getString("vat_amount", "");

            Shared.pay_bookingid = booking_id;
            Shared.pay_total = total_price;

        }

        initView();
        clickListeners();

        bookingId.setText(booking_id);
        total_price_text.setText(total_price);
        service_price.setText("AED " + service_total);
        price_split.setText(price_splits);
        maid_split.setText(maid_splits);
        date_text.setText(date);
        time_text.setText(time);
        address.setText(addresstext);
        area.setText(areatext);
        vat_texter.setText(vat_text);
        vat_price_texter.setText("AED " + vat_price);

        EventBus.getDefault().post(new ConfirmBookActivity.finishEvent());
        EventBus.getDefault().post(new BookNowActivity.finishEvent());

    }

    private void clickListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent pay = new Intent(BookedDetailActivity.this, PaymentSuccessActivity.class);
//                startActivity(pay);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//                Intent pay = new Intent(BookedDetailActivity.this, PaymentActivity.class);
//                startActivity(pay);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//                finishBackActivities();


                /*new modification alert for choosing payment mode*/
                showAlertDialog();


                /*old paymentt method call given below*/
//                PaymentEngine();
            }
        });

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(BookedDetailActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });
    }

    private void showAlertDialog() {


        AlertDialog.Builder builder = new AlertDialog.Builder(BookedDetailActivity.this);
        View view = LayoutInflater.from(BookedDetailActivity.this).inflate(R.layout.alert_payment_chooser, null);

        LinearLayout card = view.findViewById(R.id.card);
        LinearLayout cash = view.findViewById(R.id.cash);

        ImageView imageView_close_alert = view.findViewById(R.id.imageView_close_alert);



        builder.setView(view);

        AlertDialog alertDialog = builder.create();

        imageView_close_alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent webview_payment = new Intent(BookedDetailActivity.this, PaymentWebViewActivity.class);
                webview_payment.putExtra("booking_id", booking_id);
                webview_payment.putExtra("reference_id", reference_id);
                webview_payment.putExtra("price", total_price);
                webview_payment.putExtra("from","book_detail");

                webview_payment.putExtra("service_date_new", service_date_new);
                webview_payment.putExtra("day_service_id",day_service_id );

                startActivity(webview_payment);

                alertDialog.dismiss();
            }
        });

        cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent boohis = new Intent(BookedDetailActivity.this, BookingHistoryActivity.class);
                startActivity(boohis);
                alertDialog.dismiss();
            }
        });


        alertDialog.show();
    }

//    private void showAlertDialog(Context context) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//        View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.alert_payment_chooser, null);
//
//        LinearLayout card = view.findViewById(R.id.card);
//        LinearLayout cash = view.findViewById(R.id.cash);
//
//
//        builder.setView(view);
//
//        final AlertDialog alertDialog = builder.create();
//
//        card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                Intent webview_payment = new Intent(BookedDetailActivity.this, PaymentWebViewActivity.class);
//                webview_payment.putExtra("booking_id", booking_id);
//                startActivity(webview_payment);
//
//                alertDialog.dismiss();
//            }
//        });
//
//        cash.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent boohis = new Intent(BookedDetailActivity.this, BookingHistoryActivity.class);
//                startActivity(boohis);
//                alertDialog.dismiss();
//            }
//        });
//
//
//        alertDialog.show();
//    }

    private void initView() {
        backButton = findViewById(R.id.backbutton);
        payButton = findViewById(R.id.paynowButton);
        bookingId = findViewById(R.id.booking_id);
        service_price = findViewById(R.id.service_price);
        total_price_text = findViewById(R.id.total_price_text);
        price_split = findViewById(R.id.price_split);
        maid_split = findViewById(R.id.maid_split);
        date_text = findViewById(R.id.date_text);
        time_text = findViewById(R.id.time_text);
        help_lay = findViewById(R.id.help_lay);
        address = findViewById(R.id.address);
        area = findViewById(R.id.area);
        vat_texter = findViewById(R.id.vat_texter);
        vat_price_texter = findViewById(R.id.vat_price_texter);
    }

    @Override
    public void onBackPressed() {
        finishBackActivities();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(finishEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void finishBackActivities() {
        finish();
//        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void stateChanged() {

//        String modelState = CustomModel.getInstance().getState();
//
//        Log.e("Status ", modelState);
//
//        PaymentSuccess payment = new Gson().fromJson(modelState, PaymentSuccess.class);
//
//        if (payment.getOrder_status().equalsIgnoreCase("success")) {
//            Intent intent = new Intent(getApplicationContext(), PaymentSuccessActivity.class);
//            intent.putExtra("transStatus", payment.getOrder_status());
//            intent.putExtra("price", payment.getAmount());
//            intent.putExtra("order_id", payment.getOrder_id());
//            startActivity(intent);
//            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
//        }
//
//        finish();
    }

    public static class finishEvent {

    }

    private void PaymentEngine() {

        pDialog = new ProgressDialog(BookedDetailActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject paydetails = new JSONObject();
        try {
            paydetails.put("customer_id", pref.getCustomerId());
            paydetails.put("booking_id", booking_id);
            paydetails.put("price", total_price);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest paymentObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getOrderId(), paydetails,
                response -> {
                    pDialog.dismiss();
                    try {
                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            Snackbar.make(payButton, "Something went wrong!", Snackbar.LENGTH_LONG).show();
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {

/*

   ************************************************************ old CC Avenue *************************************************************************

                            String vAccessCode = ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim();
                            String vMerchantId = ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim();
                            String vCurrency = ServiceUtility.chkNull("AED").toString().trim();
                            String vAmount = ServiceUtility.chkNull(total_price).toString().trim(); //total_price
                            if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
                                Intent intent = new Intent(BookedDetailActivity.this, WebViewActivity.class);
                                intent.putExtra(AvenuesParams.ACCESS_CODE, ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim());
                                intent.putExtra(AvenuesParams.MERCHANT_ID, ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim());
                                intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(response.getString("order_id")).toString().trim());
                                intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull("AED").toString().trim());
                                intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(total_price).toString().trim()); //total_price
                                intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(PaymentUtils.RSA_URL).toString().trim());


                                intent.putExtra(AvenuesParams.BILLING_NAME, model.getName());
                                if (customerModels.getAreaList().size() > 0) {
                                    intent.putExtra(AvenuesParams.BILLING_ADDRESS, customerModels.getAreaList().get(0).getCustomerAddress());
                                    intent.putExtra(AvenuesParams.BILLING_CITY, customerModels.getAreaList().get(0).getAreaName());
                                }
                                intent.putExtra(AvenuesParams.BILLING_COUNTRY, "United Arab Emirates");
                                intent.putExtra(AvenuesParams.BILLING_TEL, model.getPhone());
                                intent.putExtra(AvenuesParams.BILLING_EMAIL, model.getEmail());

                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            } else {
                                Toast.makeText(BookedDetailActivity.this, "Toast: " + "Something went wrong!", Toast.LENGTH_LONG).show();
                            }

   ************************************************************ old CC Avenue *************************************************************************
*/

                            MerchantDetails m = new MerchantDetails();

                            m.setCurrency("AED");
                            m.setAmount(total_price);
                            m.setAccess_code(PaymentUtils.AccessCode);
                            m.setMerchant_id(PaymentUtils.MerchantID);
                            m.setRedirect_url(PaymentUtils.RedirectUrl);
                            m.setCancel_url(PaymentUtils.RedirectUrl);
                            m.setRsa_url(PaymentUtils.RSA_URL);
                            m.setOrder_id(response.getString("order_id"));
                            m.setCustomer_id(pref.getCustomerId());
                            m.setPromo_code("");
                            m.setAdd1("add1");
                            m.setAdd2("add2");
                            m.setAdd3("add3");
                            m.setAdd4("add4");
                            m.setAdd5("add5");

                            BillingAddress billingAddress = new BillingAddress();

                            billingAddress.setName(customerModels.getCustomerDetails().getName());
                            billingAddress.setAddress(addresstext);
                            billingAddress.setCountry("Dubai");
                            billingAddress.setState("Dubai");
                            billingAddress.setCity("Dubai");
                            billingAddress.setTelephone(model.getPhone());
                            billingAddress.setEmail(model.getEmail());

                            ShippingAddress s = new ShippingAddress();
                            s.setName(customerModels.getCustomerDetails().getName());
                            s.setAddress(addresstext);
                            s.setCountry("Dubai");
                            s.setState("Dubai");
                            s.setCity("Dubai");
                            s.setTelephone(model.getPhone());

                            Intent i = new Intent(BookedDetailActivity.this, PaymentActivity.class);

                            i.putExtra("merchant", m);
                            i.putExtra("billing", billingAddress);
                            i.putExtra("shipping", s);

                            startActivity(i);

                        }
                        else {
                            Toast.makeText(BookedDetailActivity.this, "Toast: " + "Something went wrong!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }, error -> {
            pDialog.dismiss();
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ServerError) {
                message = "The server could not be found.\nPlease try again after some time!!";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ParseError) {
                message = "Something went wrong!\nPlease try again after some time!!";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut!\nPlease check your internet connection.";
            }

            Snackbar.make(payButton, message, Snackbar.LENGTH_LONG).show();
        });

        paymentObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(paymentObjReq);

    }
}
