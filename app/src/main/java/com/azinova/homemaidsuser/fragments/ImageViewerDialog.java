package com.azinova.homemaidsuser.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.adapters.GalleryViewAdapter;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 15/11/17.
 */

public class ImageViewerDialog extends DialogFragment {


    MyViewPagerAdapter customPagerAdapter;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.imageCount)
    TextView imageCount;
    @BindView(R.id.close)
    ImageView close;
    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            imageCount.setText(String.valueOf(position + 1 + " / " + GalleryViewAdapter.galleries.size()));
        }

        @Override
        public void onPageSelected(int position) {


        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
    private Dialog mDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        if (mDialog == null) {
            mDialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
            mDialog.setContentView(R.layout.dialog_image_viewer);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.setCancelable(false);
            mDialog.getWindow().setGravity(Gravity.CENTER);
            View view = mDialog.getWindow().getDecorView();
            ButterKnife.bind(this, view);
        }

        customPagerAdapter = new MyViewPagerAdapter();

        viewPager.setAdapter(customPagerAdapter);
        viewPager.setCurrentItem(GalleryViewAdapter.position_);
        mDialog.setCancelable(false);
        viewPager.addOnPageChangeListener(pageChangeListener);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
//                mDialog.cancel();
            }
        });
        return mDialog;


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public class MyViewPagerAdapter extends PagerAdapter {

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.imageview_adapter, container, false);

            ImageView imageViewPreview = view.findViewById(R.id.imageView);


            Glide.with(getActivity()).load(GalleryViewAdapter.galleries.get(position).getImgUrl())

                    .into(imageViewPreview);


            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return GalleryViewAdapter.galleries.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }


}
