package com.azinova.homemaidsuser.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 13/12/17.
 */

public class CutomerTypeFrag extends DialogFragment {
    public static final String TAG = "message";
    Dialog dialog;
    @BindView(R.id.help_title)
    TextView title;
    @BindView(R.id.help_inst)
    TextView content;
    @BindView(R.id.close)
    RelativeLayout close;

    public CutomerTypeFrag() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            dialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
            dialog.setContentView(R.layout.frag_customertype);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().setGravity(Gravity.CENTER);

        }
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        if(getArguments().getString("content")!=null){
            title.setText(getArguments().getString("title"));
            content.setText(Html.fromHtml(getArguments().getString("content")));
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
        }

        return dialog;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        super.onViewCreated(view, savedInstanceState);
    }
}
