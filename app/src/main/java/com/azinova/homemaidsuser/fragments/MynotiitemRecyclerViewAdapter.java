package com.azinova.homemaidsuser.fragments;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.NotifDatum;

import java.util.List;

import static android.text.format.DateUtils.getRelativeTimeSpanString;


public class MynotiitemRecyclerViewAdapter extends RecyclerView.Adapter<MynotiitemRecyclerViewAdapter.ViewHolder> {

    private List<NotifDatum> notifData;
    private Context context;

    public MynotiitemRecyclerViewAdapter(List<NotifDatum> items, Context context) {
        this.notifData = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_home_noti, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        NotifDatum datum = notifData.get(position);
        holder.content.setText(datum.getContent());
        holder.title.setText(datum.getTitle());
        holder.timestamp.setText(getRelativeTimeSpanString(Long.parseLong(datum.getCreated_on()),System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS));

    }

    @Override
    public int getItemCount() {
        return notifData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public TextView content;
        public TextView title;
        public TextView timestamp;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            content = (TextView) view.findViewById(R.id.notf_content);
            title = (TextView) view.findViewById(R.id.notf_title);
            timestamp = view.findViewById(R.id.timestamp);
        }

    }
}
