package com.azinova.homemaidsuser.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.azinova.homemaidsuser.ConfirmBookActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.adapters.AddressAdapter;
import com.azinova.homemaidsuser.utils.Prefs;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Abins Shaji on 08/01/18.
 */

public class ChooseAdressDialog extends DialogFragment {
    private Dialog dialog;
    @BindView(R.id.close)
    ImageView close;
    @BindView(R.id.add_address_recycler)
    RecyclerView recycler;
    AddressAdapter adapter;
    String muladdressid;

    protected  Prefs prefs;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        prefs=Prefs.with(getActivity());
        if (dialog == null) {
            dialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
            dialog.setContentView(R.layout.frag_add_address);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().setGravity(Gravity.CENTER);

        }
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new ConfirmBookActivity.AddressEvent(prefs.getUserDetails().getAreaList().get(0).getCustomerAddressId(),
                        prefs.getUserDetails().getAreaList().get(0).getCustomerAddress(),prefs.getUserDetails().getAreaList().get(0).getAreaName()));
                dismiss();
            }
        });
        init();

        return dialog;

    }
    @OnClick(R.id.submitClick)public void submitClick(View view)
    {

        dialog.dismiss();

    }

    private void init()
    {
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayout.VERTICAL));
        if(adapter==null)
        {
            adapter=new AddressAdapter(prefs.getUserDetails().getAreaList(),dialog);
            recycler.setAdapter(adapter);
        }
    }




}
