package com.azinova.homemaidsuser.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.BookNowActivity;
import com.azinova.homemaidsuser.MainActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.SmsVerifyActivity;
import com.azinova.homemaidsuser.adapters.TypeCustomArrayAdapter;
import com.azinova.homemaidsuser.models.CustomerTypes;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.Shared;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.Shared.offermodel;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

/**
 * ABGthDev
 */

public class RegisterFragment extends Fragment {

    public static final String TAG = "Registerfragment";
    @BindView(R.id.reg_user_name)
    EditText name;
    @BindView(R.id.reg_user_email)
    EditText email;
    @BindView(R.id.reg_user_phone)
    EditText phone;
    @BindView(R.id.reg_user_pwd)
    EditText pwd;
    @BindView(R.id.reg_click)
    RelativeLayout reg_click;
    @BindView(R.id.reg_fb_linear)
    LinearLayout fb_click;
    @BindView(R.id.tanc_click)
    LinearLayout tandc_click;
    @BindView(R.id.eyelay)
    RelativeLayout eyeLay;
    @BindView(R.id.eyeimage)
    ImageView eyeImage;
    @BindView(R.id.type_spinner)
    AutoCompleteTextView typespinner;
    @BindView(R.id.container)
    LinearLayout container;
    private View registerView;
    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;
    private Prefs prefs;
    private String FCM_token;
    private ProgressDialog pDialog;
    private boolean isshow = true;
    private boolean isonthego;
    private List<CustomerTypes> custypes;
    private String customer_type_id;
    private FragmentActivity fragmentActivity;
    private Bundle bundle;
    private CutomerTypeFrag customertype;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public RegisterFragment() {
        custypes = new ArrayList<>();
    }


    /* new FCM update :
            By REEMA on 18/05/2022
            for next playstore update (current version code - 25 )
            FCM - validation , prefrence save
    */
    @Override
    public void onResume() {
        super.onResume();
        FCM_token = FirebaseInstanceId.getInstance().getToken();

        if (FCM_token != null) {
            if (FCM_token.isEmpty()) {
                FCM_token = "12345";
            }
        }

        Log.d("TAG", "onResume: " + FCM_token);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();
        prefs = Prefs.with(getActivity());

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Creating new account...");
        pDialog.setCancelable(false);

        fragmentActivity = (FragmentActivity) (getActivity());
        bundle = new Bundle();
        customertype = new CutomerTypeFrag();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        isonthego = getArguments().getBoolean("isonthego", false);
        if (registerView != null) {
            ViewGroup parent = (ViewGroup) registerView.getParent();
            if (parent == null) {
                parent = container;
            }
            parent.removeView(registerView);

            return registerView;
        }

        registerView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, registerView);
        typeSpinnerengine();

        reg_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getActivity().getCurrentFocus() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
                validate(); //// TODO: 20/12/17 commneted for testing purpose

            }
        });

        fb_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fbClick();

            }
        });
        tandc_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TermsConditionFragment termsConditionFragment = new TermsConditionFragment();

                getFragmentManager().beginTransaction().replace(R.id.layout_fragment, termsConditionFragment).addToBackStack(null).commit();
            }
        });

        eyeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isshow) {
                    eyeImage.setImageResource(R.drawable.ic_visibility_black_24dp);
                    pwd.setTransformationMethod(null);
                    isshow = false;
                    pwd.setSelection(pwd.getText().length());
                } else {
                    eyeImage.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                    pwd.setTransformationMethod(new PasswordTransformationMethod());
                    isshow = true;
                    pwd.setSelection(pwd.getText().length());
                }
            }
        });

        typespinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (custypes.size() > 1) {
                    if (typespinner.isPopupShowing()) {
                        typespinner.dismissDropDown();
                    } else {
                        typespinner.showDropDown();
                    }
                }

            }
        });


        if (Shared.mobile_number != null) {
            phone.setText(Shared.mobile_number);
        }

        phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    if(phone.getText().toString().isEmpty())
                    phone.setText("+971");
                }else{
                    
                }
            }
        });

        return registerView;

    }

    private void fbClick() {
        LogUtil.e(TAG, "fbClick: ");
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        pDialog.show();
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        LogUtil.e(TAG, "cancelled");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        LogUtil.e(TAG, exception.toString());

                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(final AccessToken token) {

        //progressBar.setVisibility(View.VISIBLE);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            // progressBar.setVisibility(View.GONE);
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("oauth_token", token.getUserId()); //for facebook
                                jsonObject.put("email", user.getEmail());
                                jsonObject.put("name", user.getDisplayName());
                                //jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken()); //for fcm
                                jsonObject.put("device_id", FCM_token); //for fcm
                                jsonObject.put("oauth_provider", "facebook");
                                jsonObject.put("device_type", "android");
                                prefs.setisfacebook(true);
                                regUserApi(jsonObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //startActivity(new Intent(SignInActivity.this, LogoutTestActivity.class));
                        } else {
                            // progressBar.setVisibility(View.GONE);

                        }

                    }
                });
    }

    private void validate() {
        if (name.getText().toString().isEmpty()) {
            name.setError("Enter your name!");
        } else if (!isValidEmail(email.getText().toString())) {
            email.setError("Enter valid email address!");
        } else if (phone.getText().length() == 0 || phone.getText().length() < 6) {
            phone.setError("Enter valid phone number!");
        } else if (pwd.getText().toString().isEmpty()) {
            pwd.setError("Enter valid password!");
        } else if (pwd.getText().length() < 6) {
            pwd.setError("Password must contain minimum 6 characters!");
        } else {
            JSONObject jsonobject = new JSONObject();
            Log.e(TAG, "firebaseinstance ID: " + FCM_token);
            Log.d("TAG", "validate: "+phone.getText().toString());
            try {
                jsonobject.put("name", name.getText().toString());
                jsonobject.put("email", email.getText().toString());
                jsonobject.put("phone", phone.getText().toString());
                jsonobject.put("password", pwd.getText().toString());
                jsonobject.put("oauth_provider", "normal");
               // jsonobject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                jsonobject.put("device_id", FCM_token);
                jsonobject.put("device_type", "android");
                jsonobject.put("cust_type", customer_type_id);
                if (Shared.sharedareaid != null)
                    jsonobject.put("area_id", Shared.sharedareaid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pDialog.show();
            regUserApi(jsonobject);
        }

    }

    private void regUserApi(JSONObject object) {
        LogUtil.e(TAG, "regUserApi: " + object.toString());
        JsonObjectRequest regUserRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.customerRegister(), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, "onResponse: " + response.toString());
                pDialog.dismiss();
                try {
                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {
                        Snackbar.make(registerView, response.getString("message"), Snackbar.LENGTH_SHORT).show();
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {
                       // prefs.setFCMTOKEN(FirebaseInstanceId.getInstance().getToken());
                        prefs.setFCMTOKEN(FCM_token);
                        prefs.setCustomerId(response.getJSONObject("customer_details").getString("customer_id"));
//
                        prefs.setEmail(response.getJSONObject("customer_details").getString("email"));
                        prefs.setName(response.getJSONObject("customer_details").getString("name"));
//                        prefs.setAddress(response.getJSONObject("customer_details").getString("customer_address"));


                        prefs.setisverfied(response.getJSONObject("customer_details").getBoolean("isverified"));
                        prefs.setUserDetails(response.toString());
                        prefs.setSignedin(true);

                        EventBus.getDefault().post(new MainActivity.LoginUiUpdate(true)); //update main ui according to login status

                        if (!prefs.getisverfied()) { //// TODO: 20/12/17 need to check conditions here ie, sms verified or not

                            Intent sms = new Intent(getActivity(), SmsVerifyActivity.class);
                            if (isonthego)
                                sms.putExtra("isonthego", true);
                            else
                                sms.putExtra("isonthego", false);
                            startActivity(sms);
                        } else {
                            if (isonthego)
                                EventBus.getDefault().post(new BookNowActivity.GotoConfirm());
                        }
                        getActivity().finish();
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(registerView, "Something went wrong", Snackbar.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(registerView, message, Snackbar.LENGTH_SHORT).show();
            }
        });
        regUserRequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(regUserRequest);

    }

    private void typeSpinnerengine() {

        custypes = offermodel.getTypes();
//        for (int i = 0; i < times.length(); i++)
//            try {
//                custypes.add(times.getString(i));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        typespinner.setText(custypes.get(0).getType());
        customer_type_id = custypes.get(0).getTid();
        TypeCustomArrayAdapter typeAdapter = new TypeCustomArrayAdapter(getActivity(),
                R.layout.custom_spinner_item, custypes);
        typeAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        typespinner.setThreshold(100);
        typespinner.setAdapter(typeAdapter);

        DisplayMetrics metrics = container.getResources().getDisplayMetrics();
        float dp = 150f;
        float fpixels = metrics.density * dp;
        final int pixels = (int) (fpixels + 0.5f);

        if (custypes.size() < 4) {
            typespinner.setDropDownHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
        } else {
            typespinner.setDropDownHeight(pixels);//250
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TypeClickEvent event) {

        typespinner.setText(event.typename);
        customer_type_id = event.typeid;

        if (event.description != null && !event.description.equalsIgnoreCase("") && !event.description.isEmpty() ) {

            bundle.putString("content", event.description);
            bundle.putString("title", event.typename);
            customertype.setArguments(bundle);
            customertype.show(fragmentActivity.getFragmentManager(), "");
        }

    }

    public static class TypeClickEvent {
        public String typename;
        public String typeid;
        public String description;

        public TypeClickEvent(String name, String id, String description) {
            this.typename = name;
            this.typeid = id;
            this.description = description;
        }

    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }
}
