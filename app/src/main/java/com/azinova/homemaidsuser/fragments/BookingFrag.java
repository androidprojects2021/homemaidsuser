package com.azinova.homemaidsuser.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.adapters.BookingHistoryAdapter;
import com.azinova.homemaidsuser.adapters.BookingHistoryAdapterPrev;
import com.azinova.homemaidsuser.models.BookingHistory;
import com.azinova.homemaidsuser.models.CurHistory;
import com.azinova.homemaidsuser.models.PreHistory;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BookingFrag extends Fragment {

    public static final String TAG = "comment";
    @BindView(R.id.recyclerView)
    RecyclerView bookinghistoryRecyc;
    BookingHistoryAdapter adapter;
    BookingHistoryAdapterPrev adapterprev;
    BookingHistory bookingHistoryModel;
    List<PreHistory> prev = new ArrayList<>();
    List<CurHistory> cur = new ArrayList<>();
    private LottieAnimationView animationViewfrag;
    private LinearLayout errorLay;
    private TextView hiserrortextfrag;

    public BookingFrag() {
        // Required empty public constructor
    }

    public BookingFrag newInstance(String s, String type) {
        BookingFrag myFragment = new BookingFrag();

        Bundle args = new Bundle();
        args.putString("json", s);
        args.putString("type", type);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_booking, container, false);
        ButterKnife.bind(this, view);
        setRecyclerView();

        errorLay = view.findViewById(R.id.hiserrorlayfrag);
        animationViewfrag = view.findViewById(R.id.animation_view_frag);
        hiserrortextfrag = view.findViewById(R.id.hiserrortextfrag);
        animationViewfrag.setAnimation("error_animation.json");
        animationViewfrag.loop(true);

        if (animationViewfrag.isAnimating())
            animationViewfrag.cancelAnimation();
        bookingHistoryModel = new Gson().fromJson(getArguments().getString("json"), BookingHistory.class);

        prev.clear();
        cur.clear();
        prev.addAll(bookingHistoryModel.getPreHistory());
        cur.addAll(bookingHistoryModel.getCurHistory());

        if (getArguments().getString("type").equalsIgnoreCase("current")) {
            if (cur.size() == 0) {
                errorLay.setVisibility(View.VISIBLE);
                hiserrortextfrag.setText("No active bookings!!");
                animationViewfrag.playAnimation();
            } else {
                errorLay.setVisibility(View.GONE);
                adapter = new BookingHistoryAdapter(cur, getContext());
                bookinghistoryRecyc.setAdapter(new BookingHistoryAdapter(cur, getContext()));
                adapter.notifyDataSetChanged();
                bookinghistoryRecyc.scheduleLayoutAnimation();
            }
        } else {
            if (prev.size() == 0) {
                errorLay.setVisibility(View.VISIBLE);
                hiserrortextfrag.setText("No past bookings!!");
                animationViewfrag.playAnimation();
            } else {
                errorLay.setVisibility(View.GONE);
                adapterprev = new BookingHistoryAdapterPrev(prev, getContext());
                bookinghistoryRecyc.setAdapter(adapterprev);
                adapterprev.notifyDataSetChanged();
                bookinghistoryRecyc.scheduleLayoutAnimation();
            }

        }

        return view;
    }

    private void setRecyclerView() {

        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down); //recyclerview animation
        bookinghistoryRecyc.setLayoutManager(new LinearLayoutManager(getActivity()));
        bookinghistoryRecyc.setLayoutAnimation(controller);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AdapterRefresh event) {
        prev.clear();
        cur.clear();
        prev.addAll(event.respose.getPreHistory());
        cur.addAll(event.respose.getCurHistory());
        if (getArguments().getString("type").equalsIgnoreCase("current")) {
            if (cur.size() == 0) {
                errorLay.setVisibility(View.VISIBLE);
                hiserrortextfrag.setText("No current bookings!!");
                animationViewfrag.playAnimation();
            } else {
                errorLay.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();
            }
        } else {
            if (prev.size() == 0) {
                errorLay.setVisibility(View.VISIBLE);
                hiserrortextfrag.setText("No previous bookings!!");
                animationViewfrag.playAnimation();
            } else {
                errorLay.setVisibility(View.GONE);
                adapterprev.notifyDataSetChanged();
            }

        }
        LogUtil.e("herr" + prev.toString());
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }

    public static class AdapterRefresh {
        public BookingHistory respose;

        public AdapterRefresh(BookingHistory respo) {
            this.respose = respo;
        }

    }
}