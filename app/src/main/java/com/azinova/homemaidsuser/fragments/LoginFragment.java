package com.azinova.homemaidsuser.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.BookNowActivity;
import com.azinova.homemaidsuser.ForgetPwd;
import com.azinova.homemaidsuser.MainActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.SmsVerifyActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginFragment extends Fragment {

    public static final String TAG = "LoginFragment";
    @BindView(R.id.login_email)
    TextView lemail;
    @BindView(R.id.login_pwd)
    EditText lpwd;
    @BindView(R.id.login_rel_button)
    RelativeLayout login_click;
    @BindView(R.id.facebook_button)
    RelativeLayout fb_click;
    @BindView(R.id.eyelay)
    RelativeLayout eyeLay;
    @BindView(R.id.eyeimage)
    ImageView eyeImage;
    @BindView(R.id.forgot_pass)
    TextView forgotpass;
    private View loginView;
    private Prefs prefs;
    private String FCM_token;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private ProgressDialog pDialog;
    private boolean isshow = true;
    private boolean isonthego;

    private static boolean

    isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        callbackManager = CallbackManager.Factory.create();

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Login to your account...");
        pDialog.setCancelable(false);

    }

   /* new FCM update :
            By REEMA on 18/05/2022
            for next playstore update (current version code - 25 )
            FCM - validation , prefrence save
    */

    @Override
    public void onResume() {
        super.onResume();

        FCM_token = "";//FirebaseInstanceId.getInstance().getToken();

       /* if (FCM_token != null) {
            if (FCM_token.isEmpty()) {
                FCM_token = "12345";
            }
        }*/

        Log.d("TAG", "onResume: " + FCM_token);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        prefs = Prefs.with(getActivity());

        isonthego = getArguments().getBoolean("isonthego", false);
        if (loginView != null) {
            ViewGroup parent = (ViewGroup) loginView.getParent();
            if (parent == null) {
                parent = container;
            }
            parent.removeView(loginView);
            return loginView;
        }

        loginView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, loginView);
        login_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity().getCurrentFocus() != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }
                validate();

            }
        });
        fb_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fbClick();
            }
        });

        eyeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isshow) {
                    eyeImage.setImageResource(R.drawable.ic_visibility_black_24dp);
                    lpwd.setTransformationMethod(null);
                    isshow = false;
                    lpwd.setSelection(lpwd.getText().length());
                } else {
                    eyeImage.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                    lpwd.setTransformationMethod(new PasswordTransformationMethod());
                    isshow = true;
                    lpwd.setSelection(lpwd.getText().length());
                }
            }
        });

        forgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fogot = new Intent(getActivity(), ForgetPwd.class);
                startActivity(fogot);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
            }
        });

        if (isonthego) {
            LogUtil.e("onthe go: istrue");
        } else {
            LogUtil.e("onthe go: isfalse");
        }

        return loginView;
    }

    private void fbClick() {
        Log.e(TAG, "fbClick: ");
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("user_photos", "email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        pDialog.show();
                        handleFacebookAccessToken(loginResult.getAccessToken());
                        LogUtil.e(TAG, "in handle");
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        LogUtil.e(TAG, "cancelled");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        LogUtil.e(TAG, exception.toString());

                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(final AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("oauth_token", token.getUserId());
                                jsonObject.put("oauth_provider", "facebook");
                                // jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                                jsonObject.put("device_id", FCM_token);
                                jsonObject.put("device_type", "android");
                                prefs.setisfacebook(true);
                                Log.d("Login 2", "validate 2 : " + jsonObject.toString());
                                loginUserApi(jsonObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                LogUtil.e(e.toString());
                                pDialog.dismiss();
                                Snackbar.make(lpwd, "Something went wrong", Snackbar.LENGTH_SHORT).show();
                            }

                        } else {
                            LogUtil.e("unsuucessfull");
                            pDialog.dismiss();
                            Snackbar.make(lpwd, "Something went wrong", Snackbar.LENGTH_SHORT).show();

                        }


                    }
                });
    }

    private void validate() {

        if (!isValidEmail(lemail.getText().toString())) {
            lemail.setError("Enter valid email address!");
        } else if (lpwd.getText().toString().isEmpty()) {
            lpwd.setError("Enter your Password!");
        } else {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("email", lemail.getText().toString());
                jsonObject.put("password", lpwd.getText().toString());
                jsonObject.put("oauth_provider", "normal");
                // jsonObject.put("device_id", FirebaseInstanceId.getInstance().getToken());
                jsonObject.put("device_id", FCM_token);
                jsonObject.put("device_type", "android");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            pDialog.show();

            Log.d("Login 1", "validate 1 : " + jsonObject.toString());
            LogUtil.e(jsonObject.toString());

            loginUserApi(jsonObject);
        }

    }

    private void loginUserApi(JSONObject jsonObject) {
        Log.d("Login ", "Api call : " + jsonObject.toString());
        JsonObjectRequest loginReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.customerLogin(), jsonObject, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                LogUtil.e(TAG, "onResponse: " + response.toString());
                pDialog.dismiss();
                try {

                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {

                        Snackbar.make(lpwd, response.getString("message"), Snackbar.LENGTH_SHORT).show();
                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {

                       // prefs.setFCMTOKEN(FirebaseInstanceId.getInstance().getToken());
                        prefs.setFCMTOKEN(FCM_token);
                        prefs.setCustomerId(response.getJSONObject("customer_details").getString("customer_id"));

                        prefs.setEmail(response.getJSONObject("customer_details").getString("email"));
                        prefs.setName(response.getJSONObject("customer_details").getString("name"));
                        prefs.setAddress(response.getJSONArray("area_list").getJSONObject(0).get("customer_address").toString());


                        prefs.setisverfied(response.getJSONObject("customer_details").getBoolean("isverified"));
                        prefs.setUserDetails(response.toString());
                        prefs.setSignedin(true);
                        EventBus.getDefault().post(new MainActivity.LoginUiUpdate(true)); //update main ui according to login status

                        if (!prefs.getisverfied()) { //// TODO: 20/12/17 need to check conditions here ie, sms verified or not

                            Intent sms = new Intent(getActivity(), SmsVerifyActivity.class);
                            if (isonthego)
                                sms.putExtra("isonthego", true);
                            else
                                sms.putExtra("isonthego", false);
                            startActivity(sms);
                        } else {
                            if (isonthego)
                                EventBus.getDefault().post(new BookNowActivity.GotoConfirm());
                        }
                        getActivity().finish();
                        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(lpwd, "Something went wrong", Snackbar.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(lpwd, message, Snackbar.LENGTH_SHORT).show();
            }
        });

        loginReq.setRetryPolicy(new DefaultRetryPolicy(UrlUtils.DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(loginReq);
    }

}
