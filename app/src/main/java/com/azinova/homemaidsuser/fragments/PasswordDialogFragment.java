package com.azinova.homemaidsuser.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by Aßijith on 10/06/16.
 */

public class PasswordDialogFragment extends DialogFragment {

    Prefs prefs;
    private View shiftFragment;
    private RelativeLayout OkButton;
    private RelativeLayout closeLay;
    private RelativeLayout eyelay;
    private RelativeLayout eyelay1;
    private EditText cur_pass;
    private EditText new_pas;
    private ProgressDialog pDialog;
    private ImageView eyeimage;
    private boolean isshow = true;
    private ImageView eyeimage1;
    private boolean isshow1 = true;

    public PasswordDialogFragment() {

    }


    public static PasswordDialogFragment newInstance() {
        PasswordDialogFragment fragment = new PasswordDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.MessageDialogTheme);
        prefs = Prefs.with(getActivity().getApplicationContext());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MessageDialogTheme;
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            dialog.getWindow().setNavigationBarColor(getResources().getColor(R.color.primary_dark));
//        }
        dialog.setCanceledOnTouchOutside(true);

        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        shiftFragment = inflater.inflate(R.layout.dialog_change_password, container, false);
        OkButton = shiftFragment.findViewById(R.id.OkButton);
        closeLay = (RelativeLayout) shiftFragment.findViewById(R.id.closeLay1);
        cur_pass = (EditText) shiftFragment.findViewById(R.id.cur_pass);
        new_pas = (EditText) shiftFragment.findViewById(R.id.new_pass);
        eyelay = shiftFragment.findViewById(R.id.eyelay);
        eyeimage = shiftFragment.findViewById(R.id.eyeimage);
        eyelay1 = shiftFragment.findViewById(R.id.eyelay1);
        eyeimage1 = shiftFragment.findViewById(R.id.eyeimage1);

        OkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cur_pass.getText().toString().isEmpty() || new_pas.getText().toString().isEmpty()) {
                    Snackbar.make(v, "Please fill all fields", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                if (new_pas.getText().toString().length() < 6) {
                    Snackbar.make(v, "Password is not strong", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                changepasswordApi(cur_pass.getText().toString(), new_pas.getText().toString(), v);

            }
        });


        closeLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        eyelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isshow) {
                    eyeimage.setImageResource(R.drawable.ic_visibility_black_24dp);
                    cur_pass.setTransformationMethod(null);
                    isshow = false;
                    cur_pass.setSelection(cur_pass.getText().length());
                } else {
                    eyeimage.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                    cur_pass.setTransformationMethod(new PasswordTransformationMethod());
                    isshow = true;
                    cur_pass.setSelection(cur_pass.getText().length());
                }
            }
        });

        eyelay1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isshow1) {
                    eyeimage1.setImageResource(R.drawable.ic_visibility_black_24dp);
                    new_pas.setTransformationMethod(null);
                    isshow1 = false;
                    new_pas.setSelection(new_pas.getText().length());
                } else {
                    eyeimage1.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                    new_pas.setTransformationMethod(new PasswordTransformationMethod());
                    isshow1 = true;
                    new_pas.setSelection(new_pas.getText().length());
                }
            }
        });


        return shiftFragment;
    }

    private void changepasswordApi(String cpass, String npass, final View v) {

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Requesting...");
        pDialog.setCancelable(false);

        pDialog.show();

        JSONObject json = new JSONObject();
        try {
            json.put("customer_id", prefs.getCustomerId());
            json.put("old_password", cpass);
            json.put("new_password", npass);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest changepass = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.changePassword(), json, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                LogUtil.e("password", "onResponse: " + response.toString());
                pDialog.dismiss();
                try {

                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {

                        Snackbar.make(v, response.getString("message"), Snackbar.LENGTH_SHORT).show();

                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {

                        Snackbar.make(v, response.getString("message"), Snackbar.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();

                            }
                        }, 1000);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(v, "Something went wrong", Snackbar.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(v, message, Snackbar.LENGTH_SHORT).show();
            }
        });

        changepass.setRetryPolicy(new DefaultRetryPolicy(UrlUtils.DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(changepass);
    }

}
