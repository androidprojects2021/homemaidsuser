package com.azinova.homemaidsuser.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.MainActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.models.NotifDatum;
import com.azinova.homemaidsuser.models.NotificationModel;
import com.azinova.homemaidsuser.utils.AnimationUtils;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class PopNotificationFragment extends Fragment {

    private List<NotifDatum> notifData = new ArrayList<>();
    private MynotiitemRecyclerViewAdapter myadapter;
    private Prefs prefs;
    private ProgressBar progress;
    private TextView errortext;
    private LinearLayout errorLay;

    public PopNotificationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notiitem_list, container, false);

        AnimationUtils.registerCircularRevealAnimation(getContext(), view);
        // Set the adapter
        Context context = view.getContext();
        prefs = Prefs.with(context);
        RecyclerView recyclerView = view.findViewById(R.id.list);
        errortext = view.findViewById(R.id.errorText);
        errorLay = view.findViewById(R.id.errorlay);
        progress = view.findViewById(R.id.progressbar);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        myadapter = new MynotiitemRecyclerViewAdapter(notifData, context);
        recyclerView.setAdapter(myadapter);
        fetchNotfications();

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });

        return view;
    }

    private void fetchNotfications() {

        progress.setVisibility(View.VISIBLE);
        errorLay.setVisibility(View.GONE);

        JSONObject jsonObject = new JSONObject();
        try {
            if (prefs.getSigned())
                jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest notfRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.homenotifications()
                , jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress.setVisibility(View.GONE);
                Log.e("noti frag", "onResponse: " + response.toString());
                try {

                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failure")) {
                        errortext.setText(response.getString("message"));
                        errorLay.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new Gson();
                        NotificationModel model = gson.fromJson(response.toString(), NotificationModel.class);
                        notifData.clear();
                        notifData.addAll(model.getNotifData());
                        myadapter.notifyDataSetChanged();
                    } else {
                        errortext.setText("Something went wrong!");
                        errorLay.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    errortext.setText("Something went wrong!");
                    errorLay.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                errortext.setText("Something went wrong!");
                errorLay.setVisibility(View.VISIBLE);
            }
        });
        notfRequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(notfRequest);
    }


}
