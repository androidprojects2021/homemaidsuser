package com.azinova.homemaidsuser.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.BookingHistoryActivity;
import com.azinova.homemaidsuser.R;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Abins Shaji on 13/12/17.
 */

public class RatingFrag extends DialogFragment {
    public static final String TAG = "message";
    Dialog dialog;
    @BindView(R.id.close)
    ImageView close;
    @BindView(R.id.rating)
    RatingBar ratingBar;
    @BindView(R.id.rating_button)
    Button rate;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    Prefs prefs;
    float rate_value;


    public RatingFrag() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (dialog == null) {
            dialog = new Dialog(getActivity(), R.style.MyAlertDialogStyle);
            dialog.setContentView(R.layout.frag_rating);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.getWindow().setGravity(Gravity.CENTER);

        }
        ButterKnife.bind(this, dialog.getWindow().getDecorView());
        progressBar.setVisibility(View.GONE);
        prefs = new Prefs(getActivity());
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getArguments().getString("day_service_id").isEmpty() || !getArguments().getString("booking_id").isEmpty()) {
                    if(rate_value>0)
                    {
                        rateApi();
                    }else {
                        Snackbar.make(view, "Please Rate", Snackbar.LENGTH_SHORT).show();

                    }

                } else {
                    Snackbar.make(view, "Something Went Wrong Please Try again Later", Snackbar.LENGTH_SHORT).show();

                }

            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();

            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Log.e(TAG, "onRatingChanged: " + v);
                rate_value = v;

//                if (v < 1.0f) {
//                    ratingBar.setRating(1.0f);
//                    rate_value = 1.0f;
//                }


            }
        });

        return dialog;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getDialog().setCancelable(true);
        getDialog().setCanceledOnTouchOutside(true);
        super.onViewCreated(view, savedInstanceState);
    }

    private void rateApi() {
        rate.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
            jsonObject.put("day_service_id", getArguments().getString("day_service_id"));
            jsonObject.put("rating", rate_value);
            jsonObject.put("booking_id", getArguments().getString("booking_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "rateApi: " + jsonObject.toString());
        JsonObjectRequest rate_request = new JsonObjectRequest(Request.Method.POST, UrlUtils.rating()
                , jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                EventBus.getDefault().post(new BookingHistoryActivity.RefreshEvent());
                dismiss();
                Log.e(TAG, "onResponse: " + response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismiss();

            }
        });
        rate_request.setRetryPolicy(new DefaultRetryPolicy(UrlUtils.DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(rate_request);


    }
}
