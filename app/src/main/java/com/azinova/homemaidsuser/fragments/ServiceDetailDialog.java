package com.azinova.homemaidsuser.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azinova.homemaidsuser.R;
import com.bumptech.glide.Glide;

/**
 * Created by Abins Shaji on 15/11/17.
 */

public class ServiceDetailDialog extends DialogFragment {
    public static final String TAG = "message";
    private ImageView clickbutton, imageService;
    private TextView title, content;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_service_detail, container, false);
        clickbutton = view.findViewById(R.id.serDetCloseClick);
        imageService = view.findViewById(R.id.dialog_service_image);
        title = view.findViewById(R.id.dialog_service_title);
        content = view.findViewById(R.id.dialog_service_content);
        clickbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        Bundle bundle = getArguments();


        if (bundle != null) {
            Glide.with(this).load(bundle.getString("image")).into(imageService);
            title.setText(bundle.getString("title"));
            content.setText(bundle.getString("content"));

        }

        return view;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setStyle(STYLE_NO_TITLE,R.style.MyAlertDialogStyle);
        setStyle(STYLE_NO_TITLE, R.style.MyAlertDialogStyle);

    }


}
