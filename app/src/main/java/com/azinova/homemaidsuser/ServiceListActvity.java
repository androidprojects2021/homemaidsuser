package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.ServiceListAdapter;
import com.azinova.homemaidsuser.models.ServiceList;
import com.azinova.homemaidsuser.models.ServiceListModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class ServiceListActvity extends AppCompatActivity {

    public static final String TAG = "ServiceListActvity";
    private RecyclerView recyclerView;
    private ServiceListAdapter serviceListAdapter;
    private List<ServiceList> lists = new ArrayList<>();
    private LinearLayout hiserrorLay;
    private LottieAnimationView animationView;
    private TextView hiserrorText;
    private ProgressDialog pDialog;
    private RelativeLayout help_lay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_list_actvity);
        recyclerView = findViewById(R.id.recycler_service);
        ButterKnife.bind(this);
        viewInit();
        setupRecyler();
        initAdapter();
        getServiceListApi();

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(ServiceListActvity.this, HelpActivity.class);
                startActivity(help);
            }
        });
    }

    private void viewInit() {
        animationView = findViewById(R.id.animation_view);
        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
        hiserrorText = findViewById(R.id.hiserrortext);
        hiserrorLay = findViewById(R.id.hiserrorlay);
        help_lay = findViewById(R.id.help_lay);

    }

    @OnClick(R.id.hisretry)
    public void retryClick(View view) {
        getServiceListApi();
    }

    private void initAdapter() {
        if (serviceListAdapter == null) {
            serviceListAdapter = new ServiceListAdapter(this, lists);
            recyclerView.setAdapter(serviceListAdapter);
        }
    }

    public void backClick(View view) {
        onBackPressed();
    }

    private void getServiceListApi() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        hiserrorLay.setVisibility(View.GONE);

        if (animationView.isAnimating())
            animationView.cancelAnimation();

        JsonObjectRequest serviceListjsonObjectRequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.getServiceList(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    Log.e(TAG, "onResponse: " + response.toString());
                    if (response.getString("status").equalsIgnoreCase("success")) {
                        Gson gson = new Gson();
                        ServiceListModel serviceListModel = gson.fromJson(response.toString(), ServiceListModel.class);
                        lists.addAll(serviceListModel.getList());
                        serviceListAdapter.notifyDataSetChanged();
                        recyclerView.scheduleLayoutAnimation();

                    } else {
                        animationView.playAnimation();
                        hiserrorLay.setVisibility(View.VISIBLE);
                        hiserrorText.setText(response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    animationView.playAnimation();
                    hiserrorLay.setVisibility(View.VISIBLE);
                    hiserrorText.setText("Something went wrong!!");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                animationView.playAnimation();
                hiserrorLay.setVisibility(View.VISIBLE);
                hiserrorText.setText(message);
            }
        });

        serviceListjsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(serviceListjsonObjectRequest);

    }

    private void setupRecyler() {
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down); //recyclerview animation
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setLayoutAnimation(controller);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
