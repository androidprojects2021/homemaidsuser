package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.payment.PaymentWebViewActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.PaymentUtils;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import mumbai.dev.sdkdubai.BillingAddress;
import mumbai.dev.sdkdubai.MerchantDetails;
import mumbai.dev.sdkdubai.ShippingAddress;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class MakePaymentActivity extends AppCompatActivity {

    private Prefs prefs;
    private TextView amount_descip;
    private TextView amount_out;
    private TextView dateText;
    private TextView unbilldamt;
    private Button payButton;
    private EditText detail;
    private EditText amount;
    private ProgressDialog pDialog;
    private CustomerModels customerModels;
    private CustomerDetails model;
    private String out_amount;
    private CheckBox checkbox;
    private TextView checktext;
    private ProgressBar progressBar;

    JsonObject jsonInput = new JsonObject();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        prefs = Prefs.with(MakePaymentActivity.this);
        customerModels = prefs.getUserDetails();
        model = customerModels.getCustomerDetails();
        detail = findViewById(R.id.detail);
        amount_descip = findViewById(R.id.amount_descip);
        amount_out = findViewById(R.id.amount_out);
        payButton = findViewById(R.id.paybutton);
        detail = findViewById(R.id.detail);
        amount = findViewById(R.id.amount);
        dateText = findViewById(R.id.date_text);
        unbilldamt = findViewById(R.id.unbillamt);
        checkbox = findViewById(R.id.checkbox);
        checktext = findViewById(R.id.checktext);
        progressBar = findViewById(R.id.progressbar);


        getOutstandingAmount();

        payButton.setOnClickListener(v -> {
            
            /*modification added for making outstanding amount webview*/

            if(amount.getText().toString().isEmpty()){
                Toast.makeText(this,"Please enter the amount",Toast.LENGTH_SHORT).show();
            }else if(detail.getText().toString().isEmpty()){
                Toast.makeText(this,"Please enter the description",Toast.LENGTH_SHORT).show();
            }else{
                Intent webview_payment = new Intent(MakePaymentActivity.this, PaymentWebViewActivity.class);
                webview_payment.putExtra("price",amount.getText().toString());
                webview_payment.putExtra("description",detail.getText().toString());
                webview_payment.putExtra("from","make_payment");

                webview_payment.putExtra("service_date_new", "");
                webview_payment.putExtra("day_service_id","" );

                startActivity(webview_payment);
            }

//            callWebViewMakePayment(url,jsonInput.toString().getBytes());

//            if (amount.getText().toString().isEmpty()
//                    || amount.getText().toString().equalsIgnoreCase("0")
//                    || amount.getText().toString().equalsIgnoreCase("00.00")
//                    || amount.getText().toString().equalsIgnoreCase("0.00")) {
//                Snackbar.make(v, "Not a valid amount", Snackbar.LENGTH_SHORT).show();
//            } else {
//                PaymentEngine(details.getText().toString(), amount.getText().toString());
//            }

        });

        checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                amount.setText(out_amount);
                amount.setEnabled(false);
            } else {
                amount.setEnabled(true);
            }
        });

        checktext.setOnClickListener(v -> checkbox.performClick());
    }

//    private void callWebViewMakePayment(String url, byte[] bytes) {
//        payWebView.postUrl(url,bytes);
//
//    }

    public void backButtonClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void getOutstandingAmount() { //outstanding amount
        progressBar.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            if (prefs.getSigned())
                jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtil.e(jsonObject.toString());
        JsonObjectRequest amountrequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getoutstandingAmount()
                , jsonObject, response -> {
                    Log.e("count", "onResponse: " + response.toString());
                    progressBar.setVisibility(View.GONE);
                    try {

                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {
                            out_amount = response.getString("outstanding_amount");
                            amount_out.setText(out_amount);
                            dateText.setText(response.getString("last_billed_date"));
                            unbilldamt.setText(response.getString("unbilled_amount"));
                            amount.setText(out_amount);
                            checkbox.setChecked(true);

                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();

                    }

                }, error -> progressBar.setVisibility(View.GONE));
        amountrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(amountrequest);
    }

    private void PaymentEngine(String description, final String totalprice) {

        pDialog = new ProgressDialog(MakePaymentActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject paydetails = new JSONObject();
        try {
            paydetails.put("customer_id", prefs.getCustomerId());
            paydetails.put("description", description);
            paydetails.put("price", totalprice);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        LogUtil.e(paydetails.toString());

        JsonObjectRequest adpaymentObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.payoutstandingAmount(), paydetails,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtil.e(response.toString());
                        pDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("error") ||
                                    response.getString("status").equalsIgnoreCase("failure")) {
                                Toast.makeText(MakePaymentActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
                                return;
                            }

                            if (response.getString("status").equalsIgnoreCase("success")) {
                                LogUtil.e(response.toString());

/*
************************************************************ old CC Avenue *************************************************************************
                                String vAccessCode = ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim();
                                String vMerchantId = ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim();
                                String vCurrency = ServiceUtility.chkNull("AED").toString().trim();
                                String vAmount = ServiceUtility.chkNull(totalprice).toString().trim(); //total_price
                                if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
                                    Intent intent = new Intent(MakePaymentActivity.this, WebViewActivity.class);
                                    intent.putExtra(AvenuesParams.ACCESS_CODE, ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim());
                                    intent.putExtra(AvenuesParams.MERCHANT_ID, ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim());
                                    intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(response.getString("order_id")).toString().trim());
                                    intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull("AED").toString().trim());
                                    intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(totalprice).toString().trim()); //total_price
                                    intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                    intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                    intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(PaymentUtils.RSA_URL).toString().trim());

                                    intent.putExtra(AvenuesParams.BILLING_NAME, model.getName());
                                    if (customerModels.getAreaList().size() > 0) {
                                        intent.putExtra(AvenuesParams.BILLING_ADDRESS, customerModels.getAreaList().get(0).getCustomerAddress());
                                        intent.putExtra(AvenuesParams.BILLING_CITY, customerModels.getAreaList().get(0).getAreaName());
                                    }
                                    intent.putExtra(AvenuesParams.BILLING_COUNTRY, "United Arab Emirates");
                                    intent.putExtra(AvenuesParams.BILLING_TEL, model.getPhone());
                                    intent.putExtra(AvenuesParams.BILLING_EMAIL, model.getEmail());

                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                } else {
                                    Toast.makeText(MakePaymentActivity.this, "Something went wrong!", Toast.LENGTH_LONG).show();
************************************************************ old CC Avenue *************************************************************************
*/


                                MerchantDetails m = new MerchantDetails();

                                m.setCurrency("AED");
                                m.setAmount(totalprice);
                                m.setAccess_code(PaymentUtils.AccessCode);
                                m.setMerchant_id(PaymentUtils.MerchantID);
                                m.setRedirect_url(PaymentUtils.RedirectUrl);
                                m.setCancel_url(PaymentUtils.RedirectUrl);
                                m.setRsa_url(PaymentUtils.RSA_URL);
                                m.setOrder_id(response.getString("order_id"));
                                m.setCustomer_id(customerModels.getCustomerDetails().getCustomerId());
                                m.setPromo_code("");
                                m.setAdd1("add1");
                                m.setAdd2("add2");
                                m.setAdd3("add3");
                                m.setAdd4("add4");
                                m.setAdd5("add5");

                                BillingAddress billingAddress = new BillingAddress();

                                billingAddress.setName(customerModels.getCustomerDetails().getName());
                                billingAddress.setAddress(customerModels.getAreaList().get(0).getCustomerAddress());
                                billingAddress.setCountry("Dubai");
                                billingAddress.setState("Dubai");
                                billingAddress.setCity("Dubai");
                                billingAddress.setTelephone(model.getPhone());
                                billingAddress.setEmail(model.getEmail());

                                ShippingAddress s = new ShippingAddress();
                                s.setName(customerModels.getCustomerDetails().getName());
                                s.setAddress(customerModels.getAreaList().get(0).getCustomerAddress());
                                s.setCountry("Dubai");
                                s.setState("Dubai");
                                s.setCity("Dubai");
                                s.setTelephone(model.getPhone());

                                Intent i = new Intent(MakePaymentActivity.this, PaymentActivity.class);

                                i.putExtra("merchant", m);
                                i.putExtra("billing", billingAddress);
                                i.putExtra("shipping", s);
                                startActivity(i);

                            }

                            else {
                                Toast.makeText(MakePaymentActivity.this, "Toast: " + "Something went wrong!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, error -> {
                    pDialog.dismiss();
                    String message = null;
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found.\nPlease try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ParseError) {
                        message = "Something went wrong!\nPlease try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut!\nPlease check your internet connection.";
                    }

                    Toast.makeText(MakePaymentActivity.this, message, Toast.LENGTH_SHORT).show();
                });

        adpaymentObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(adpaymentObjReq);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(finishEvent event) {
        finish();
    }

    public static class finishEvent {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


}
