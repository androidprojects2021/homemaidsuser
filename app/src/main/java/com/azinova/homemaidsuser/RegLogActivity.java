package com.azinova.homemaidsuser;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.azinova.homemaidsuser.fragments.LoginFragment;
import com.azinova.homemaidsuser.fragments.RegisterFragment;
import com.azinova.homemaidsuser.fragments.TermsConditionFragment;

public class RegLogActivity extends AppCompatActivity implements TermsConditionFragment.OnFragmentInteractionListener {

    private static final String TAG = "RegLogActivity";

    private Toolbar toolbar;
    private RegisterFragment registerFragment;
    private LoginFragment loginFragment;
    private TextView registerText;
    private TextView loginText;
    private ImageView mainlogo;
    private AppBarLayout appBarLayout;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_log);

        Bundle extras = getIntent().getExtras();
        boolean isfromlogin = true;
        if (extras != null) {
            isfromlogin = extras.getBoolean("isfromlogin");
        }

        initView();
        setUpToolbar();

        registerFragment = new RegisterFragment();
        registerFragment.setArguments(extras);
        loginFragment = new LoginFragment();
        loginFragment.setArguments(extras);

        if (isfromlogin) {
            getSupportFragmentManager().beginTransaction().add(R.id.layout_fragment, loginFragment).commit();
            loginText.setTypeface(Typeface.DEFAULT_BOLD);
            registerText.setTypeface(Typeface.DEFAULT);
            loginText.setTextColor(Color.parseColor("#ffffff"));
            registerText.setTextColor(Color.parseColor("#81ffffff"));
        } else {
            getSupportFragmentManager().beginTransaction().add(R.id.layout_fragment, registerFragment).commit();
            loginText.setTypeface(Typeface.DEFAULT);
            registerText.setTypeface(Typeface.DEFAULT_BOLD);
            loginText.setTextColor(Color.parseColor("#81ffffff"));
            registerText.setTextColor(Color.parseColor("#ffffff"));
        }

        clickListeners();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN); //for keyboard adjustments

    }

    private void clickListeners() {
        loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentEngine(loginFragment, true);
                loginText.setTypeface(Typeface.DEFAULT_BOLD);
                registerText.setTypeface(Typeface.DEFAULT);
                loginText.setTextColor(Color.parseColor("#ffffff"));
                registerText.setTextColor(Color.parseColor("#81ffffff"));
            }
        });

        registerText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentEngine(registerFragment, false);
                loginText.setTypeface(Typeface.DEFAULT);
                registerText.setTypeface(Typeface.DEFAULT_BOLD);
                loginText.setTextColor(Color.parseColor("#81ffffff"));
                registerText.setTextColor(Color.parseColor("#ffffff"));
            }
        });
    }

    private void fragmentEngine(Fragment fragment, boolean isclicklogin) { //for replacing fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (isclicklogin)
            ft.setCustomAnimations(R.anim.slide_out_right, R.anim.slide_in_right);
        else
            ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);

        ft.replace(R.id.layout_fragment, fragment).commit();
    }

    private void initView() {
        appBarLayout = findViewById(R.id.appbar);
        toolbar = findViewById(R.id.toolbar);
        registerText = findViewById(R.id.register_text);
        loginText = findViewById(R.id.login_text);
        mainlogo = findViewById(R.id.mainlogo);
        linearLayout = findViewById(R.id.linear_layout_main);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mainlogo.setVisibility(View.VISIBLE);
        loginText.setVisibility(View.VISIBLE);
        registerText.setVisibility(View.VISIBLE);
        appBarLayout.setVisibility(View.VISIBLE);
        toolbar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void setUpToolbar() {
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        onBackPressed();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        mainlogo.setVisibility(View.GONE);
        loginText.setVisibility(View.GONE);
        registerText.setVisibility(View.GONE);
        appBarLayout.setVisibility(View.GONE);
        toolbar.setVisibility(View.GONE);
        linearLayout.setVisibility(View.GONE);

    }


    @Override
    public void displayHideItems() {
        mainlogo.setVisibility(View.VISIBLE);
        loginText.setVisibility(View.VISIBLE);
        registerText.setVisibility(View.VISIBLE);
        appBarLayout.setVisibility(View.VISIBLE);
        toolbar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);

    }
}
