package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.fragments.PasswordDialogFragment;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.Shared.offermodel;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class ProfileActivity extends AppCompatActivity {

    public final String TAG = "ProfileActivity";
    @BindView(R.id.profile_image)
    ImageView profile_pic;
    @BindView(R.id.user_name)
    TextView username;
    @BindView((R.id.user_address))
    TextView address;
    @BindView(R.id.user_city)
    TextView city;
    @BindView(R.id.user_email)
    TextView email;
    @BindView(R.id.user_no)
    TextView phn;
    @BindView(R.id.pro_name)
    TextView status_uname;
    @BindView(R.id.hiserrorlay)
    LinearLayout errorlayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.hiserrortext)
    TextView errortext;
    @BindView(R.id.profile_constrain_layout)
    ConstraintLayout profile_layout;
    @BindView(R.id.changepass)
    RelativeLayout changepass;
    @BindView(R.id.help_lay)
    RelativeLayout help_lay;
    @BindView(R.id.user_type)
    TextView user_type;

    private CustomerModels customerModels;
    private Prefs prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);

        prefs = Prefs.with(this);

        initLottie();

        customerModels = prefs.getUserDetails();
        setCustomerData(customerModels);

        if (prefs.getisfacebook())
            changepass.setVisibility(View.GONE);

        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PasswordDialogFragment fragment = PasswordDialogFragment.newInstance();
                fragment.show(getSupportFragmentManager(), "");
            }
        });

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(ProfileActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

    }

    public void initLottie() {
        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
    }

    public void backButtonClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void getProfileApi() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading..");
        dialog.setCancelable(false);
        dialog.show();

        errorlayout.setVisibility(View.GONE);


        if (animationView.isAnimating())
            animationView.cancelAnimation();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, UrlUtils.userDetails(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    dialog.dismiss();

                    if (response.getString("status").equalsIgnoreCase("success")) {
                        profile_layout.setVisibility(View.VISIBLE);
                        Gson gson = new Gson();
                        customerModels = gson.fromJson(response.toString(), CustomerModels.class);
                        Log.e(TAG, "getProfileApi: " + response.toString());
                        setCustomerData(customerModels);
                        prefs.setUserDetails(response.toString());
                    } else {
                        profile_layout.setVisibility(View.INVISIBLE);
                        errorlayout.setVisibility(View.VISIBLE);
                        animationView.playAnimation();
                        errortext.setText("Something went wrong!");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                profile_layout.setVisibility(View.INVISIBLE);
                errorlayout.setVisibility(View.VISIBLE);
                animationView.playAnimation();
                errortext.setText("Something went wrong!");
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(request);

    }

    public void retryClick(View view) {
        getProfileApi();
    }

    private void setCustomerData(CustomerModels customerData) {

        Glide.with(this)
                .applyDefaultRequestOptions(RequestOptions.centerCropTransform())
                .applyDefaultRequestOptions(RequestOptions.circleCropTransform())
                .applyDefaultRequestOptions(RequestOptions.circleCropTransform().placeholder(R.drawable.default_profile))
                .load(customerData.getCustomerDetails().getPhotoUrl()).into(profile_pic);
        username.setText(customerData.getCustomerDetails().getName());
        email.setText(customerData.getCustomerDetails().getEmail());
        phn.setText(customerData.getCustomerDetails().getPhone());
        if (customerData.getAreaList().size() > 0) {
            address.setText(customerData.getAreaList().get(0).getCustomerAddress());
            city.setText(customerData.getAreaList().get(0).getAreaName());
        }

        status_uname.setText(customerData.getCustomerDetails().getName());

        String sel_cus_type = customerData.getCustomerDetails().getCust_type();
        int pos = 0;
        if (sel_cus_type != null || !sel_cus_type.isEmpty()) {
            try {
                pos = Integer.parseInt(sel_cus_type);
            } catch (NumberFormatException nfe) {
                LogUtil.e("Could not parse " + nfe);
            }
        }
        try {
            user_type.setText(offermodel.getTypes().get(pos).getType());
        } catch (Exception e) {
            LogUtil.e(e.toString());
        }


    }

    public void editClickProfile(View view) {
        customerModels = prefs.getUserDetails();
        Intent intent = new Intent(this, EditProfileActivity.class);
        intent.putExtra("user_data", customerModels);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        setCustomerData(event.cusmodels);
    }

    public static class MessageEvent {
        public CustomerModels cusmodels;

        public MessageEvent(CustomerModels models) {
            this.cusmodels = models;
        }

    }


}
