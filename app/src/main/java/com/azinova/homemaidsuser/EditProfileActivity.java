package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.fragments.CutomerTypeFrag;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.models.CustomerTypes;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.Shared.listModel;
import static com.azinova.homemaidsuser.utils.Shared.offermodel;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;


public class EditProfileActivity extends AppCompatActivity {
    public static final String TAG = "EditProfileActivity";
    @BindView(R.id.ep_user_name)
    EditText name;
    @BindView(R.id.ep_user_ph)
    EditText phn;
    @BindView(R.id.ep_user_mail)
    EditText mail;
    @BindView(R.id.ep_user_addr)
    EditText addres;
    @BindView(R.id.spinner_area_id)
    Spinner spinner;
    @BindView(R.id.cus_type_spinner)
    Spinner cus_type_spinner;
    @BindView(R.id.profile_pic_image)
    ImageView imageView;
    @BindView(R.id.edit_image)
    ImageView edit;
    @BindView(R.id.hiserrorlay)
    LinearLayout errorlayout;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.hiserrortext)
    TextView errortext;
    @BindView(R.id.edit_profile_layout)
    ConstraintLayout edit_profile_layout;

    private String[] spinner_string;
    private CustomerModels customerModels;
    private Bitmap bitmap = null;
    private InputStream imageStream;
    private ProgressDialog dialog;
    private String sel_spinner = "null";
    private String sel_cus_type = "0";
    private Prefs prefs;
    private boolean isfromconfirm = false;
    private List<CustomerTypes> custypes;
    private boolean isfirst = true;
    private FragmentActivity fragmentActivity;
    private Bundle bundle;
    private CutomerTypeFrag customertype;

    public EditProfileActivity() {
        custypes = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        prefs = Prefs.with(this);

        dialog = new ProgressDialog(this);


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setActivityTitle("Crop")
                        .setCropMenuCropButtonTitle("Done")
                        .setMultiTouchEnabled(true)
                        .setAspectRatio(1, 1)
                        .start(EditProfileActivity.this);

            }
        });


        Bundle b = getIntent().getExtras();
        if (b != null) {
            customerModels = b.getParcelable("user_data");
            isfromconfirm = b.getBoolean("isfromconfirm");
        }
        edit_profile_layout.setVisibility(View.INVISIBLE);
        initLottie();
        getAreasApi();
        CustypeSpinEngine();
        if(customerModels!=null){
            setData(customerModels);
        }

        if (isfromconfirm) {
            phn.setEnabled(false);
        }

        fragmentActivity = (FragmentActivity) (EditProfileActivity.this);
        bundle = new Bundle();
        customertype = new CutomerTypeFrag();


    }

    private void CustypeSpinEngine() {

        custypes = offermodel.getTypes();
        final List<String> cus_types = new ArrayList<String>();
        for (int i = 0; i < custypes.size(); i++) {
            cus_types.add(custypes.get(i).getType());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, cus_types);
        cus_type_spinner.setAdapter(adapter);

        cus_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                sel_cus_type = custypes.get(i).getTid();
                LogUtil.e("spinner" + sel_cus_type);
                String description = custypes.get(i).getDescription();
                if (!isfirst && description != null && !description.equalsIgnoreCase("") && !description.isEmpty()) {
                    bundle.putString("content", description);
                    bundle.putString("title", custypes.get(i).getType());
                    customertype.setArguments(bundle);
                    customertype.show(fragmentActivity.getFragmentManager(), "");
                }
                isfirst = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void initLottie() {

        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
    }


    public void backButtonClick(View view) {
        onBackPressed();
    }

    public void editClick(View view) {
        validate();
    }

    public void cancelClick(View view) {
        onBackPressed();

    }

    private void validate() {
        if (name.getText().toString().isEmpty()) {
            Snackbar.make(name, "Enter your Name!", Snackbar.LENGTH_SHORT).show();

        } else if (phn.getText().length() < 7) {
            Snackbar.make(phn, "Enter Valid Phone Number!", Snackbar.LENGTH_SHORT).show();

        } else if (false && mail.getText().toString().isEmpty()) {
            Snackbar.make(mail, "Enter your Email!", Snackbar.LENGTH_SHORT).show();

        } else if (addres.getText().toString().isEmpty()) {
            Snackbar.make(addres, "Enter your Address!", Snackbar.LENGTH_SHORT).show();

        } else if (sel_spinner.isEmpty()) {
            Snackbar.make(addres, "Select Area", Snackbar.LENGTH_SHORT).show();

        } else if (bitmap != null) {
            updateProfileApi(name.getText().toString(), phn.getText().toString(), mail.getText().toString(), addres.getText().toString(), encodeTobase64(bitmap));
        } else {
            updateProfileApi(name.getText().toString(), phn.getText().toString(), mail.getText().toString(), addres.getText().toString(), "");
        }

    }

    private void updateProfileApi(String uname, String uphon, String umail, String uaddress, String image) {
        dialog.setCancelable(false);
        dialog.setMessage("Loading...");
        dialog.show();
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
            jsonObject.put("photo", image);
            jsonObject.put("name", uname);
            jsonObject.put("phone", uphon);
            jsonObject.put("customer_address_id", prefs.getUserDetails().getAreaList().get(0).getCustomerAddressId());
            jsonObject.put("area_id", sel_spinner);
            jsonObject.put("address", uaddress);
            jsonObject.put("cust_type", sel_cus_type);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "updateProfileApi: " + jsonObject.toString());
        JsonObjectRequest updateProfileRequest = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.editProfile(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                LogUtil.e(response.toString());

                try {
                    if (response.getString("status").equalsIgnoreCase("success")) {
                        getProfileApi(response.getString("message"));
                    } else {
                        dialog.dismiss();
                        Snackbar.make(name, response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                    Snackbar.make(name, "Something went wrong!.", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(name, message, Toast.LENGTH_SHORT).show();
            }
        });
        updateProfileRequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(updateProfileRequest);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Glide.with(this)
                        .applyDefaultRequestOptions(RequestOptions.centerCropTransform())
                        .applyDefaultRequestOptions(RequestOptions.circleCropTransform())
                        .applyDefaultRequestOptions(RequestOptions.circleCropTransform().placeholder(R.drawable.dummy_profile))
                        .load(result.getUri())
                        .into(imageView);

                try {
                    imageStream = getContentResolver().openInputStream(result.getUri());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                bitmap = BitmapFactory.decodeStream(imageStream);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    private void setData(CustomerModels customerData) {

        if (customerData != null) {
            edit_profile_layout.setVisibility(View.VISIBLE);
            name.setText(customerData.getCustomerDetails().getName());
            MoveCursorToEnd(name);
            phn.setText(customerData.getCustomerDetails().getPhone());
            mail.setText(customerData.getCustomerDetails().getEmail());

            if (customerData.getAreaList().size() > 0) {
                addres.setText(customerData.getAreaList().get(0).getCustomerAddress());
                spinner.setSelection(getIndex(spinner, customerData.getAreaList().get(0).getAreaName()));
                sel_spinner = customerData.getAreaList().get(0).getAreaId();
            }
            sel_cus_type = customerData.getCustomerDetails().getCust_type();
            if (sel_cus_type != null || !sel_cus_type.isEmpty()) {
                int pos = 0;
                try {
                    pos = Integer.parseInt(sel_cus_type);
                } catch (NumberFormatException nfe) {
                    LogUtil.e("Could not parse " + nfe);
                }
                cus_type_spinner.setSelection(pos);

            }

            Glide.with(this)
                    .applyDefaultRequestOptions(RequestOptions.centerCropTransform())
                    .applyDefaultRequestOptions(RequestOptions.circleCropTransform())
                    .applyDefaultRequestOptions(RequestOptions.circleCropTransform().placeholder(R.drawable.default_profile))
                    .load(customerData.getCustomerDetails().getPhotoUrl()).into(imageView);
        } else {
            Snackbar.make(name, "Something Went Wrong", Toast.LENGTH_SHORT).show();
        }

    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                index = i;
                break;
            }

        }
        return index;
    }

    private void getAreasApi() {


        if (listModel.getAreas() == null) {
            return;
        }
        spinner_string = new String[listModel.getAreas().size()];
        for (int i = 0; i < listModel.getAreas().size(); i++) {
            spinner_string[i] = listModel.getAreas().get(i).getAreaName();
        }
        setupSpinner();
    }

    private void setupSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinner_string);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                spinner.setSelection(i);
                sel_spinner = listModel.getAreas().get(i).getAreaId().toString();
                Log.e(TAG, "onItemSelected: " + sel_spinner);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public String encodeTobase64(Bitmap image) {
        Bitmap image_ = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image_.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public void MoveCursorToEnd(TextView textView) {
        Editable etext = textView.getEditableText();
        Selection.setSelection(etext, textView.getText().toString().length());
    }


    public void getProfileApi(final String message) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest userrequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.userDetails(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    dialog.dismiss();

                    if (response.getString("status").equalsIgnoreCase("success")) {
                        Snackbar.make(name, message, Toast.LENGTH_SHORT).show();
                        Gson gson = new Gson();
                        customerModels = gson.fromJson(response.toString(), CustomerModels.class);
                        Log.e(TAG, "getProfileApi: " + response.toString());
                        prefs.setUserDetails(response.toString());
                        prefs.setisverfied(response.getJSONObject("customer_details").getBoolean("isverified"));
                        EventBus.getDefault().post(new MainActivity.LoginUiUpdate(false));
                        if (isfromconfirm) {
                            EventBus.getDefault().post(new ConfirmBookActivity.UiupdateEvent());
                        } else {
                            EventBus.getDefault().post(new ProfileActivity.MessageEvent(customerModels));
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!isfromconfirm) {
                                    if (!prefs.getisverfied()) {
                                        Intent sms = new Intent(EditProfileActivity.this, SmsVerifyActivity.class);
                                        startActivity(sms);
                                    }
                                }
                                finish();
                                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            }
                        }, 1000);
                    } else {

                        Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

            }
        });
        userrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(userrequest);

    }


}
