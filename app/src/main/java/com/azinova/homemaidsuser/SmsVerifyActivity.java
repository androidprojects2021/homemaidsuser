package com.azinova.homemaidsuser;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class SmsVerifyActivity extends AppCompatActivity {
    @BindView(R.id.timer)
    TextView timer;
    @BindView(R.id.retry_text)
    TextView retry_text;
    @BindView(R.id.retry_image)
    ImageView retry_image;
    @BindView(R.id.otp)
    TextView otp;
    @BindView(R.id.backbutton)
    RelativeLayout bacbutton;
    @BindView(R.id.content_input)
    LinearLayout content_input_change_no;
    @BindView(R.id.submit_button)
    Button submit_button;
    @BindView(R.id.bottom_sheet)
    RelativeLayout bottomSheet;
    @BindView(R.id.mobile_no)
    EditText mobileEdit;
    @BindView(R.id.change_no_button)
    RelativeLayout changeButton;
    @BindView(R.id.success_lay)
    RelativeLayout success_lay;
    @BindView(R.id.close_text)
    TextView close_text;
    BottomSheetBehavior bottomSheetBehavior;
    @BindView(R.id.actual_content_relat)
    RelativeLayout actual_content_relat;
    @BindView(R.id.progress_bar_change_no)
    ProgressBar progress_bar_bottom_sheet;
    @BindView(R.id.full_linear)
    LinearLayout full_linear;
    @BindView(R.id.title_text)
    TextView titleText;
    @BindView(R.id.ch_but)
    TextView ch_but;
    @BindView(R.id.mobile)
    TextView mobile;
    boolean isVisible = true;
    private Prefs prefs;
    private ProgressDialog pDialog;
    private CustomerModels customerModels;
    private CountDownTimer timering;
    private boolean isonthego;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms_verify_cord_layout);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isonthego = extras.getBoolean("isonthego", false);
        }

        ButterKnife.bind(this);
        setupBottomSheet();
        full_linear.setVisibility(View.GONE);

        prefs = Prefs.with(SmsVerifyActivity.this);
        customerModels = prefs.getUserDetails();

//        requestPermission();
//        SmsReceiver.bindListener(new SmsListener() {
//            @Override
//            public void messageReceived(String messageText) {
//                Log.e("otp smsverify", "" + messageText);
//                otp.setText(messageText);
//                VerifyOTP(messageText);
//                MoveCursorToEnd(otp);
//
//            }
//        });

        phone = customerModels.getCustomerDetails().getPhone();
        LogUtil.e("phone  1Input: " + phone);
        if (phone == null || phone.isEmpty()) {
            isVisible = false;
            mobileEdit.setText("");
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            full_linear.setVisibility(View.VISIBLE);
            submit_button.setEnabled(false);
            otp.setEnabled(false);
            retry_text.setEnabled(false);
            changeButton.setVisibility(View.VISIBLE);
            success_lay.setVisibility(View.GONE);
            progress_bar_bottom_sheet.setVisibility(View.GONE);
            titleText.setText("Please Enter Your Phone Number");
            ch_but.setText("Verify");
        } else {
            //String result = phone.substring(4);
            mobile.setText(phone);
            //RequestSms("");
            LogUtil.e("phone length: " + phone.length());
            RequestSms(phone);
            timerClock();
        }

        retry_text.setOnClickListener(view -> {
            if (isVisible) {
                isVisible = false;
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                isVisible = true;
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
           // RequestSms("");
            RequestSms(phone);
            timerClock();

        });
        actual_content_relat.setOnClickListener(view -> {
            isVisible = false;
            mobileEdit.setText("");
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        });
        close_text.setOnClickListener(view -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            close_text.setVisibility(View.GONE);

        });

        bacbutton.setOnClickListener(v -> onBackPressed());


        changeButton.setOnClickListener(v -> {
            if (mobileEdit.getText().length() > 7) {
                progress_bar_bottom_sheet.setVisibility(View.VISIBLE);
                changeButton.setVisibility(View.GONE);
                RequestSms(mobileEdit.getText().toString());
            }
        });


    }

    public void timerClock() {
        retry_text.setVisibility(View.INVISIBLE);
        retry_image.setVisibility(View.INVISIBLE);
        timer.setVisibility(View.VISIBLE);
        if (timering != null) {
            timering.cancel();
        }
        timering = new CountDownTimer(60000, 1000) {//60000 - 60 sec , //30000 - 30s

            public void onTick(long millisUntilFinished) {
                timer.setText("00:" + millisUntilFinished / 1000 + " Seconds remaining");
            }

            public void onFinish() {
                timer.setVisibility(View.INVISIBLE);
                retry_text.setVisibility(View.VISIBLE);
                retry_image.setVisibility(View.VISIBLE);
            }
        };
        timering.start();
    }

    public void smsSubmit(View view) {

        if (!otp.getText().toString().isEmpty())
            VerifyOTP(otp.getText().toString());
        else
            Snackbar.make(view, "Not a valid OTP!!", Snackbar.LENGTH_SHORT).show();
    }

    public void MoveCursorToEnd(TextView textView) {
        Editable etext = textView.getEditableText();
        Selection.setSelection(etext, textView.getText().toString().length());
    }

    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(SmsVerifyActivity.this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(SmsVerifyActivity.this,
                    new String[]{Manifest.permission.RECEIVE_SMS},
                    0);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void RequestSms(final String phone) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
            LogUtil.e("phone Input: " + phone);
            if (phone.length() > 0)
                jsonObject.put("phone", phone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtil.e(jsonObject.toString());
        LogUtil.e("SMS Input: " + jsonObject.toString());
        JsonObjectRequest smsreq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.requestSms(), jsonObject, response -> {
                    LogUtil.e("onResponse: " + response.toString());
                    try {

                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failed")) {

                            Snackbar.make(otp, response.getString("message"), Snackbar.LENGTH_SHORT).show();
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {
                            if (phone.length() > 0) {
                                //getuser details here
                                getuserdetailsApi();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Snackbar.make(otp, "Something went wrong", Snackbar.LENGTH_SHORT).show();
                    }

                }, error -> {
            String message = null;
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ServerError) {
                message = "The server could not be found.\nPlease try again after some time!!";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof ParseError) {
                message = "Parsing error!\nPlease try again after some time!!";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...\nPlease check your connection!";
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut!\nPlease check your internet connection.";
            }

            Snackbar.make(otp, message, Snackbar.LENGTH_SHORT).show();
        });

        smsreq.setRetryPolicy(new DefaultRetryPolicy(UrlUtils.DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(smsreq);

    }

    public void VerifyOTP(String msgotp) {

        pDialog = new ProgressDialog(SmsVerifyActivity.this);
        pDialog.setMessage("Verifying OTP...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
            jsonObject.put("sms_otp", msgotp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LogUtil.e(jsonObject.toString());

        JsonObjectRequest veriotp = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.verifyotp(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                LogUtil.e("onResponse: " + response.toString());
                try {

                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {

                        Snackbar.make(otp, response.getString("message"), Snackbar.LENGTH_LONG).show();
                        return;
                    }

                    if (response.getString("status").equalsIgnoreCase("success")) {

                        prefs.setisverfied(true);
                        if (isonthego) {
                            EventBus.getDefault().post(new BookNowActivity.GotoConfirm());
                        }
                        finish();
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(otp, "Something went wrong", Snackbar.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(otp, message, Snackbar.LENGTH_LONG).show();
            }
        });

        veriotp.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(veriotp);

    }

    private void setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    full_linear.setVisibility(View.VISIBLE);

                    submit_button.setEnabled(false);
                    otp.setEnabled(false);
                    retry_text.setEnabled(false);
                    changeButton.setVisibility(View.VISIBLE);
                    success_lay.setVisibility(View.GONE);
                    progress_bar_bottom_sheet.setVisibility(View.GONE);

                    if (phone == null || phone.isEmpty()) {
                        titleText.setText("Please Enter Your Phone Number");
                        ch_but.setText("Verify");
                    } else {
                        titleText.setText("Do you want to change Mobile Number ?");
                        ch_but.setText("Change");
                        close_text.setVisibility(View.VISIBLE);
                    }


                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    close_text.setVisibility(View.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(bottomSheet.getWindowToken(), 0);

                    submit_button.setEnabled(true);
                    otp.setEnabled(true);
                    retry_text.setEnabled(true);


                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    if (phone == null || phone.isEmpty()) {
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        titleText.setText("Please Enter Your Phone Number");
                        ch_but.setText("Verify");
                    } else {
                        titleText.setText("Do you want to change Mobile Number ?");
                        ch_but.setText("Change");
                        close_text.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                full_linear.setAlpha(slideOffset);

            }
        });

    }

    public void getuserdetailsApi() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest userrequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.userDetails(), jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equalsIgnoreCase("success")) {
                        Log.e("smsverify", "getProfileApi: " + response.toString());
                        Gson gson = new Gson();
                        customerModels = gson.fromJson(response.toString(), CustomerModels.class);
                        prefs.setUserDetails(response.toString());
                        prefs.setisverfied(response.getJSONObject("customer_details").getBoolean("isverified"));
                        success_lay.setVisibility(View.VISIBLE);
                        progress_bar_bottom_sheet.setVisibility(View.GONE);
                        EventBus.getDefault().post(new ConfirmBookActivity.UiupdateEvent());
                        EventBus.getDefault().post(new ProfileActivity.MessageEvent(customerModels));

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                isVisible = true;
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                mobile.setText(customerModels.getCustomerDetails().getPhone());
                                timerClock();
                            }
                        }, 1000);

                    } else {

                        Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong!.", Snackbar.LENGTH_SHORT).show();

            }
        });
        userrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(userrequest);

    }


}
