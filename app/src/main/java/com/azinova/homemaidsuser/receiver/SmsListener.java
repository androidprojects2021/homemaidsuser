package com.azinova.homemaidsuser.receiver;

/**
 * Created by Abins Shaji on 22/09/17.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
