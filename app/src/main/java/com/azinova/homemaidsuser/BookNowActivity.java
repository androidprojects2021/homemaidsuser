package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.CustomArrayAdapter;
import com.azinova.homemaidsuser.models.PriceDetailsModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import static com.azinova.homemaidsuser.utils.Shared.bookingdetails;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class BookNowActivity extends AppCompatActivity {

    private RelativeLayout cleanlay, backbutton, proceedButton;
    private RelativeLayout cleanAns;
    private ImageView cleanArrow;
    private boolean isclicked = true;
    private LinearLayout dayLay;
    private AutoCompleteTextView booktypeSpin, sertypeSpin;
    private ScrollView bookScroll;
    private SeekBar seeksBar;
    private TextView seekText;
    private LinearLayout yeslay;
    private ImageView yesimg;
    private LinearLayout nolay;
    private ImageView noimg;
    private ImageView sunimg;
    private ImageView monimg;
    private ImageView tueimg;
    private ImageView wedimg;
    private ImageView thuimg;
    private ImageView satimg;
    private boolean issunclicked = false, ismonclicked = false, istueclicked = false, iswedclicked = false, isthuclicked = false, issatclicked = false;
    private String booktype = "OD", servicetype = "1", cleansing = "N",chemical_cleaning = "N",vaccum_cleaning = "N";
    private Prefs prefs;
    private TextView totalprice;
    private TextView serviceprice;
    private TextView splitprice;
    private TextView cleanprice;
    private TextView discountprice;
    private ProgressBar total_progress;
    private LinearLayout total_lay;
    private ProgressBar service_progress;
    private LinearLayout sc_text;
    private ProgressDialog pDialog;
    private TextView clean_split_text;
    private int dayofweek;
    private RelativeLayout help_lay;
    private RelativeLayout discount_lay;
    private LinearLayout dis_linear;
    private RelativeLayout dis_content;
    private boolean isshowed = false;
    private TextView dis_text;
    private TextView vat_price;
    private TextView discount_label;
    private TextView vat_label;
    private RelativeLayout select_clean;
    private LinearLayout chemicallay;
    private LinearLayout vaccumlay;

    private Boolean isChemicalSelected = false;
    private  Boolean isVaccumSelected = false;
    private  Boolean isMateralSelected = false;

    private EditText edt_notes;

    private  ImageView chemical_img;
    private  ImageView vaccuum_img;


    private RelativeLayout select_matte_lay;

    private PriceDetailsModel priceDetailsModel;
    private JSONArray recur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_now);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dayofweek = extras.getInt("dayofweek", 0);
        }

        prefs = Prefs.with(this);


        initView();
        spinnerEngine();
        clickListeners();

        priceDetailApi();
        fetchDataEngine(false);

    }

    private void spinnerEngine() {
        final List<String> booktypes = new ArrayList<>();
        booktypes.add("One Day");
        booktypes.add("Every Week");


        final CustomArrayAdapter dataAdapter = new CustomArrayAdapter(this,
                R.layout.custom_spinner_item, booktypes);
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        booktypeSpin.setThreshold(100);
        booktypeSpin.setAdapter(dataAdapter);
        booktypeSpin.setText(booktypes.get(0));

        List<String> sertypes = new ArrayList<>();
        sertypes.add("Residential Cleaning");
        sertypes.add("Commercial Cleaning");
        sertypes.add("Parties and After Parties");


        CustomArrayAdapter serdataAdapter = new CustomArrayAdapter(this,
                R.layout.custom_spinner_item, sertypes);
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        sertypeSpin.setThreshold(100);
        sertypeSpin.setAdapter(serdataAdapter);
        sertypeSpin.setText(sertypes.get(0));


        booktypeSpin.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 1) { //ie, weekly booking type //todo change this will affect conditions in confirmbookactivity so beware
                    booktype = "WE";
                    dayLay.setVisibility(View.VISIBLE);
                    initWeekbuttons();
                } else {
                    dayLay.setVisibility(View.GONE);
                    booktype = "OD";
                    //fetchDataEngine(false);
                    recur=null;//todo back to single recursion
                    calculatePrice();
                }

            }
        });

        sertypeSpin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    servicetype = "1"; //todo change this will affect conditions in confirmbookactivity so beware
                } else if (position == 1) {
                    servicetype = "2";
                } else {
                    servicetype = "6";
                }

                //fetchDataEngine(false);

            }
        });
    }

    private void clickListeners() {
        cleanlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isclicked) {
                    cleanAns.setVisibility(View.GONE);
                    isclicked = false;
                    cleanArrow.setRotation(0);
                } else {
                    cleanAns.setVisibility(View.VISIBLE);
                    isclicked = true;
                    cleanArrow.setRotation(180);

                    bookScroll.post(new Runnable() {
                        @Override
                        public void run() {
//                            bookScroll.fullScroll(ScrollView.FOCUS_DOWN);
                            bookScroll.smoothScrollTo(0, bookScroll.getBottom());
                        }
                    });
                }
            }
        });

        booktypeSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (booktypeSpin.isPopupShowing()) {
                    booktypeSpin.dismissDropDown();
                } else {
                    booktypeSpin.showDropDown();
                }
            }
        });

        sertypeSpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sertypeSpin.isPopupShowing()) {
                    sertypeSpin.dismissDropDown();
                } else {
                    sertypeSpin.showDropDown();
                }
            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        proceedButton.setOnClickListener(v -> {

            if (prefs.getSigned()) {

                LogUtil.e("" + prefs.getisverfied());

                if (!prefs.getisverfied()) { //// TODO: 20/12/17 for sms verification
                    Intent sms = new Intent(BookNowActivity.this, SmsVerifyActivity.class);
                    sms.putExtra("isonthego", true);
                    startActivity(sms);
                } else {
                    pDialog = new ProgressDialog(BookNowActivity.this);
                    pDialog.setMessage("Please Wait...");
                    pDialog.setCancelable(false);
                    pDialog.show();
                    fetchDataEngine(true);
                }
            } else {
                Intent reg = new Intent(BookNowActivity.this, RegLogActivity.class);
                reg.putExtra("isfromlogin", true);
                reg.putExtra("isonthego", true);
                startActivity(reg);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

        });

        seeksBar.setMax(10);
        seeksBar.setProgress(1);
        final int minimumValue = 1;
        seeksBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress != 10)
                    seekText.setText("0" + String.valueOf(progress));
                else
                    seekText.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() < minimumValue)
                    seekBar.setProgress(minimumValue);

                //fetchDataEngine(false);
                calculatePrice();
            }
        });

        yeslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesimg.setImageResource(R.drawable.on_tick);
                noimg.setImageResource(R.drawable.off_tick);
                cleansing = "Y";
                chemical_cleaning = "Y";
                isMateralSelected = true;
                isChemicalSelected = true;
                //fetchDataEngine(false);
                clean_split_text.setVisibility(View.VISIBLE);

                select_matte_lay.setVisibility(View.VISIBLE);
                select_clean.setVisibility(View.VISIBLE);

                calculatePrice();

            }
        });


        nolay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yesimg.setImageResource(R.drawable.off_tick);
                noimg.setImageResource(R.drawable.on_tick);
                cleansing = "N";
                isMateralSelected = false;

                //fetchDataEngine(false);
                clean_split_text.setVisibility(View.GONE);

                select_matte_lay.setVisibility(View.GONE);
                select_clean.setVisibility(View.GONE);

                calculatePrice();
            }
        });


        chemicallay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                chemical_cleaning = "Y";
//                vaccum_cleaning = "N";


                if (isChemicalSelected) {
                    chemical_img.setImageResource(R.drawable.off_tick);
                    isChemicalSelected = false;
                } else {
                    chemical_img.setImageResource(R.drawable.on_tick);
                    isChemicalSelected = true;
                }

                if (!isVaccumSelected && !isChemicalSelected){
//                    Toast.makeText(BookNowActivity.this, "Minimum one cleaning material have to selected!!", Toast.LENGTH_SHORT).show();
                    if (isChemicalSelected) {
                        chemical_img.setImageResource(R.drawable.off_tick);
                        isChemicalSelected = false;
                    } else {
                        chemical_img.setImageResource(R.drawable.on_tick);
                        isChemicalSelected = true;
                    }


                }



            }
        });

        vaccumlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                vaccum_cleaning = "Y";
//                chemical_cleaning = "N";



                if (isVaccumSelected) {
                        vaccuum_img.setImageResource(R.drawable.off_tick);
                        isVaccumSelected = false;
                    } else {
                        vaccuum_img.setImageResource(R.drawable.on_tick);
                        isVaccumSelected = true;
                    }
                if (!isVaccumSelected && !isChemicalSelected){
                    if (isVaccumSelected) {
                        vaccuum_img.setImageResource(R.drawable.off_tick);
                        isVaccumSelected = false;
                    } else {
                        vaccuum_img.setImageResource(R.drawable.on_tick);
                        isVaccumSelected = true;
                    }
                }
            }
        });


//        if(isChemicalSelected){
//            vaccuum_img.setImageResource(R.drawable.off_tick);
//            chemical_img.setImageResource(R.drawable.on_tick);
//
//
//        }else if(isVaccumSelected){
//            chemical_img.setImageResource(R.drawable.off_tick);
//            vaccuum_img.setImageResource(R.drawable.on_tick);
//
//        }



        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(BookNowActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

    }

    public void buttonClicked(View view) {
        if (view.getId() == R.id.sunlay) {
            if (issunclicked) {
                sunimg.setImageResource(R.drawable.off_tick);
                issunclicked = false;
            } else {
                sunimg.setImageResource(R.drawable.on_tick);
                issunclicked = true;
            }
            //fetchDataEngine(false);
            calculatePrice();
        } else if (view.getId() == R.id.monlay) {
            if (ismonclicked) {
                monimg.setImageResource(R.drawable.off_tick);
                ismonclicked = false;
            } else {
                monimg.setImageResource(R.drawable.on_tick);
                ismonclicked = true;
            }
            // fetchDataEngine(false);
            calculatePrice();
        } else if (view.getId() == R.id.tuelay) {
            if (istueclicked) {
                tueimg.setImageResource(R.drawable.off_tick);
                istueclicked = false;
            } else {
                tueimg.setImageResource(R.drawable.on_tick);
                istueclicked = true;
            }
            //fetchDataEngine(false);
            calculatePrice();
        } else if (view.getId() == R.id.wedlay) {
            if (iswedclicked) {
                wedimg.setImageResource(R.drawable.off_tick);
                iswedclicked = false;
            } else {
                wedimg.setImageResource(R.drawable.on_tick);
                iswedclicked = true;
            }
            //fetchDataEngine(false);
            calculatePrice();
        } else if (view.getId() == R.id.thulay) {
            if (isthuclicked) {
                thuimg.setImageResource(R.drawable.off_tick);
                isthuclicked = false;
            } else {
                thuimg.setImageResource(R.drawable.on_tick);
                isthuclicked = true;
            }
            //fetchDataEngine(false);
            calculatePrice();
        } else if (view.getId() == R.id.satlay) {
            if (issatclicked) {
                satimg.setImageResource(R.drawable.off_tick);
                issatclicked = false;
            } else {
                satimg.setImageResource(R.drawable.on_tick);
                issatclicked = true;
            }
            //fetchDataEngine(false);
            calculatePrice();
        }
    }

    private void initView() {
        cleanlay = findViewById(R.id.clean_matte_lay);
        cleanAns = findViewById(R.id.view_clean);
        cleanArrow = findViewById(R.id.cm_arrow);
        booktypeSpin = findViewById(R.id.bt_spinner);
        sertypeSpin = findViewById(R.id.st_spinner);
        dayLay = findViewById(R.id.day_lay);
        bookScroll = findViewById(R.id.scroll_view_book);
        backbutton = findViewById(R.id.backbutton);
        proceedButton = findViewById(R.id.proceedButton);
        seeksBar = findViewById(R.id.seekbar);
        seekText = findViewById(R.id.seek_text);
        yeslay = findViewById(R.id.yeslay);
        yesimg = findViewById(R.id.yes_img);
        nolay = findViewById(R.id.nolay);
        noimg = findViewById(R.id.no_img);
        edt_notes = findViewById(R.id.edt_notes);

        chemical_img = findViewById(R.id.chemical_img);
        vaccuum_img = findViewById(R.id.vaccuum_img);

        select_clean = findViewById(R.id.select_clean);

        vaccumlay = findViewById(R.id.vaccuumlay);
        chemicallay = findViewById(R.id.chemicallay);

        select_matte_lay = findViewById(R.id.select_matte_lay);

        sunimg = findViewById(R.id.sunimg);
        monimg = findViewById(R.id.monimg);
        tueimg = findViewById(R.id.tueimg);
        wedimg = findViewById(R.id.wedimg);
        thuimg = findViewById(R.id.thuimg);
        satimg = findViewById(R.id.satimg);

        totalprice = findViewById(R.id.total_price);
        serviceprice = findViewById(R.id.service_price);
        splitprice = findViewById(R.id.split_text);
        cleanprice = findViewById(R.id.clean_price);
        discountprice = findViewById(R.id.offer_price);

        total_progress = findViewById(R.id.total_progress);
        total_lay = findViewById(R.id.total_price_app);

        service_progress = findViewById(R.id.service_progress);
        sc_text = findViewById(R.id.sc_text);

        clean_split_text = findViewById(R.id.clean_split_text);
        help_lay = findViewById(R.id.help_lay);

        discount_lay = findViewById(R.id.discount_lay);

        dis_linear = findViewById(R.id.dis_linear);
        dis_content = findViewById(R.id.dis_content);
        dis_text = findViewById(R.id.dis_text);

        vat_price = findViewById(R.id.vat_price);
        discount_label = findViewById(R.id.discount_label);

        vat_label = findViewById(R.id.vat_label);
    }

    @Override
    public void onBackPressed() {
        if (booktypeSpin.isPopupShowing()) {
            booktypeSpin.dismissDropDown();
            return;
        }
        if (sertypeSpin.isPopupShowing()) {
            sertypeSpin.dismissDropDown();
            return;
        }
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_right);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(finishEvent event) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initWeekbuttons() {
        switch (dayofweek) {
            case 1:
                issunclicked = true;
                ismonclicked = false;
                istueclicked = false;
                iswedclicked = false;
                isthuclicked = false;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.on_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
            case 2:
                issunclicked = false;
                ismonclicked = true;
                istueclicked = false;
                iswedclicked = false;
                isthuclicked = false;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.on_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
            case 3:
                issunclicked = false;
                ismonclicked = false;
                istueclicked = true;
                iswedclicked = false;
                isthuclicked = false;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.on_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
            case 4:
                issunclicked = false;
                ismonclicked = false;
                istueclicked = false;
                iswedclicked = true;
                isthuclicked = false;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.on_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
            case 5:
                issunclicked = false;
                ismonclicked = false;
                istueclicked = false;
                iswedclicked = false;
                isthuclicked = true;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.on_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
            case 7:
                issunclicked = false;
                ismonclicked = false;
                istueclicked = false;
                iswedclicked = false;
                isthuclicked = false;
                issatclicked = true;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.on_tick);
                break;
            default:
                issunclicked = false;
                ismonclicked = false;
                istueclicked = false;
                iswedclicked = false;
                isthuclicked = false;
                issatclicked = false;
                sunimg.setImageResource(R.drawable.off_tick);
                monimg.setImageResource(R.drawable.off_tick);
                tueimg.setImageResource(R.drawable.off_tick);
                wedimg.setImageResource(R.drawable.off_tick);
                thuimg.setImageResource(R.drawable.off_tick);
                satimg.setImageResource(R.drawable.off_tick);
                break;
        }

    }

    private void fetchDataEngine(final boolean isproceed) {
        datafetchingEngine();
        LogUtil.e(bookingdetails.toString());

        if (!isproceed) {
            total_progress.setVisibility(View.VISIBLE);
            service_progress.setVisibility(View.VISIBLE);
            total_lay.setVisibility(View.GONE);
            sc_text.setVisibility(View.GONE);

            vat_price.setVisibility(View.INVISIBLE);
            discountprice.setVisibility(View.INVISIBLE);
        }


        JsonObjectRequest calculusjsonObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.priceCalculator(), bookingdetails,
                response -> {
                    if (pDialog != null)
                        pDialog.dismiss();
                    LogUtil.e(response.toString());
                    try {
                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            discountprice.setText("");
                            splitprice.setText("");
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {
                            LogUtil.e(response.toString());

                            vat_price.setVisibility(View.VISIBLE);
                            discountprice.setVisibility(View.VISIBLE);

                            totalprice.setText(response.getString("total_vat_price"));
                            serviceprice.setText(response.getString("service_total"));
                            splitprice.setText(response.getString("price_splits"));
                            cleanprice.setText(response.getString("cleaning_material_total_price"));

                            vat_price.setText("AED " + response.getString("vat_amount"));
                            vat_label.setText(response.getString("vat"));
                            String offerdis = response.getString("offer_price_perc");
                            if (!offerdis.isEmpty()) {
                                discount_lay.setVisibility(View.VISIBLE);
                                discountprice.setText("AED " + response.getString("discount_price"));
                                discount_label.setText("Discount(" + offerdis + "%)");
                                if (!isshowed) {
                                    discountAnimation(response.getString("discount_detail"));
                                    isshowed = true;
                                }

                            } else {
                                discount_lay.setVisibility(View.INVISIBLE);
                            }

                            String clean = response.getString("cleaning_price_splits");

                            if (clean.isEmpty() || clean.equalsIgnoreCase("")) {
                                clean_split_text.setVisibility(View.GONE);
                            } else {
                                clean_split_text.setVisibility(View.VISIBLE);
                                clean_split_text.setText(response.getString("cleaning_price_splits"));
                            }

                            if(Integer.parseInt(response.getString("total_hours")) * Integer.parseInt(response.getString("no_of_maids")) >=4 ){
                                clean_split_text.setVisibility(View.VISIBLE);
                            } else  {
                                clean_split_text.setVisibility(View.INVISIBLE);
                            }

//                            if(totalHours)

                            if (isproceed) {
                                pDialog.dismiss();
                                Intent cbook = new Intent(BookNowActivity.this, ConfirmBookActivity.class);
                                cbook.putExtra("total_price", response.getString("total_vat_price"));
                                cbook.putExtra("service_total", response.getString("service_total"));
                                cbook.putExtra("price_splits", response.getString("price_splits"));
                                cbook.putExtra("cleaning_material_total_price", response.getString("cleaning_material_total_price"));
                                cbook.putExtra("offer_price_perc", offerdis);
                                cbook.putExtra("notes", edt_notes.getText().toString());
                                cbook.putExtra("cleaning_price_splits", response.getString("cleaning_price_splits"));
                                cbook.putExtra("cleaning_hours", Integer.parseInt(response.getString("total_hours")) * Integer.parseInt(response.getString("no_of_maids")));
                                cbook.putExtra("vat", response.getString("vat"));
                                cbook.putExtra("vat_amount", response.getString("vat_amount"));
                                cbook.putExtra("dis_type", response.getString("discount_type"));
                                cbook.putExtra("dis_amount", response.getString("discount_price"));
                                startActivity(cbook);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                            } else {
                                total_progress.setVisibility(View.GONE);
                                service_progress.setVisibility(View.GONE);
                                total_lay.setVisibility(View.VISIBLE);
                                sc_text.setVisibility(View.VISIBLE);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        discountprice.setText("");
                        splitprice.setText("");
                    }
                }, error -> {
                    discountprice.setText("");
                    splitprice.setText("");
                    if (pDialog != null)
                        pDialog.dismiss();
                    String message = null;
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found.\nPlease try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ParseError) {
                        message = "Something went wrong!\nPlease try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut!\nPlease check your internet connection.";
                    }

                    Snackbar.make(proceedButton, message, Snackbar.LENGTH_LONG).show();
                });

        calculusjsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(calculusjsonObjReq);

    }

    public void datafetchingEngine() {
        proceedButton.setEnabled(true);
        try {
            bookingdetails.put("maid_no", seekText.getText().toString());
            bookingdetails.put("service_type", servicetype);
            bookingdetails.put("cleaning_materials", cleansing);

            bookingdetails.put("notes",edt_notes.getText().toString());
            if (isMateralSelected){



                    if (isChemicalSelected){
                        bookingdetails.put("chemical_cleaning","Y");
                    }else {
                        bookingdetails.put("chemical_cleaning","N");
                    }

                    if(isVaccumSelected){
                        bookingdetails.put("vaccum_cleaning","Y");
                    }else{
                        bookingdetails.put("vaccum_cleaning","N");
                    }

            }else {

                bookingdetails.put("chemical_cleaning","N");
                bookingdetails.put("vaccum_cleaning","N");

            }



            bookingdetails.put("book_type", booktype);
            if (booktype.equalsIgnoreCase("WE")) {

                recur = new JSONArray();
                if (issunclicked)
                    recur.put("0");
                if (ismonclicked)
                    recur.put("1");
                if (istueclicked)
                    recur.put("2");
                if (iswedclicked)
                    recur.put("3");
                if (isthuclicked)
                    recur.put("4");
                if (issatclicked)
                    recur.put("6");

                if (recur.length() == 0) {
                    Snackbar.make(proceedButton, "Please select the days", Snackbar.LENGTH_SHORT).show();
                    proceedButton.setEnabled(false);
                    return;
                }
                bookingdetails.put("service_week", recur);
            } else {
                bookingdetails.remove("service_week");
            }
            if (prefs.getSigned()) {
                bookingdetails.put("customer_id", prefs.getCustomerId());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(GotoConfirm event) {
        proceedButton.performClick();
    }

    public static class finishEvent {

    }

    public static class GotoConfirm {

    }

    private void discountAnimation(String text) {
        dis_text.setText(text);
        final Transition slide_left = new Slide(Gravity.TOP);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TransitionManager.beginDelayedTransition(dis_linear, slide_left);
                dis_content.setVisibility(View.VISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        TransitionManager.beginDelayedTransition(dis_linear, slide_left);
                        dis_content.setVisibility(View.INVISIBLE);

                    }
                }, 3000);

            }
        }, 800);


    }


    private void priceDetailApi() {
        JSONObject object = new JSONObject();
        try {
            object.put("customer_id", prefs.getCustomerId() != null ? prefs.getCustomerId() : "");
            object.put("from_time", bookingdetails.getString("from_time"));
            object.put("to_time", bookingdetails.getString("to_time"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        LogUtil.e(object.toString());

        JsonObjectRequest priceDetailRequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.priceDetailApi(), object,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            LogUtil.e(response.toString());
                            if (response.getString("status").equalsIgnoreCase("success")) {

                                priceDetailsModel = new Gson().fromJson(response.toString(), PriceDetailsModel.class);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(proceedButton, message, Snackbar.LENGTH_LONG).show();


            }
        });

        priceDetailRequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(priceDetailRequest);
    }

    private void calculatePrice() {
        datafetchingEngine();

        int recursion ;
        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);

        int no_maids = Integer.parseInt(seekText.getText().toString());
        if (recur != null) {
            recursion = recur.length() > 0 ? recur.length() : 1;
        } else {
            recursion = 1;
        }

        LogUtil.e(""+recursion );

        if (priceDetailsModel!=null){
            int no_hours = priceDetailsModel.getRateData().getNo_hour();
            Double service_rate = priceDetailsModel.getRateData().getService_rate();
            Double material_rate = priceDetailsModel.getRateData().getMaterial_rate();
            Double vat_perc = priceDetailsModel.getRateData().getVat_perc();
            Double discount_perc = priceDetailsModel.getRateData().getDiscount_perc();

            int totalHours =  no_hours * no_maids;

            Double calc_material ;
            Double total_calc_material ;
            String str_no_maid = no_maids > 1 ? no_maids + " maids" : no_maids + " maid";
            String str_no_days = recursion > 1 ? recursion + " days" : recursion + " day";

            if(totalHours  >= 4){
                LogUtil.e(totalHours +" Hours");
                calc_material = cleansing.equals("Y") ? material_rate : 0.00;
                total_calc_material = calc_material * recursion * no_hours * no_maids;
                clean_split_text.setVisibility(View.VISIBLE);
            } else  {
                calc_material = cleansing.equals("Y") ? 20.0 : 0.00;
                total_calc_material = calc_material * recursion  * no_maids;
                clean_split_text.setVisibility(View.INVISIBLE);
            }

            splitprice.setText("" + no_hours + " Hours(" + service_rate + " / hr) x " + str_no_maid + " x " + str_no_days);
            clean_split_text.setText("" + no_hours + " Hours( " + material_rate + " / hr ) x " + str_no_maid + " x " + str_no_days);

//            calc_material = cleansing.equals("Y") ? material_rate : 0.00;

            Double calc_service_cost = no_hours * service_rate * no_maids * recursion;

            Double calc_discount = (calc_service_cost + total_calc_material) * discount_perc / 100;
            Double calc_vat = ((calc_service_cost + total_calc_material) - calc_discount) * vat_perc / 100;

            Double calc_total = (calc_service_cost + total_calc_material + calc_vat) - calc_discount;

            serviceprice.setText(formatter.format(calc_service_cost).toString());
            discount_label.setText("Discount(" + discount_perc + "%)");
            discountprice.setText("AED " + formatter.format(calc_discount).toString());
            vat_label.setText("VAT " + priceDetailsModel.getRateData().getVat_perc() + "%");
            vat_price.setText("AED " + formatter.format(calc_vat).toString());
            totalprice.setText(formatter.format(calc_total).toString());
            cleanprice.setText(formatter.format(total_calc_material).toString());

        }

    }

}
