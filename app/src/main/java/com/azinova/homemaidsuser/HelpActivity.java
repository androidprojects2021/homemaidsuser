package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.adapters.Help_adapter;
import com.azinova.homemaidsuser.models.HelpDatum;
import com.azinova.homemaidsuser.models.HelpModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class HelpActivity extends AppCompatActivity {

    public static final String TAG = "message";
    List<HelpDatum> modelList = new ArrayList<>();
    @BindView(R.id.recycler_popular_help)
    RecyclerView recyclerView;
    @BindView(R.id.faq_relativelayout)
    RelativeLayout faq_relativelayout;
    @BindView(R.id.feedback_relativelayout)
    RelativeLayout feedback_rel;
    @BindView(R.id.feedback_content)
    RelativeLayout feedback_content;
    @BindView(R.id.image_popular)
    ImageView image_popularl;
    @BindView(R.id.image_feedback)
    ImageView image_feedback;
    @BindView(R.id.faq_parent_rel)
    RelativeLayout faq_parent_rel;
    @BindView(R.id.progress_rel)
    RelativeLayout progress_rel;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.feedback_main_rel)
    RelativeLayout feedback_main_rel;
    @BindView(R.id.conten_linear)
    LinearLayout conten_linear;
    @BindView(R.id.help_feedback_mail)
    TextInputEditText help_feedback_mail;
    @BindView(R.id.feedback_user_msg)
    TextInputEditText feedback_user_msg;
    Help_adapter adapter;
    Transition transition;
    Boolean get = false;
    Prefs prefs;
    private TextView hiserrorText;
    private LinearLayout hiserrorLay;
    private Button hisRetry;
    private LottieAnimationView animationView;
    private ProgressDialog pDialog;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        ButterKnife.bind(this);
        prefs = Prefs.with(this);
        initAdapter();
        initView();
        getHelpData();

        if (prefs.getSigned()) {
            if (!prefs.getUserDetails().getCustomerDetails().getEmail().isEmpty()) {
                help_feedback_mail.setText(prefs.getUserDetails().getCustomerDetails().getEmail());
                MoveCursorToEnd(help_feedback_mail);
            }
        }


        //transition
        transition = new AutoTransition();
        transition.setDuration(300);
        transition.setInterpolator(new FastOutSlowInInterpolator());
        transition.setStartDelay(200);

        recyclerView.setActivated(true);
        feedback_content.setActivated(false);
        faq_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!recyclerView.isActivated()) {
                    if (get) {
                        image_popularl.setRotation(0);
                        //TransitionManager.beginDelayedTransition(faq_parent_rel);
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.setActivated(true);
                    } else {
                        recyclerView.setActivated(true);
                        progressBar.setVisibility(View.VISIBLE);
                        //TransitionManager.beginDelayedTransition(faq_parent_rel);
                        progress_rel.setVisibility(View.VISIBLE);
                    }

                } else {
                    image_popularl.setRotation(-90);
                    progressBar.setVisibility(View.GONE);
                    progress_rel.setVisibility(View.GONE);
                    //TransitionManager.beginDelayedTransition(faq_parent_rel);
                    recyclerView.setVisibility(View.GONE);
                    recyclerView.setActivated(false);
                }

            }
        });
        feedback_rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!feedback_content.isActivated()) {
                    image_feedback.setRotation(0);
                    //TransitionManager.beginDelayedTransition(feedback_main_rel);
                    feedback_content.setVisibility(View.VISIBLE);
                    feedback_content.setActivated(true);
                } else {
                    image_feedback.setRotation(-90);
                    //TransitionManager.beginDelayedTransition(feedback_main_rel);
                    feedback_content.setVisibility(View.GONE);
                    feedback_content.setActivated(false);
                }

            }
        });

        animationView.setAnimation("error_animation.json");
        animationView.loop(true);
        hisRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHelpData();
            }
        });

    }

    private void initView() {
        animationView = findViewById(R.id.animation_view);
        hiserrorText = findViewById(R.id.hiserrortext);
        hiserrorLay = findViewById(R.id.hiserrorlay);
        hisRetry = findViewById(R.id.hisretry);

    }

    public void backClick(View view) {
        onBackPressed();
    }

    public void sendFeedback(View view) {
        if (!isValidEmail(help_feedback_mail.getText().toString())) {
            Snackbar.make(view, "Enter a Valid Email Address", Snackbar.LENGTH_SHORT).show();
        } else if (feedback_user_msg.getText().toString().length() < 20) {
            Snackbar.make(view, "Minimum Message Length is 20 characters", Snackbar.LENGTH_SHORT).show();
        } else {
            //TODO:feedback api call
            feedbackApi(view);

        }


    }

    public void initAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (adapter == null) {
            adapter = new Help_adapter(this, modelList);
            recyclerView.setAdapter(adapter);

        }

    }

    public void getHelpData() {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        conten_linear.setVisibility(View.GONE);
        hiserrorLay.setVisibility(View.GONE);

        if (animationView.isAnimating())
            animationView.cancelAnimation();

        JsonObjectRequest help_req = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.helplist(),
                null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();

                try {
                    if (response.getString("status").equalsIgnoreCase("success")) {
                        conten_linear.setVisibility(View.VISIBLE);
                        if (response != null)
                            get = true;
                        Log.e(TAG, "onResponse: " + response.toString());
                        HelpModel helpModel = new Gson().fromJson(response.toString(), HelpModel.class);
                        modelList.addAll(helpModel.getHelpData());
                        adapter.notifyDataSetChanged();


                    } else {
                        pDialog.dismiss();
                        conten_linear.setVisibility(View.GONE);
                        hiserrorLay.setVisibility(View.VISIBLE);
                        hiserrorText.setText(response.getString("message"));
                        animationView.playAnimation();
                        return;

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LogUtil.e(TAG, "Error: " + error.getMessage());
                pDialog.dismiss();

                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                conten_linear.setVisibility(View.GONE);
                hiserrorLay.setVisibility(View.VISIBLE);
                hiserrorText.setText(message);
                animationView.playAnimation();


            }
        });

        help_req.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(help_req);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void MoveCursorToEnd(TextView textView) {
        Editable etext = textView.getEditableText();
        Selection.setSelection(etext, textView.getText().toString().length());
    }

    private void feedbackApi(final View view) {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Sending...");
        pDialog.setCancelable(false);
        pDialog.show();
        JSONObject object = new JSONObject();
        try {
            object.put("email", help_feedback_mail);
            object.put("message", feedback_user_msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest feedbackReq = new JsonObjectRequest(Request.Method.POST, UrlUtils.help()
                , object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                Log.e(TAG, "onResponse: " + response.toString());
                try {
                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {
                        Snackbar.make(view, response.getString("message"), Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if (response.getString("status").equalsIgnoreCase("success")) {

                        Snackbar snack = Snackbar.make(view, response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        closeKeyboard();
                                        //onBackPressed();
                                        feedback_user_msg.setText("");
                                        image_feedback.setRotation(-90);
                                        //TransitionManager.beginDelayedTransition(feedback_main_rel);
                                        feedback_content.setVisibility(View.GONE);
                                        feedback_content.setActivated(false);
                                    }
                                });
                        snack.show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
            }


        });
        feedbackReq.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(feedbackReq);

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

    }
}
