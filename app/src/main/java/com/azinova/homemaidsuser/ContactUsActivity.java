package com.azinova.homemaidsuser;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class ContactUsActivity extends AppCompatActivity {

    public static final String TAG = "message";
    @BindView(R.id.cu_user_msg)
    EditText msg;
    @BindView(R.id.cu_user_name)
    TextInputEditText name;
    @BindView(R.id.cu_user_email)
    TextInputEditText email;
    @BindView(R.id.cu_user_no)
    TextInputEditText number;
    @BindView(R.id.help_lay)
    RelativeLayout help_lay;

    @BindView(R.id.cu_hm_booking_no_one)
    TextView booking_no_one;
    @BindView(R.id.cu_hm_booking_no_two)
    TextView booking_no_two;
    @BindView(R.id.cu_hm_office_no)
    TextView office_no;
    @BindView(R.id.cu_hm_booking_mail)
    TextView booking_mail;
    @BindView(R.id.azinova_click)
    ImageView azinova_click;
    @BindView(R.id.cu_hm_website)
    TextView cu_hm_website;
    @BindView(R.id.hm_fb)
    ImageView facebook;
    @BindView(R.id.hm_twitter)
    ImageView twitter;
    @BindView(R.id.hm_linkedin)
    ImageView linkedin;
    @BindView(R.id.hm_gplus)
    ImageView google;
    Prefs prefs;


    @BindView(R.id.cu_hm_address)
    TextView hm_address;
//    @BindView(R.id.cu_hm_booking_no_one)
//    TextView cu_hm_booking_no_one;
//    @BindView(R.id.cu_hm_booking_no_two)
//    TextView cu_hm_booking_no_two;
//    @BindView(R.id.cu_hm_office_no)
//    TextView cu_hm_office_no;
    @BindView(R.id.cu_hm_fax)
    TextView cu_hm_fax;
//    @BindView(R.id.cu_hm_booking_mail)
//    TextView cu_hm_booking_mail;

    ProgressDialog progressDialog;

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        prefs = Prefs.with(this);
        //msg.requestFocus();
        //closeKeyboard();
        // MoveCursorToEnd(name);


        //name.requestFocus();
        //MoveCursorToEnd(name);
        //closeKeyboard();

        setupAddress();

        clicks();


        if (prefs.getSigned()) {
            name.setText(prefs.getUserDetails().getCustomerDetails().getName());
            email.setText(prefs.getUserDetails().getCustomerDetails().getEmail());
            number.setText(prefs.getUserDetails().getCustomerDetails().getPhone());
            msg.requestFocus();
        } else {
            name.requestFocus();
        }


        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(ContactUsActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });
    }

    private void setupAddress() {

        if (prefs.getEmiratesIs_Dubai()) {
            hm_address.setText(getResources().getString(R.string.contact_us_hm_addr));
            booking_no_one.setText(" 800 6243 ");
            booking_no_two.setText("/  +971 50 256 1525");
            office_no.setText("+971 4 339 7757");
            cu_hm_fax.setText("+971 4 339 7234");
            booking_mail.setText("booking@homemaids.ae");
            cu_hm_website.setText("www.homemaids.ae");

        }else {
            hm_address.setText(getResources().getString(R.string.contact_us_hm_addr_abu));
            booking_no_one.setText(" 024452352 ");
            booking_no_two.setText("");
            office_no.setText(" - ");
            cu_hm_fax.setText(" - ");
            booking_mail.setText("bookings@homemaids.ae");
            cu_hm_website.setText("www.homemaids.ae");
        }

    }

    private void clicks() {
        azinova_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.azinovatechnologies.com/"));
                startActivity(intent);


            }
        });
        booking_no_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:8006243"));
                startActivity(intent);


            }
        });
        booking_no_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+971 50-2561525"));
                startActivity(intent);


            }
        });
        office_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+971 4-3397757"));
                startActivity(intent);


            }
        });
        booking_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("mailto:booking@homemaids.ae"));
                startActivity(intent);


            }
        });
        cu_hm_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.homemaids.ae/"));
                startActivity(intent);


            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hm_intent = new Intent(Intent.ACTION_VIEW);
                hm_intent.setData(Uri.parse("https://www.facebook.com/homemaidsdubai"));
                startActivity(hm_intent);
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hm_intent = new Intent(Intent.ACTION_VIEW);
                hm_intent.setData(Uri.parse("https://twitter.com/homemaidsdubai"));
                startActivity(hm_intent);
            }
        });
        linkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hm_intent = new Intent(Intent.ACTION_VIEW);
                hm_intent.setData(Uri.parse("https://www.linkedin.com/company/homemaids-llc"));
                startActivity(hm_intent);
            }
        });
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hm_intent = new Intent(Intent.ACTION_VIEW);
                hm_intent.setData(Uri.parse("https://plus.google.com/+HomemaidsAe-dubai"));
                startActivity(hm_intent);
            }
        });

    }

    private void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

    }

    public void MoveCursorToEnd(TextView textView) {
        Editable etext = textView.getEditableText();
        Selection.setSelection(etext, textView.getText().toString().length());
    }

    public void backClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void proceedClick(View view) {
        validate(view);


    }

    private void proceedApi(final View view) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Sending Feedback..");
        progressDialog.setCancelable(false);
        progressDialog.show();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name.getText().toString());
            jsonObject.put("mobile", email.getText().toString());
            jsonObject.put("email", number.getText().toString());
            jsonObject.put("message", msg.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest feedbackReq = new JsonObjectRequest(Request.Method.POST, UrlUtils.contactus()
                , jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                Log.e(TAG, "onResponse: " + response.toString());
                try {
                    if (response.getString("status").equalsIgnoreCase("error") ||
                            response.getString("status").equalsIgnoreCase("failed")) {
                        Snackbar.make(view, response.getString("message"), Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if (response.getString("status").equalsIgnoreCase("success")) {

                        Snackbar snack = Snackbar.make(view, response.getString("message"), Snackbar.LENGTH_INDEFINITE)
                                .setAction("OK", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        closeKeyboard();
                                        onBackPressed();
                                    }
                                });
                        snack.show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
            }


        });
        feedbackReq.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(feedbackReq);


    }

    private void validate(View view) {
        if (name.getText().toString().isEmpty()) {
            Snackbar.make(name, "Enter your Name!", Snackbar.LENGTH_SHORT).show();

        } else if (number.getText().length() < 7) {
            Snackbar.make(number, "Enter Valid Phone Number!", Snackbar.LENGTH_SHORT).show();

        } else if (!isValidEmail(email.getText().toString())) {
            Snackbar.make(email, "Enter Valid Email!", Snackbar.LENGTH_SHORT).show();

        } else if (msg.getText().toString().isEmpty()) {
            Snackbar.make(msg, "Enter your Message!", Snackbar.LENGTH_SHORT).show();

        } else {
            proceedApi(view);
        }

    }
}

