package com.azinova.homemaidsuser;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.models.AreaListModel;
import com.azinova.homemaidsuser.models.OfferModel;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.azinova.homemaidsuser.utils.Shared.listModel;
import static com.azinova.homemaidsuser.utils.Shared.offerdetails;
import static com.azinova.homemaidsuser.utils.Shared.offermodel;
import static com.azinova.homemaidsuser.utils.Shared.offertitle;
import static com.azinova.homemaidsuser.utils.Shared.times;
import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class SplashActivity extends AppCompatActivity {

    private final String TAG = "SplashActivity";
    // Splash screen timer
    private ImageView mainLogo;
    private Prefs prefs;
    private ProgressBar progress;
    private LinearLayout errorlay;
    private TextView errortext;
    private ImageView errorButton;
    private Calendar cal;
    private int mYear, mMonth, mDay;
    private SimpleDateFormat format1 = null;
    private boolean isfromnoti;

    private LinearLayout linearEmirats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isfromnoti = extras.getBoolean("isnotification", false);
        }

        prefs = Prefs.with(this);

        mainLogo = findViewById(R.id.mainlogo);
        progress = findViewById(R.id.progress);
        errorlay = findViewById(R.id.errorlay);
        errortext = findViewById(R.id.errorText);
        errorButton = findViewById(R.id.errorButton);
        linearEmirats = findViewById(R.id.linear_emirates);


        Glide.with(this).load(R.drawable.splash_anim).into(new DrawableImageViewTarget(mainLogo) {
            @Override
            public void onResourceReady(Drawable resource, @Nullable Transition<? super Drawable> transition) {
                if (resource instanceof GifDrawable) {
                    ((GifDrawable) resource).setLoopCount(1);
                }
                super.onResourceReady(resource, transition);
            }
        });

        errorButton.setOnClickListener(v -> fetchAreaDataEngine());

        // NEW CHANGES TO REMOVE ABUDhABI  @LEO

        prefs.setEmiratesIs_Dubai(true);
        prefs.setIsEmiratesSelected(true);
        UrlUtils.setBaseUrl(true);

        emiratesCheck();

    }

    private void emiratesCheck() {
        if (prefs.getIsEmiratesSelected()) {
            Log.e(TAG, "delay: ");
            linearEmirats.setVisibility(View.GONE);
            progress.setVisibility(View.VISIBLE);
            UrlUtils.setBaseUrl(prefs.getEmiratesIs_Dubai());
            delay();

        } else {
            linearEmirats.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);

        }
    }

    private void delay() {

        new Handler().postDelayed(this::fetchAreaDataEngine, 3000);

    }


    private void fetchAreaDataEngine() {
        progress.setVisibility(View.VISIBLE);
        errorlay.setVisibility(View.GONE);

        JsonObjectRequest areajsonObjReq = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.getAreaList(), null,
                response -> {
                    try {
                        LogUtil.e(TAG, "onResponse: " + response.toString());
                        if (response.getString("status").equalsIgnoreCase("error") ||
                                response.getString("status").equalsIgnoreCase("failure")) {
                            return;
                        }

                        if (response.getString("status").equalsIgnoreCase("success")) {

                            Gson gson = new Gson();
                            listModel = gson.fromJson(response.toString(), AreaListModel.class);

                            getTime();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progress.setVisibility(View.GONE);
                        errorlay.setVisibility(View.VISIBLE);
                        errortext.setText("Something went wrong!");
                    }
                }, error -> {
                    progress.setVisibility(View.GONE);
                    String message = null;
                    if (error instanceof NetworkError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ServerError) {
                        message = "The server could not be found.\nPlease try again after some time!!";
                    } else if (error instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof ParseError) {
                        message = "Something went wrong!\nPlease try again after some time!!";
                    } else if (error instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...\nPlease check your connection!";
                    } else if (error instanceof TimeoutError) {
                        message = "Connection TimeOut!\nPlease check your internet connection.";
                    }

                    errorlay.setVisibility(View.VISIBLE);
                    errortext.setText(message);

                });

        areajsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(areajsonObjReq);

    }

    private void fetchOfferDataEngine() {


        JsonObjectRequest offerjsonObjReq = new JsonObjectRequest(Request.Method.GET,
                UrlUtils.getOffer(), null,
                response -> {
                    try {
                        LogUtil.e(TAG, "onResponse: " + response.toString());
                        if (response.getString("status").equalsIgnoreCase("success")) {

                            offertitle = response.getString("title");
                            offerdetails = response.getString("details");

                            Gson gson = new Gson();
                            offermodel = gson.fromJson(response.toString(), OfferModel.class);

                        }
                        flownavigator(); //// TODO: 13/12/17 go to next activity

                    } catch (JSONException e) {
                        e.printStackTrace();
                        flownavigator();
                    }
                }, error -> flownavigator());

        offerjsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(offerjsonObjReq);

    }

    public void getUserDetail() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("customer_id", prefs.getCustomerId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest userrequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.userDetails(), jsonObject, response -> {
            try {
                LogUtil.e(TAG, "onResponse: " + response.toString());
                if (response.getString("status").equalsIgnoreCase("success")) {
                    prefs.setUserDetails(response.toString());
                    prefs.setisverfied(response.getJSONObject("customer_details").getBoolean("isverified"));
                    fetchOfferDataEngine();
                } else {
                    progress.setVisibility(View.GONE);
                    errorlay.setVisibility(View.VISIBLE);
                    errortext.setText("Something went wrong!");

                }
            } catch (JSONException e) {
                e.printStackTrace();
                progress.setVisibility(View.GONE);
                errorlay.setVisibility(View.VISIBLE);
                errortext.setText("Something went wrong!");
            }

        }, error -> {
            progress.setVisibility(View.GONE);
            errorlay.setVisibility(View.VISIBLE);
            errortext.setText("Something went wrong!");
        });
        userrequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(userrequest);

    }


    public void getTime() {


        JsonObjectRequest timerequest = new JsonObjectRequest(Request.Method.GET, UrlUtils.getTime(initCalendar()), null, response -> {
            try {
                LogUtil.e(TAG, "onResponse: " + response.toString());
                if (response.getString("status").equalsIgnoreCase("success")) {

                    times = null;
                    times = response.getJSONArray("times");

                    if (prefs.getSigned()) {
                        getUserDetail();
                    } else {
                        fetchOfferDataEngine();
                    }
                } else {
                    progress.setVisibility(View.GONE);
                    errorlay.setVisibility(View.VISIBLE);
                    errortext.setText("Something went wrong!");

                }
            } catch (JSONException e) {
                e.printStackTrace();
                progress.setVisibility(View.GONE);
                errorlay.setVisibility(View.VISIBLE);
                errortext.setText("Something went wrong!");
            }

        }, error -> {
            progress.setVisibility(View.GONE);
            errorlay.setVisibility(View.VISIBLE);
            errortext.setText("Something went wrong!");
        });
        timerequest.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(timerequest);

    }

    private String initCalendar() {
        //get date
        cal = Calendar.getInstance();
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            cal.add(Calendar.DATE, 2);
        } else {
            cal.add(Calendar.DATE, 1);
        }
        mYear = cal.get(Calendar.YEAR);
        mMonth = cal.get(Calendar.MONTH);
        mDay = cal.get(Calendar.DAY_OF_MONTH);

        format1 = new SimpleDateFormat("dd/MM/yyyy");
        return format1.format(cal.getTime());
    }

    private void flownavigator() {
        if (isfromnoti) {
            Intent i = new Intent(SplashActivity.this, NotificationActivity.class);
            i.putExtra("isfromsplash", true);
            startActivity(i);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        } else {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }
    }

    public void bclickDubai(View view) {
        prefs.setEmiratesIs_Dubai(true);
        prefs.setIsEmiratesSelected(true);
        UrlUtils.setBaseUrl(true);
        emiratesCheck();

    }

    public void bclickAbuDhabi(View view) {
        prefs.setEmiratesIs_Dubai(false);
        prefs.setIsEmiratesSelected(true);
        UrlUtils.setBaseUrl(false);
        emiratesCheck();

    }
}
