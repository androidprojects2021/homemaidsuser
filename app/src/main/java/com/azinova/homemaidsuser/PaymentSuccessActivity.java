package com.azinova.homemaidsuser;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.PaymentUtility.AvenuesParams;
import com.azinova.homemaidsuser.PaymentUtility.ServiceUtility;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.payment.PaymentWebViewActivity;
import com.azinova.homemaidsuser.payment.WebViewActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.PaymentUtils;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.Shared;
import com.azinova.homemaidsuser.utils.UrlUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import static com.azinova.homemaidsuser.utils.UrlUtils.DEFAULT_TIMEOUT;

public class PaymentSuccessActivity extends AppCompatActivity {

    private RelativeLayout backButton;
    private LottieAnimationView animationView;
    private RelativeLayout closeButton;
    private RelativeLayout help_lay;
    private String status, amount, order_id,trackingId,modeOfPayment,From,service_date_new,day_service_id;
    private TextView price;
    private TextView header, statustext;
    private ImageView errorimage;
    private RelativeLayout retrypay;
    private ProgressDialog pDialog;
    private Prefs prefs;
    private CustomerModels customerModels;
    private CustomerDetails model;
    private TextView orderText, orderstatText, trackText, curText, modeText, amtText, disText, netText;
    private LinearLayout detaillay;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            status = extras.getString("transStatus", "error");
            amount = extras.getString("price", "error");
            order_id = extras.getString("order_id","");
            trackingId =  extras.getString("tracking_id");
            modeOfPayment =  extras.getString("modeOfPayment");
            From =extras.getString("from");
            service_date_new =extras.getString("service_date_new");
            day_service_id =extras.getString("day_service_id");

        }
        initView();

        prefs = Prefs.with(PaymentSuccessActivity.this);
        customerModels = prefs.getUserDetails();
        model = customerModels.getCustomerDetails();

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.tick_tone);
        final Handler handler = new Handler();
        final Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (status.equalsIgnoreCase("success")) {

            if(From.equals("make_payment")){

                orderText.setText("HM-ON-"+order_id);

            }else{
                orderText.setText("HM"+order_id);

            }

            statustext.setText("Payment Completed Successfully");
            header.setText("PAYMENT SUCCESS");
            price.setText("AED " + amount);
            animationView.setVisibility(View.VISIBLE);
            errorimage.setVisibility(View.GONE);
            curText.setText("AED");
            animationView.setAnimation("tick_animation.json");
            animationView.playAnimation();
            retrypay.setVisibility(View.GONE);
//            detaillay.setVisibility(View.VISIBLE);
            trackText.setText(trackingId);
            orderstatText.setText("Success");
            modeText.setText(modeOfPayment);
            amtText.setText(amount);

        } else {
            animationView.setVisibility(View.GONE);
            errorimage.setVisibility(View.VISIBLE);
            header.setText("PAYMENT FAILED");
            statustext.setText("Your Payment has Failed");
            orderstatText.setText("Failed");
            curText.setText("AED");
            trackText.setText(trackingId);
            modeText.setText(modeOfPayment);
            amtText.setText(amount);
            price.setText("AED " + amount);
            retrypay.setVisibility(View.VISIBLE);

            if(From.equals("make_payment")){

                orderText.setText("HM-ON-"+order_id);

            }else{
                orderText.setText("HM"+order_id);

            }
        }

        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mp.start();
                        v.vibrate(100);
                    }
                }, 800);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        clickListeners();

//        PaymentDetails();
    }

    private void clickListeners() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PaymentSuccessActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
//                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentSuccessActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                   }
        });

        help_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent help = new Intent(PaymentSuccessActivity.this, HelpActivity.class);
                startActivity(help);
            }
        });

        retrypay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                if (Shared.pay_bookingid != null || !Shared.pay_bookingid.isEmpty()) {
//                    EventBus.getDefault().post(new WebViewActivity.finishEvent());
//                    PaymentEngine();
//                }

                Intent webview_payment = new Intent(PaymentSuccessActivity.this, PaymentWebViewActivity.class);

                webview_payment.putExtra("booking_id", order_id);
                webview_payment.putExtra("price", amount);
                webview_payment.putExtra("from","payment_fail");
                webview_payment.putExtra("service_date_new","service_date_new");
                webview_payment.putExtra("day_service_id","day_service_id");

                startActivity(webview_payment);

            }
        });
    }

    private void initView() {
        backButton = findViewById(R.id.backbutton);
        animationView = findViewById(R.id.animation_view);
        closeButton = findViewById(R.id.close_button);
        help_lay = findViewById(R.id.help_lay);
        price = findViewById(R.id.price);
        header = findViewById(R.id.header);
        statustext = findViewById(R.id.status);
        errorimage = findViewById(R.id.errorimage);
        retrypay = findViewById(R.id.retrypay);

        orderText = findViewById(R.id.orderText);
        orderstatText = findViewById(R.id.orderstatText);
        trackText = findViewById(R.id.trackText);
        curText = findViewById(R.id.curText);
        modeText = findViewById(R.id.modeText);
        amtText = findViewById(R.id.amtText);
        disText = findViewById(R.id.disText);
        netText = findViewById(R.id.netText);

        detaillay = findViewById(R.id.detaillay);
        progressbar = findViewById(R.id.progressbar);

    }


    @Override
    public void onBackPressed() {
        this.finish();
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void PaymentEngine() { //for retry payment

        pDialog = new ProgressDialog(PaymentSuccessActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JSONObject paydetails = new JSONObject();
        try {
            paydetails.put("customer_id", prefs.getCustomerId());
            paydetails.put("booking_id", Shared.pay_bookingid);
            paydetails.put("price", Shared.pay_total);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest paymentObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getOrderId(), paydetails,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtil.e(response.toString());
                        pDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("error") ||
                                    response.getString("status").equalsIgnoreCase("failure")) {
                                Snackbar.make(retrypay, "Something went wrong!", Snackbar.LENGTH_LONG).show();
                                return;
                            }

                            if (response.getString("status").equalsIgnoreCase("success")) {
                                LogUtil.e(response.toString());

                                String vAccessCode = ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim();
                                String vMerchantId = ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim();
                                String vCurrency = ServiceUtility.chkNull("AED").toString().trim();
                                String vAmount = ServiceUtility.chkNull(Shared.pay_total).toString().trim(); //total_price
                                if (!vAccessCode.equals("") && !vMerchantId.equals("") && !vCurrency.equals("") && !vAmount.equals("")) {
                                    Intent intent = new Intent(PaymentSuccessActivity.this, WebViewActivity.class);
                                    intent.putExtra(AvenuesParams.ACCESS_CODE, ServiceUtility.chkNull(PaymentUtils.AccessCode).toString().trim());
                                    intent.putExtra(AvenuesParams.MERCHANT_ID, ServiceUtility.chkNull(PaymentUtils.MerchantID).toString().trim());
                                    intent.putExtra(AvenuesParams.ORDER_ID, ServiceUtility.chkNull(response.getString("order_id")).toString().trim());
                                    intent.putExtra(AvenuesParams.CURRENCY, ServiceUtility.chkNull("AED").toString().trim());
                                    intent.putExtra(AvenuesParams.AMOUNT, ServiceUtility.chkNull(Shared.pay_total).toString().trim()); //total_price
                                    intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                    intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(PaymentUtils.RedirectUrl).toString().trim());
                                    intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(PaymentUtils.RSA_URL).toString().trim());

                                    intent.putExtra(AvenuesParams.BILLING_NAME,model.getName());
                                    if (customerModels.getAreaList().size() > 0) {
                                        intent.putExtra(AvenuesParams.BILLING_ADDRESS,customerModels.getAreaList().get(0).getCustomerAddress());
                                        intent.putExtra(AvenuesParams.BILLING_CITY,customerModels.getAreaList().get(0).getAreaName());
                                    }
                                    intent.putExtra(AvenuesParams.BILLING_COUNTRY,"United Arab Emirates");
                                    intent.putExtra(AvenuesParams.BILLING_TEL,model.getPhone());
                                    intent.putExtra(AvenuesParams.BILLING_EMAIL,model.getEmail());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    finish();
                                } else {
                                    Toast.makeText(PaymentSuccessActivity.this, "Toast: " + "Something went wrong!", Toast.LENGTH_LONG).show();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found.\nPlease try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Something went wrong!\nPlease try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...\nPlease check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut!\nPlease check your internet connection.";
                }

                Snackbar.make(retrypay, message, Snackbar.LENGTH_LONG).show();
            }
        });

        paymentObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(paymentObjReq);

    }

    private void PaymentDetails() {
        progressbar.setVisibility(View.VISIBLE);
        JSONObject getdetails = new JSONObject();
        try {
            getdetails.put("order_id", order_id);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest paymentdetailObjReq = new JsonObjectRequest(Request.Method.POST,
                UrlUtils.getPaymentDetail(), getdetails,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtil.e(response.toString());
                        progressbar.setVisibility(View.GONE);
                        try {
                            if (response.getString("status").equalsIgnoreCase("error") ||
                                    response.getString("status").equalsIgnoreCase("failure")) {
                                Snackbar.make(retrypay, "Something went wrong!", Snackbar.LENGTH_LONG).show();
                                return;
                            }

                            if (response.getString("status").equalsIgnoreCase("success")) {
                                LogUtil.e(response.toString());
                                JSONObject json = response.getJSONObject("payment_details");
                                orderText.setText(json.getString("order_id"));
                                orderstatText.setText(json.getString("order_status"));
                                trackText.setText(json.getString("track_id"));
                                curText.setText(json.getString("currency"));
                                modeText.setText(json.getString("pay_mode"));
                                amtText.setText(json.getString("amount"));
                                disText.setText(json.getString("discount"));
                                netText.setText(json.getString("net_amount"));
                                detaillay.setVisibility(View.VISIBLE);
                                if(!json.getString("order_status").equalsIgnoreCase("success")){
                                    orderstatText.setTextColor(Color.RED);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressbar.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressbar.setVisibility(View.GONE);
            }
        });

        paymentdetailObjReq.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(paymentdetailObjReq);

    }


}
