package com.azinova.homemaidsuser;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.azinova.homemaidsuser.models.CustomerDetails;
import com.azinova.homemaidsuser.models.CustomerModels;
import com.azinova.homemaidsuser.models.PaymentSuccess;
import com.azinova.homemaidsuser.models.TransactionResposnse;
import com.azinova.homemaidsuser.payment.WebViewActivity;
import com.azinova.homemaidsuser.utils.AppController;
import com.azinova.homemaidsuser.utils.LogUtil;
import com.azinova.homemaidsuser.utils.Prefs;
import com.azinova.homemaidsuser.utils.UrlUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import mumbai.dev.sdkdubai.BillingAddress;
import mumbai.dev.sdkdubai.CustomModel;
import mumbai.dev.sdkdubai.MerchantDetails;
import mumbai.dev.sdkdubai.PaymentOptions;
import mumbai.dev.sdkdubai.ShippingAddress;

public class PaymentActivity extends AppCompatActivity implements CustomModel.OnCustomStateListener {

    @BindView(R.id.animation_view)
    LottieAnimationView animation_view;
    static MerchantDetails merchant;
    static BillingAddress billing;
    static ShippingAddress shipping;
    ProgressDialog pDialog;

    private RelativeLayout backButton;
    private LottieAnimationView animationView;
    private RelativeLayout closeButton;
    private RelativeLayout help_lay;
    private TextView price;
    private TextView header, statustext;
    private ImageView errorimage;
    private RelativeLayout retrypay;
    private String payAmount = "";
    private Prefs prefs;
    private CustomerModels customerModels;
    private CustomerDetails model;
    private TextView orderText, orderstatText, trackText, curText, modeText, amtText, disText, netText;
    private LinearLayout detaillay;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        initView();

        CustomModel.getInstance().setListener(this);  // Payment Gateway state listener

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        doPayment();
        clickListeners();
    }

    private void doPayment() {

        Intent intent = this.getIntent();
        merchant = intent.getParcelableExtra("merchant");
        billing = intent.getParcelableExtra("billing");
        shipping = intent.getParcelableExtra("shipping");
        Intent i = new Intent(PaymentActivity.this, PaymentOptions.class);

        i.putExtra("merchant", merchant);
        i.putExtra("billing", billing);
        i.putExtra("shipping", shipping);
        startActivity(i);
    }

    private void updateTransaction(String payment_status, String amount, String order_id, String tracking_id, PaymentSuccess paymentSuccess) {
//        pDialog.show();

        runOnUiThread(() -> updatePaymentDetails(paymentSuccess));

        try {

            JSONObject jsonObjectNew = new JSONObject();

            jsonObjectNew.put("payment_status", payment_status);
            jsonObjectNew.put("amount", amount);
            jsonObjectNew.put("order_id", order_id);
            jsonObjectNew.put("tracking_id", tracking_id);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, UrlUtils.updateTransaction(), jsonObjectNew,
                    response -> {

                        Log.e(" Resposne ", new GsonBuilder().setPrettyPrinting().create().toJson(response));

                        TransactionResposnse transactionResposnse = new Gson().fromJson(response.toString(), TransactionResposnse.class);

                        if (transactionResposnse != null) {

                        }

                    }, error -> {
                pDialog.dismiss();
                LogUtil.e("error   ", error.toString());
            });

            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
            VolleyLog.DEBUG = true;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupViews(PaymentSuccess transactionResposnse, String amount) {


        final MediaPlayer mp = MediaPlayer.create(this, R.raw.tick_tone);
        final Handler handler = new Handler();
        final Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (transactionResposnse.getOrder_status().equalsIgnoreCase("success")) {
            statustext.setText("Your Payment is Completed");
            header.setText("PAYMENT SUCCESS");
            price.setText("AED " + amount);
            animationView.setVisibility(View.VISIBLE);
            errorimage.setVisibility(View.GONE);
            animationView.setAnimation("tick_animation.json");
            animationView.playAnimation();
            retrypay.setVisibility(View.GONE);
        } else {
            animationView.setVisibility(View.GONE);
            errorimage.setVisibility(View.VISIBLE);
            header.setText("PAYMENT FAILED");
            statustext.setText("Your Payment has Failed");
            price.setText("AED " + amount);
            retrypay.setVisibility(View.VISIBLE);
        }

        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

                handler.postDelayed(() -> {
                    mp.start();
                    v.vibrate(100);
                }, 800);
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    private void initView() {
        backButton = findViewById(R.id.backbutton);
        animationView = findViewById(R.id.animation_view);
        closeButton = findViewById(R.id.close_button);
        help_lay = findViewById(R.id.help_lay);
        price = findViewById(R.id.price);
        header = findViewById(R.id.header);
        statustext = findViewById(R.id.status);
        errorimage = findViewById(R.id.errorimage);
        retrypay = findViewById(R.id.retrypay);

        orderText = findViewById(R.id.orderText);
        orderstatText = findViewById(R.id.orderstatText);
        trackText = findViewById(R.id.trackText);
        curText = findViewById(R.id.curText);
        modeText = findViewById(R.id.modeText);
        amtText = findViewById(R.id.amtText);
        disText = findViewById(R.id.disText);
        netText = findViewById(R.id.netText);

        detaillay = findViewById(R.id.detaillay);
        progressbar = findViewById(R.id.progressbar);

    }

    private void clickListeners() {
        backButton.setOnClickListener(v -> finishBackActivities());

        closeButton.setOnClickListener(v -> finishBackActivities());

        help_lay.setOnClickListener(v -> {
            Intent help = new Intent(PaymentActivity.this, HelpActivity.class);
            startActivity(help);
        });

        retrypay.setOnClickListener(v -> {

                doPayment();

        });
    }

    private void finishBackActivities() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        EventBus.getDefault().post(new WebViewActivity.finishEvent());
        EventBus.getDefault().post(new BookedDetailActivity.finishEvent());
        EventBus.getDefault().post(new MainActivity.BookingHistoryOpenEvent());
        EventBus.getDefault().post(new MakePaymentActivity.finishEvent());
    }

    @Override
    public void stateChanged() {
        String modelState = CustomModel.getInstance().getState();

        Log.e("Status ", modelState);

        if (modelState.equals("Transaction Closed")) {
            animationView.setVisibility(View.GONE);
            errorimage.setVisibility(View.VISIBLE);
            header.setText("PAYMENT CANCELLED");
            statustext.setText("Transaction cancelled");
            price.setText("AED " + merchant.getAmount());
            retrypay.setVisibility(View.VISIBLE);
        } else {

            PaymentSuccess payment = new Gson().fromJson(modelState, PaymentSuccess.class);

            if (payment != null) {

                runOnUiThread(() -> {
                    setupViews(payment, payment.getAmount());
                    updateTransaction(payment.getOrder_status(), payment.getAmount(), payment.getOrder_id(), payment.getTracking_id(), payment);
                });

            }
        }

    }

    private void updatePaymentDetails(PaymentSuccess payment) {

        orderText.setText(payment.getOrder_id());
        orderstatText.setText(payment.getOrder_status());
        trackText.setText(payment.getTracking_id());
        curText.setText(payment.getCurrency());
        modeText.setText(payment.getPayment_mode());
        amtText.setText(payment.getAmount());
        disText.setText(payment.getDiscount_value());
        netText.setText(payment.getAmount());
        detaillay.setVisibility(View.VISIBLE);

    }

}
